var stompClient = null;

function setConnected(connected) {
    $("#connect").prop("disabled", connected);
    $("#disconnect").prop("disabled", !connected);
    if (connected) {
        $("#conversation").show();
    }
    else {
        $("#conversation").hide();
    }
    $("#messageArea").html("");
}

function connect() {
    var socket = new SockJS('http://localhost:8080/chatEnd');
    stompClient = Stomp.over(socket);
    stompClient.connect({}, function (frame) {
        setConnected(true);
        console.log('Connected: ' + frame);
        stompClient.subscribe('/topic/greetings', function (greeting) {
            showGreeting(JSON.parse(greeting.body).time ,JSON.parse(greeting.body).username ,JSON.parse(greeting.body).content);
        });
    });
}

function disconnect() {

    if (stompClient != null) {
        stompClient.disconnect();
    }
    setConnected(false);
    console.log("Disconnected");
}

function sendName() {
    var username = getUsername();

    if( username === '"anonymousUser"') {
        username = 'anonymous';
    }

    stompClient.send("/app/hello", {}, JSON.stringify({'name': username, 'message' : $("#messageArea").val()}));
    $("#messageArea").val('') ;
}

function showGreeting(time, username, message) {
    $("#log").append("<tr><td>" + time + ' ' + username + ' ' + message + "</td></tr>");
}

function getUsername() {
    var username = "";

    $.ajax({
        url: "/chat/username",
        async: false
    }).success(function (data) {
        username = JSON.stringify(data);
    });

    return username;
}

$(function () {
    $("form").on('submit', function (e) {
        e.preventDefault();
    });
    $( "#connect" ).click(function() { connect(); });
    $( "#disconnect" ).click(function() { disconnect(); });
    $( "#send" ).click(function() { sendName(); });
});

