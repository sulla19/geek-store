// Check if user logged;
jQuery(document).ready(function($) {

    $("select[id = sort_criteria]").change(function(){
        var sort_type =  $( "#sort_criteria").find("option:selected" ).val();
        loadWithSort(sort_type)
    });

    $.ajax({
        type: 'GET',
        url: '/login/check'

    }).done(function () {
        $('#login-logout').html('<a href="/logout">Logout</a>');

    }).fail(function () {
        $('#login-logout').html('<a href="/login">Login</a> or <a href="/login">Create an account</a>');
    });
});

function toUrlRequiredAuth(url) {
    event.preventDefault();

    jQuery(document).ready(function($) {
        $.ajax({
            type: 'GET',
            url: "/login/check"

        }).done(function () {
            window.location.replace(url);

        }).fail(function (jqXHR) {
            if (jqXHR.status === 401) { // HTTP Status 401: Unauthorized
                window.location = '/login';
            }
        });
    });
}

jQuery(document).ready(function ($) {
    $('#login-form').submit(function (event) {
        event.preventDefault();
        var data = 'username=' + $('#username').val() + '&password=' + $('#password').val();
        $.ajax({
            data: data,
            timeout: 1000,
            type: 'POST',
            url: '/login'

        }).done(function(response) {
            window.location.replace(document.referrer);

        }).fail(function(jqXHR, textStatus, errorThrown) {
            $('#login_error').text('Bad credentials');
        });
    });

    $('#signup-form').submit(function (event) {
        event.preventDefault();
        var data = 'email=' + $('#email').val() + '&password=' + $('#pass').val() +
            '&passwordRepeated=' + $('#confirm_pass').val();
        $.ajax({
            data: data,
            timeout: 1000,
            type: 'POST',
            url: '/user/register'

        }).done(function() {
            window.location.replace("/home");

        }).fail(function(jqXHR) {
            if (jqXHR.status === 401) {
                $('#register-fail').text(jqXHR.responseText);
            }
        });
    });
});

// load categories tree on sidebar
function loadCategories() {
    var id = getIdFromPath();

    if( id === 'shop') {
        id = 70;
    }

    $.getJSON("/categories/" + id, function (data) {

        var items = "";
        // 1st level categories
        $.each(data, function (key, val) {
            items += '<li><a href="/shop/category/' + val.categoryId + '">' + val.name + '</a></li>';

            $("#categories").html(items);
        });
    });
}

// load all products in category and all subcategories'
function loadProductsByCategoryAndPriceRange(pageId, perPage, min, max, sort_criteria) {

        var id = getIdFromPath();
        var currentMinPrice = $('#slider-range').slider("values", 0);
        var currentMaxPrice = $('#slider-range').slider("values", 1);
        if (min == undefined || max == undefined) {
            min = currentMinPrice;
            max = currentMaxPrice;
        }

        if (sort_criteria == undefined) {
            sort_criteria = 'most_viewed';
        }

        var query_url = urlBuild(id, pageId, perPage, min, max, sort_criteria);
        var sort = '\'' + $( "#sort_criteria").find("option:selected" ).val() + '\'';

        $.getJSON(query_url, function (data) {
           console.log("query " + query_url);
            var items = "";
            $.each(data.items, function (key, val) {
                items += '<div class="col-sm-2 fix">' +
                            '<div class="product-item fix">' +
                            '<div class="product-img-hover">';
                items += '<a href="/product/' + val.productId + '" class="pro-image fix"><img src="' + val.srcImg + '" alt="product" /></a>';
                items += '<div class="product-action-btn">' +
                            '<a class="quick-view" href="#"><i class="fa fa-search"></i></a>' +
                            '<a class="favorite" href="#"><i class="fa fa-heart-o"></i></a>' +
                            '<a class="add-cart" href="#" onclick="addItem(' + val.productId + ')"><i class="fa fa-shopping-cart"></i></a>' +
                    '</div></div>' +
                    '<div class="pro-name-price-ratting">' +
                    '<div class="pro-name">';
                items += '<a href="/product/"' + val.productId + '>' + val.title + '</a></div>';
                items += '<div class="pro-ratting">' +
                    '<i class="on fa fa-star"></i>' +
                    '<i class="on fa fa-star"></i>' +
                    '<i class="on fa fa-star"></i>' +
                    '<i class="on fa fa-star"></i>' +
                    '<i class="on fa fa-star-half-o"></i>' +
                    '</div>' +
                    '<div class="pro-price fix">';
                items += '<p><span class="old">$165</span><span class="new">' + val.price + '</span></p>';
                items += '</div></div></div></div>';
                $("#products").html(items);
            });
            pagination(items, data, currentMinPrice, currentMaxPrice, sort);
        });
}

function urlBuild(id, pageId, perPage, min, max, sort) {
    console.log("sort " + sort_criteria);

    return "/category/" + id + '?pageId=' + pageId + '&perPage=' + perPage + '&minPrice=' + min +
            '&maxPrice=' + max + '&sort=' + sort.toUpperCase();
}

function pagination(items, data, currentMinPrice, currentMaxPrice, sort) {
    var perPage = 18;

    var prevPageId = data.page == 1 ? 1 : data.page - 1;
    var nextPageId = data.page == data.pageCount ? data.page : data.page + 1;

    items += '<div id="pagination" class="pagination">';
    items += '<ul>' +
        '<li><a id="btn_prev" onclick="loadProductsByCategoryAndPriceRange(' + prevPageId + ',' + perPage + ',' +
        currentMinPrice + ',' + currentMaxPrice + ',' + sort + ')"><i class="fa fa-angle-left"></i></a></li>';

    items += '<li><a id="btn_prev" onclick="loadProductsByCategoryAndPriceRange(' + nextPageId + ',' + perPage + ',' +
        currentMinPrice + ',' + currentMaxPrice + ',' + sort + ')"><i class="fa fa-angle-right"></i></a></li>' +
        '</ul></div>';
    $("#products").html(items);
}

function addItem(id) {
    event.preventDefault();
    $.ajax({
        url: "/cart/add/" + id
    }).success(function () {
        headerCartInfo();
    });
}

function getCartProducts() {

    $.getJSON("/cart/all", function (data) {

        var items = "";
        var totalPrice = 0;
        $.each(data, function (key, val) {

            items += '<tr class="table-info">';
            items += '<td class="produ">' +
                         '<a href="/product/' + val.product.productId + '"><img alt="" src="' + val.product.srcImg + '"></a>' +
                     '</td>';
            items += '<td class="namedes">' +
                         '<h2><a href="/product/' + val.product.productId + '">' + val.product.title + '</a></h2>' +
                      '<p>' + val.product.dtoDescription + '</p>' +
                     '</td>';
            items += '<td class="unit">' +
                         '<h5>' + val.product.price + '</h5>' +
                     '</td>';
            items += '<td class="quantity">' +
                         '<div class="cart-plus-minus">' +
                                '<input type="text" value="' + val.count +' " name="qtybutton" class="cart-plus-minus-box">' +
                         '</div>' +
                     '</td>';
            items += '<td class="valu">' +
                         '<h5>' + val.product.price + '</h5>' +
                     '</td>';
            items += '<td class="acti">' +
                         '<a href="#" onclick="removeItem(' + val.product.productId + ')"><i class="fa fa-trash-o"></i></a>' +
                     '</td>' +
                     '</tr>';
            totalPrice += val.product.price;

            $("#cart-products").html(items);
            $("#total_price").html(totalPrice);
        });
    });
}

function removeItem(id) {
    event.preventDefault();
    $.ajax({
        url: "/cart/remove/" + id
    }).success(function () {
        getCartProducts();
        headerCartInfo();
    });
}

function headerCartInfo() {
    $.getJSON("/cart/all", function (data) {

        var items = "";
        var totalPrice = 0;
        $.each(data, function (key, val) {
            items += '<li>' +
                '<div class="image"><a href="/product/' + val.product.productId + '"><img alt="cart item" src="' + val.product.srcImg + '"></a></div>' +
                '<div class="content fix"><a href="/product/' + val.product.productId + '">' + val.product.title + '</a>' +
                '<span class="price">$' + val.product.price + '</span><span class="quantity">Quantity: ' + val.count  + '</span></div>' +
                '<a href="#" onclick="removeItem(' + val.product.productId + ')"><i class="fa fa-trash delete"></i></a>' +
                '</li>';
            totalPrice += val.product.price;
            $("#header-cart-info").html(items);
            $("#header-card-items").text(data.length);
            $("#header-card-price").text(totalPrice);
            $("#header-cart-total-price").text(totalPrice);
        })
    });
}

// loading product info and all reviews on product page
function getProduct() {
    var id = getIdFromPath();

    $.getJSON("/product/" + id + "/get", function (data) {

        $("#product-name").text(data.title);
        $("#product-author").html('Author : <span>' + data.author + '</span></h5>');
        $("#product-price").html('<span>$165</span>$' + data.price + '');
        $("#product-isbn").html('ISBN : <span>' + data.isbn + '</span></h5>');
        $("#big-image-scaling").attr("data-lens-image", data.srcImg);
        $("#big-image-base").attr("src", data.srcImg);
        $("#product-description").text(data.description);
        $("#review-count").text(data.reviews.length + " reviews");

        $("#action_panel").html('<a href="#" onclick="addItem(' + id + ')"><i class="fa fa-shopping-cart"></i></a>' +
                                '<a href="#"><i class="fa fa-heart-o"></i></a>' +
                                '<a href="#"><i class="fa fa-refresh"></i></a>' +
                                '<a href="#" onclick="addProductComment()" class="float-right">Add</a>');

        var items = "";
        $.each(data.reviews, function (key, val) {
            items += '<li class="sin-comment">' +
                '<div class="the-comment">' +
                '<div class="comment-box">' +
                '<div class="comment-author">' +
                '<p class="com-name"><strong>' + val.customerFirstName + ' ' + val.customerLastName + '</strong>' +
                '</p>' + val.reviewDate + '<a href="#" class="repost-link"> Repost </a>' +
                '<a href="#" class="comment-reply-link"> Reply </a>' +
                '</div>' +
                '<div class="comment-text">' +
                '<p>' + val.comment + '</p>' +
                '</div>' +
                '<div class="comment-text">' +
                '<p><strong>Advantages</strong></p>' +
                '<p>' + val.advantages + '</p>' +
                '</div>' +
                '<div class="comment-text">' +
                '<p><strong>Disadvantages</strong></p>' +
                '<p>' + val.disadvantages + '</p>' +
                '</div>' +
                '</div>' +
                '</div>' +
                '</li>';
            $("#reviews").html(items);
        })
    });
}

function addProductComment() {
    event.preventDefault();

    jQuery(document).ready(function($) {
        $.ajax({
            type: 'GET',
            url: "/login/check"

        }).done(function () {
            var comment = $("#add_comment").val();
            var id = getIdFromPath();
            var data = {"comment" : comment, "productId" : id};

            addComment(data);

        }).fail(function (jqXHR) {
            if (jqXHR.status === 401) { // HTTP Status 401: Unauthorized
                window.location = '/login';
            }
        });
    });
}

function addComment(data) {
    $.ajax({
        url: "/product/comment",
        type: 'POST',
        data: data
    }).done(function() {
        window.location.reload(true);
    })
}

// Setting price range and title of category
function getPageInfo() {

    $(document).ready(function ($) {
        var attr = getPageAttributes();
        var minPrice = attr.minPrice;
        var maxPrice = attr.maxPrice;

        $("#slider-range").slider({
            range: true,
            min: 1,
            max: maxPrice,
            values: [1, maxPrice],
            slide: function (event, ui) {
                $("#price-amount").val("$" + ui.values[0] + " - $" + ui.values[1]);
            }
        });
        $("#price-amount").val("$" + $("#slider-range").slider("values", 0) +
            " - $" + $("#slider-range").slider("values", 1));

        $('.newslater-container .close').on("click", function () {
            $('#popup-newslater').addClass('hidden');
        });

        $('#submit').on("click", function () {
            var minPrice = $("#slider-range").slider("values", 0);
            var maxPrice = $("#slider-range").slider("values", 1);
            loadProductsByCategoryAndPriceRange(1, 18, minPrice, maxPrice);
        });
    });
}

// Getting price range and title of category
function getPageAttributes() {
    var categoryId = getIdFromPath();

    var result = 0;
    $.ajax({
        url: "/category/" + categoryId + "/range",
        async: false
    }).success(function (data) {
        result = data.priceRange;

        $("#category_name").html(data.categoryName);
    });
    return result
}

function loadWithSort(str) {
    var currentMinPrice = $('#slider-range').slider("values", 0);
    var currentMaxPrice = $('#slider-range').slider("values", 1);

    console.log("min-max " + currentMinPrice + " " + currentMaxPrice);
    loadProductsByCategoryAndPriceRange(1, 18, currentMinPrice, currentMaxPrice, str);
}

function getIdFromPath() {
    var url = window.location.pathname;
    var url_array = url.split('/');
    var id = url_array[url_array.length - 1];

    if (id === 'shop') {
        id = 70;
    }
    return id;
}

function submit_order() {
    event.preventDefault();

    var billing = billing_info();
    var shipping = shipping_info();
    var payment = pay_info();

    $.ajax({
        data: billing + shipping + payment,
        timeout: 1000,
        type: 'POST',
        url: '/order'

    }).done(function() {
        window.location.replace("/shop");

    }).fail(function() {
        alert("Smth wrong")
    });
}

function pdf_order() {
    event.preventDefault();

    var billing = billing_info();
    var shipping = shipping_info();
    var payment = pay_info();
    window.location.replace("/order/pdf?" + billing + shipping + payment);
}

function payment(cc_type) {
    event.preventDefault();

    $('#cc_type').attr('value', cc_type);

    if (cc_type == 'VISA' || cc_type == 'MASTERCARD') {
        $('#cc_info').html('<div class="group">'+
                                '<input id="c_type" type="text" value="' + cc_type + '" placeholder="' + cc_type + '" class="third">'+
                                '<input id="c_number" type="text" placeholder="CC NUMBER*" class="third">'+
                                '<input id="c_cvv" type="text" placeholder="CC CVV*" class="third">'+
                           '</div>');
    } else if (cc_type == 'PAYPAL') {
        $('#cc_info').html('<div class="group">'+
                                '<input id="c_type" type="text" placeholder="PAYPAL ADDRESS" class="third">'+
                            '</div>');
    }
}

// Setting info from input "Shipping info" to general order to submit
function billing_info() {
    event.preventDefault();
    // ----Billing---
    var first_name = $("#first_name").val();
    var last_name = $("#last_name").val();
    var birth_date = $("#birth_date").val();
    var gender = $("#gender").val();
    var email = $("#email").val();
    var phone_number = $("#phone_number").val();

    var address_street = $("#address_street").val();
    var address_city = $("#address_city").val();
    var address_postcode = $("#address_postcode").val();
    var address_country = $("#address_country").val();

    $('#b_name').text(first_name + ' ' + last_name);
    $('#b_street').text(address_street);
    $('#b_birth_date').text(birth_date);
    $('#b_gender').text(gender);
    $('#b_post_city').text(address_postcode + ' ' + address_city);
    $('#b_phone').text(phone_number);
    $('#b_email').text(email);

    var customer = 'firstName=' + first_name + '&lastName=' + last_name + '&birthDate=' + birth_date +
        '&gender='+ gender + '&emailAddress=' + email + '&telephone=' + phone_number;

    var customer_address = '&customerStreet=' + address_street + '&customerCity=' + address_city +
        '&customerPostcode=' + address_postcode + '&customerCountry=' + address_country;

    return [customer, customer_address] ;
}

function shipping_info() {
    event.preventDefault();
    // ---Order---
    var shipping_type = $("#shipping_type").val();
    var total_price = $("#total_price").val();

    // ----Delivery
    var shipping_company = $("#company").val();
    var shipping_city = $("#city").val();
    var shipping_address = $("#address").val();
    var shipping_postcode = $("#postal_code").val();
    var shipping_country = $("#country").val();
    var shipping_telephone = $("#telephone").val();

    $('#sh_type').text('Type : ' + shipping_type);
    $('#sh_company').text('Company : ' + shipping_company);
    $('#sh_country').text('Country : ' + shipping_country);
    $('#sh_city_street').text('Address : ' + shipping_city + ' ' + shipping_address);
    $('#sh_postcode').text('Postcode : ' + shipping_postcode);
    $('#sh_telephone').text('Telephone : ' + shipping_telephone);

    var delivery = '&company=' + shipping_company + '&country='+ shipping_country + '&city=' + shipping_city +
        '&address=' + shipping_address + '&postalCode=' + shipping_postcode + '&deliveryTelephone=' + shipping_telephone;

    var order = '&shippingType=' + shipping_type + '&totalPrice=' + total_price;

    return [order + delivery];
}

function pay_info() {
    var c_type = $("#c_type").val();
    var c_number = $("#c_number").val();
    var c_cvv = $("#c_cvv").val();

    if (c_type == 'VISA' || c_type == 'MASTERCARD') {
        $('#pay_method').text('Method : CREDIT CARD');
        $('#pay_info').html('<h4>Card : ' + c_type + ' </h4>' +
                            '<h4>Number : ' + c_number + '</h4>' +
                            '<h4>Cvv : ' + c_cvv + '</h4>');

        return '&paymentType=creditcard' + '&creditCardType=' + c_type + '&ccNumber=' + c_number + '&ccCvv=' + c_cvv;
    } else {
        $('#pay_method').text('Method : PAYPAL');
        $('#pay_info').html('<h4>Paypal address : : ' + c_type + ' </h4>');
        return '&paymentType=paypal'
    }
}

function setOrderPage() {

    $.getJSON("/cart/all", function (data) {

        var items = "";
        var totalPrice = 0;
        $.each(data, function (key, val) {
            items += '<li>' +
                '<div class="image"><a href="/product/' + val.product.productId + '"><img alt="cart item" src="' + val.product.srcImg + '"></a></div>' +
                '<div class="content fix">' +
                '<a href="/product/' + val.product.productId + '">' + val.product.title + '</a>' +
                '</div>' +
                '<span class="price">$' + val.product.price * val.count + '</span>' +
                '<div class="content fix">' +
                '<span class="quantity">Quantity: ' + val.count + '</span>' +
                '<a href="#" onclick="removeItem(' + val.product.productId + ')"><i class="fa fa-trash-o"></i></a>' +
                '</div>' +
                '</li>';

            totalPrice += val.product.price * val.count;
            $("#order-cart-info").html(items);
            $("#grand_total").html('Grand Total <span>$' + totalPrice + '</span>');
            $("#total_price").attr("value", totalPrice );
        })
    });
}

jQuery(document).ready(function ($) {

    var engine = new Bloodhound({
        remote: {
            url: '/search?q=%QUERY%',
            wildcard: '%QUERY%'
        },
        datumTokenizer: Bloodhound.tokenizers.whitespace('q'),
        queryTokenizer: Bloodhound.tokenizers.whitespace
    });

    $(".search-input").typeahead({
        hint: true,
        highlight: true,
        minLength: 1,
        maxLength : false
    }, {
        limit: Infinity,
        readonly : true,
        source: engine.ttAdapter(),

        name: 'usersList',

        templates: {
            empty: [
                '<div class="list-group search-results-dropdown"><div class="list-group-item">Nothing found.</div></div>'
            ],
            header: [
                '<div class="list-group search-results-dropdown">'

            ],
            suggestion: function (data) {
                console.log(JSON.stringify(data));
                return '<a href=/product/' + data.productId + ' class="list-group-item">' + data.title + '. ' + data.isbn + ' ' + data.srcImg + '  '
                    + data.telephone + '</a>'
            }
        }
    });
});

