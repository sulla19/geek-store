package com.geekhub.geekstore.shop.store.repository;


import com.geekhub.core.model.common.PriceRange;

public interface MetricsRepository {

    PriceRange getPriceRangeByCategory(Integer id);
}
