package com.geekhub.geekstore.shop.store.service;

import com.geekhub.core.model.catalog.category.CategoryTree;

import java.util.List;

public interface CategoryService {

    String getCategoryName(Integer id);

    List<CategoryTree> getCategoryTree(Integer id);
}
