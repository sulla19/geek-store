package com.geekhub.geekstore.shop.application.config;

import com.geekhub.geekstore.shop.store.metric.MetricInterceptor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry;
import org.springframework.web.servlet.config.annotation.ViewControllerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;
import org.springframework.web.servlet.view.ResourceBundleViewResolver;

@Configuration
@ComponentScan(basePackages = "com.geekhub.geekstore.shop.*")
public class WebConfig extends WebMvcConfigurerAdapter {

    @Autowired
    private ApplicationContext context;

    @Override
    public void addResourceHandlers(ResourceHandlerRegistry registry) {
        registry.addResourceHandler("/static/**").
                addResourceLocations("classpath:/static/");
    }

    @Override
    public void addInterceptors(InterceptorRegistry registry) {
        registry.addInterceptor(context.getBean(MetricInterceptor.class)).addPathPatterns("/product/{id}");
    }

    @Override
    public void addViewControllers(ViewControllerRegistry registry) {
        registry.addRedirectViewController("/", "home");
        registry.addViewController("/product/{id}").setViewName("product");
        registry.addViewController("/chat").setViewName("chat");
        registry.addViewController("/home").setViewName("home");
        registry.addViewController("/shop").setViewName("shop");
        registry.addViewController("/shop/category/{id}").setViewName("shop");
        registry.addViewController("/cart").setViewName("cart");
    }

    @Bean
    public ResourceBundleViewResolver viewResolver() {
        ResourceBundleViewResolver resolver = new ResourceBundleViewResolver();
        resolver.setOrder(1);
        resolver.setBasename("views");
        return resolver;
    }
}
