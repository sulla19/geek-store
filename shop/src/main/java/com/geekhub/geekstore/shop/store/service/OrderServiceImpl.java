package com.geekhub.geekstore.shop.store.service;

import com.geekhub.core.model.catalog.product.ProductDto;
import com.geekhub.core.model.order.Order;
import com.geekhub.core.model.order.OrderDto;
import com.geekhub.core.model.order.OrderItem;
import com.geekhub.core.model.order.delivery.Delivery;
import com.geekhub.core.model.order.payment.PaymentType;
import com.geekhub.core.model.order.shipping.ShippingType;
import com.geekhub.core.repository.dtomapper.DtoMapper;
import com.geekhub.geekstore.shop.store.repository.OrderRepository;
import com.geekhub.geekstore.shop.store.security.repository.UserRepository;
import com.geekhub.geekstore.shop.store.security.util.SecurityUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class OrderServiceImpl implements OrderService {

    private OrderRepository orderRepository;
    private UserRepository userRepository;

    @Autowired
    public OrderServiceImpl(OrderRepository orderRepository, UserRepository userRepository) {
        this.orderRepository = orderRepository;
        this.userRepository = userRepository;
    }

    @Override
    public Integer saveOrderByUsername(String username, OrderDto orderDto, Delivery delivery, List<OrderItem> products) {
        Integer customerId = SecurityUtil.getLoggedUserId();

        Order order = new Order();
        order.setCustomerId(customerId);
        order.setDelivery(delivery);
        order.setLastModified(LocalDate.now());

        order.setProducts(products.stream().map(orderItem -> {
            int count = orderItem.getCount();
            List<ProductDto> list = new ArrayList<>();

            for (int i = 1; i <= count; i++) {
                list.add(orderItem.getProduct());
            }
            return list;
        }).flatMap(Collection::stream)
                .map(DtoMapper::getProduct)
                .collect(Collectors.toList()));

        order.setShippingType(ShippingType.valueOf(orderDto.getShippingType().toUpperCase()));
        order.setPaymentType(PaymentType.valueOf(orderDto.getPaymentType().toUpperCase()));
        order.setTotalPrice(orderDto.getTotalPrice());
        order.setContactTelephone(orderDto.getTelephone());

        return orderRepository.saveOrderByUsername(username, order, delivery);
    }
}
