package com.geekhub.geekstore.shop.store.controller;

import com.geekhub.core.model.catalog.product.review.Review;
import com.geekhub.geekstore.shop.store.service.ProductService;
import com.geekhub.geekstore.shop.store.service.SearchService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

@RestController
public class ProductController {

    private ProductService productService;
    private SearchService searchService;

    @Autowired
    public ProductController(ProductService productService, SearchService searchService) {
        this.productService = productService;
        this.searchService = searchService;
    }

    @GetMapping("/product/{id}/get")
    public ResponseEntity<?> getProduct(@PathVariable Integer id) {
        return new ResponseEntity<>(productService.getProductById(id), HttpStatus.OK);
    }

    @PostMapping("/product/comment")
    public ResponseEntity<?> addComment(Review review) {
        productService.addReview(review);
        return new ResponseEntity<>(HttpStatus.OK);
    }

    @GetMapping("/product/{id}")
    public ModelAndView toProduct(@PathVariable Integer id) {
        return new ModelAndView("product");
    }

    @GetMapping("/search")
    public ResponseEntity<?> fulltextSearch(@RequestParam(name = "q") String find) {
        return new ResponseEntity<>(searchService.searchProduct(find), HttpStatus.OK);
    }
}
