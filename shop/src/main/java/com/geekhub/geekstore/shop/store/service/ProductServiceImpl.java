package com.geekhub.geekstore.shop.store.service;

import com.geekhub.core.model.catalog.product.ProductDto;
import com.geekhub.core.model.catalog.product.review.Review;
import com.geekhub.core.model.common.PriceRange;
import com.geekhub.core.model.common.SortCriteria;
import com.geekhub.geekstore.shop.store.repository.MetricsRepository;
import com.geekhub.geekstore.shop.store.repository.ProductRepository;
import com.geekhub.geekstore.shop.store.security.util.SecurityUtil;
import com.geekhub.geekstore.shop.store.statistic.service.StatisticService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.util.List;

import static com.geekhub.core.model.common.ReportPeriod.REPORT_PERIOD_START;
import static com.geekhub.core.model.common.SortCriteria.*;

@Service
public class ProductServiceImpl implements ProductService {

    private ProductRepository productRepository;
    private MetricsRepository metricsRepository;
    private StatisticService statisticService;

    @Autowired
    public ProductServiceImpl(ProductRepository productRepository, MetricsRepository metricsRepository,
                              StatisticService statisticService) {
        this.productRepository = productRepository;
        this.metricsRepository = metricsRepository;
        this.statisticService = statisticService;
    }

    @Override
    public ProductDto getProductById(Integer id) {
        return productRepository.getProductById(id);
    }

    @Override
    public List<ProductDto> getProductsByRequest(Integer id, PriceRange priceRange, SortCriteria sortCriteria) {

        if (sortCriteria == PRICE_LOW_HIGH || sortCriteria == PRICE_HIGH_LOW) {
            return productRepository.getProductsByCategoryRecursively(id, priceRange, sortCriteria);
        } else if (sortCriteria == MOST_VIEWED) {
            return statisticService.getMostViewedProductsInCategory(id, REPORT_PERIOD_START, 100, priceRange);
        } else if (sortCriteria == BEST_SELLING) {
            return statisticService.getBestSellingProducts(id, REPORT_PERIOD_START, 100, priceRange);
        } else if (sortCriteria == REVIEW_COUNT) {
            return statisticService.getMostReviewedProducts(id, 100, priceRange);
        }
        return null;
    }

    @Override
    public List<ProductDto> getProductsByCategory(Integer id, PriceRange priceRange, SortCriteria criteria) {
        return productRepository.getProductsByCategoryRecursively(id, priceRange, criteria);
    }

    @Override
    public PriceRange getPriceRangeByCategory(Integer id) {
        return metricsRepository.getPriceRangeByCategory(id);
    }

    @Override
    public void addReview(Review review) {
        review.setCustomerId(SecurityUtil.getLoggedUserId());
        review.setReviewDate(LocalDate.now());
        productRepository.addReview(review);
    }
}
