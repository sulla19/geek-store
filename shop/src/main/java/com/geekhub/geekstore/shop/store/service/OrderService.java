package com.geekhub.geekstore.shop.store.service;

import com.geekhub.core.model.order.OrderDto;
import com.geekhub.core.model.order.OrderItem;
import com.geekhub.core.model.order.delivery.Delivery;

import java.util.List;

public interface OrderService {

    Integer saveOrderByUsername(String username, OrderDto orderDto, Delivery delivery, List<OrderItem> products);
}
