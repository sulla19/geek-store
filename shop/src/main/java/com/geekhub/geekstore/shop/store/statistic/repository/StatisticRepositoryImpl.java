package com.geekhub.geekstore.shop.store.statistic.repository;

import com.geekhub.core.model.catalog.product.ProductDto;
import com.geekhub.core.model.common.PriceRange;
import com.geekhub.core.model.common.ReportPeriod;
import com.geekhub.core.repository.rowmappers.ProductDtoRowMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import javax.sql.DataSource;
import java.util.List;

@Repository
public class StatisticRepositoryImpl implements StatisticRepository {
    private JdbcTemplate jdbcTemplate;

    @Autowired
    public StatisticRepositoryImpl(DataSource dataSource) {
        this.jdbcTemplate = new JdbcTemplate(dataSource);
    }

    @Override
    public List<ProductDto> getBestSellingProductByPeriodInCategory(Integer categoryId, ReportPeriod fromDate, Integer limit, PriceRange priceRange) {

        String sql = "WITH RECURSIVE r AS ( " +
                "  SELECT geek_store.category.id AS cat_id " +
                "  FROM geek_store.category " +
                "  WHERE geek_store.category.id = ? " +
                "  UNION ALL " +
                "  SELECT geek_store.category.id " +
                "  FROM geek_store.category " +
                "    JOIN r " +
                "      ON geek_store.category.parent_id = r.cat_id) " +
                "SELECT temp.product_id, temp.count, title, author, src_image, temp.price FROM r " +
                "  INNER JOIN ( " +
                "               WITH base AS ( " +
                "                   SELECT " +
                "                     product_id, COUNT(product_id) AS count " +
                "                   FROM geek_store.orders_products " +
                "                     INNER JOIN geek_store.orders " +
                "                       ON geek_store.orders.id = geek_store.orders_products.order_id " +
                "                   WHERE geek_store.orders.last_modified :: DATE > ? " +
                "                   GROUP BY product_id " +
                "                   ORDER BY count DESC " +
                "                   LIMIT ?) " +
                "               SELECT " +
                "                 base.product_id, count, products.price, products.category_id, title, author, src_image " +
                "               FROM base " +
                "                 INNER JOIN geek_store.products" +
                "                   ON base.product_id = geek_store.products.id " +
                "                 INNER JOIN geek_store.product_descriptions " +
                "                   ON base.product_id = geek_store.product_descriptions.product_id " +
                "                 INNER JOIN geek_store.product_image " +
                "                   ON base.product_id = geek_store.product_image.product_id " +
                "             ) AS temp " +
                "    ON r.cat_id = temp.category_id " +
                "WHERE price >= ? AND price <= ? " +
                "ORDER BY count DESC";

        return jdbcTemplate.query(sql, new ProductDtoRowMapper(), categoryId, fromDate.getValue(), limit,
                priceRange.getMinPrice(), priceRange.getMaxPrice());
    }

    @Override
    public List<ProductDto> getMostViewedProductByPeriodInCategory(Integer categoryId, ReportPeriod fromDate, Integer limit, PriceRange priceRange) {

        String sql = "WITH RECURSIVE r AS ( " +
                "  SELECT geek_store.category.id AS cat_id " +
                "  FROM geek_store.category " +
                "  WHERE geek_store.category.id = ? " +
                "  UNION ALL " +
                "  SELECT geek_store.category.id " +
                "  FROM geek_store.category " +
                "    JOIN r " +
                "      ON geek_store.category.parent_id = r.cat_id) " +
                "SELECT temp.product_id, temp.count, title, author, src_image, temp.price FROM r " +
                "  INNER JOIN ( " +
                "               WITH base AS ( " +
                "                   SELECT product_id, COUNT(product_id) AS count " +
                "                   FROM geek_store.product_metric " +
                "                   WHERE geek_store.product_metric.view_date :: DATE > ? " +
                "                   GROUP BY product_id " +
                "                   ORDER BY count DESC)" +
                "               SELECT base.product_id, count, price, category_id, title, author, src_image " +
                "               FROM base " +
                "                 INNER JOIN geek_store.products " +
                "                   ON base.product_id = geek_store.products.id " +
                "                 INNER JOIN geek_store.product_descriptions " +
                "                   ON base.product_id = geek_store.product_descriptions.product_id " +
                "                 INNER JOIN geek_store.product_image " +
                "                   ON base.product_id = geek_store.product_image.product_id " +
                "             ) AS temp " +
                "    ON r.cat_id = temp.category_id " +
                "WHERE price >= ? AND price <= ? " +
                "ORDER BY count DESC " +
                "LIMIT ?";

        return jdbcTemplate.query(sql, new ProductDtoRowMapper(), categoryId, fromDate.getValue(),
                priceRange.getMinPrice(), priceRange.getMaxPrice(), limit);
    }

    @Override
    public List<ProductDto> getMostReviewedProductsInCategory(Integer categoryId, int limit, PriceRange priceRange) {

        String sql = "WITH RECURSIVE r AS ( " +
                "  SELECT geek_store.category.id AS cat_id " +
                "  FROM geek_store.category " +
                "  WHERE geek_store.category.id = ? " +
                "  UNION ALL " +
                "  SELECT geek_store.category.id " +
                "  FROM geek_store.category " +
                "    JOIN r " +
                "      ON geek_store.category.parent_id = r.cat_id) " +
                "SELECT base.product_id, count, base.title, base.author , base.price, base.src_image FROM r " +
                "INNER JOIN (" +
                "    WITH stat AS (SELECT " +
                "                    product_id, COUNT(product_id) AS count " +
                "                  FROM geek_store.reviews " +
                "                  GROUP BY reviews.product_id) " +
                "    SELECT stat.product_id, stat.count, category_id, price, title, author, src_image " +
                "    FROM stat " +
                "      INNER JOIN geek_store.products " +
                "        ON stat.product_id = geek_store.products.id " +
                "    INNER JOIN geek_store.product_descriptions " +
                "      ON stat.product_id = geek_store.product_descriptions.product_id " +
                "    INNER JOIN geek_store.product_image " +
                "      ON stat.product_id = geek_store.product_image.product_id " +
                "    ) AS base " +
                "  ON r.cat_id = base.category_id " +
                "WHERE price >= ? AND price <= ? " +
                "ORDER BY count DESC " +
                "LIMIT ?";

        return jdbcTemplate.query(sql, new ProductDtoRowMapper(), categoryId,
                priceRange.getMinPrice(), priceRange.getMaxPrice(), limit);
    }
}
