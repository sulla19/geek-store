package com.geekhub.geekstore.shop.store.security.repository;

public class UserNotFoundException extends Exception {

    public UserNotFoundException(String text) {
        super(text);
    }
}
