package com.geekhub.geekstore.shop.store.security.controller;

import com.geekhub.geekstore.shop.store.security.util.SecurityUtil;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;

@Controller
public class SecurityController {

    @GetMapping("/login")
    public String login() {
        return "login";
    }

    @GetMapping("login/check")
    public ResponseEntity checkingLogged() {
        if (SecurityUtil.getLoggedUsername().equals("anonymousUser")) {
            return new ResponseEntity(HttpStatus.UNAUTHORIZED);
        } else {
            return new ResponseEntity(HttpStatus.OK);
        }
    }
}

