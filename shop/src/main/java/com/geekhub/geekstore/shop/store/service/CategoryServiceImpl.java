package com.geekhub.geekstore.shop.store.service;

import com.geekhub.core.model.catalog.category.CategoryTree;
import com.geekhub.geekstore.shop.store.repository.CategoryRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class CategoryServiceImpl implements CategoryService {

    private CategoryRepository categoryRepository;

    @Autowired
    public CategoryServiceImpl(CategoryRepository categoryRepository) {
        this.categoryRepository = categoryRepository;
    }

    @Override
    public String getCategoryName(Integer id) {
        return categoryRepository.getCategoryName(id);
    }

    @Override
    public List<CategoryTree> getCategoryTree(Integer id) {
        return categoryRepository.getCategoryTree(id);
    }
}
