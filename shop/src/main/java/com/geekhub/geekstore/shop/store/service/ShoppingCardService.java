package com.geekhub.geekstore.shop.store.service;

import com.geekhub.core.model.order.OrderItem;

import java.util.List;

public interface ShoppingCardService {

    List<OrderItem> addItem(List<OrderItem> products, Integer productId);

    List<OrderItem> removeItem(List<OrderItem> products, Integer productId);
}
