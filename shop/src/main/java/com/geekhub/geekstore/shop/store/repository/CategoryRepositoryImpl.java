package com.geekhub.geekstore.shop.store.repository;

import com.geekhub.core.model.catalog.category.Category;
import com.geekhub.core.model.catalog.category.CategoryTree;
import com.geekhub.core.repository.rowmappers.CategoryRowMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import javax.sql.DataSource;
import java.util.List;

@Repository
public class CategoryRepositoryImpl implements CategoryRepository {

    private JdbcTemplate jdbcTemplate;

    @Autowired
    public CategoryRepositoryImpl(DataSource dataSource) {
        jdbcTemplate = new JdbcTemplate(dataSource);
    }

    @Override
    public Category find(Integer id) {

        String sql = "WITH temp AS ( " +
                "    SELECT " +
                "      geek_store.category.id                       AS curr_id, " +
                "      geek_store.category.img_url                  AS curr_img_url, " +
                "      geek_store.category.parent_id                AS parent_id, " +
                "      geek_store.category_descriptions.title       AS current_title, " +
                "      geek_store.category_descriptions.description AS current_description " +
                "    FROM geek_store.category " +
                "      INNER JOIN geek_store.category_descriptions " +
                "        ON geek_store.category.id = geek_store.category_descriptions.category_id " +
                ") SELECT " +
                "    temp.curr_id, " +
                "    temp.curr_img_url, " +
                "    temp.parent_id, " +
                "    temp.current_title, " +
                "    temp.current_description, " +
                "    geek_store.category_descriptions.title AS parent_title " +
                "  FROM temp " +
                "    INNER JOIN geek_store.category_descriptions " +
                "      ON temp.parent_id = geek_store.category_descriptions.category_id " +
                " WHERE curr_id = ?";

        return jdbcTemplate.queryForObject(sql, new CategoryRowMapper(), id);
    }

    @Override
    public String getCategoryName(Integer id) {

        String sql = "SELECT " +
                "  title " +
                "FROM geek_store.category_descriptions " +
                "WHERE category_id = ?";

        return jdbcTemplate.queryForObject(sql, String.class, id);
    }

    @Override
    public List<CategoryTree> getCategoryTree(Integer id) {

        String sql = "SELECT " +
                "  id, " +
                "  parent_id, " +
                "  title " +
                "FROM geek_store.category " +
                "  INNER JOIN geek_store.category_descriptions " +
                "    ON geek_store.category.id = geek_store.category_descriptions.category_id " +
                "WHERE parent_id = " + id;

        return jdbcTemplate.query(sql,
                (rs, rowNum) -> {
                    CategoryTree categoryTree = new CategoryTree();
                    categoryTree.setCategoryId(rs.getInt("id"));
                    categoryTree.setParentId(rs.getInt("parent_id"));
                    categoryTree.setName(rs.getString("title"));
                    return categoryTree;
                });
    }
}

