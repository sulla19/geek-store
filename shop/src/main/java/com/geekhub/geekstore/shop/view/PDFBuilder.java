package com.geekhub.geekstore.shop.view;

import com.geekhub.core.model.customer.Address;
import com.geekhub.core.model.customer.Customer;
import com.geekhub.core.model.order.OrderDto;
import com.geekhub.core.model.order.OrderItem;
import com.geekhub.core.model.order.delivery.Delivery;
import com.geekhub.core.model.order.payment.CreditCard;
import com.itextpdf.text.*;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfWriter;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.List;
import java.util.Map;

public class PDFBuilder extends PdfView {

    @Override
    protected void buildPdfDocument(Map<String, Object> model, Document doc,
                                    PdfWriter writer, HttpServletRequest request, HttpServletResponse response)
            throws Exception {

        Font font = FontFactory.getFont(FontFactory.HELVETICA,
                18, Font.BOLDITALIC);

        Customer customer = (Customer) model.get("customer");

        doc.add(new Paragraph("Customer info :", font));
        doc.add(new Paragraph("Name : " + customer.getFirstName() + " " + customer.getLastName()));
        doc.add(new Paragraph("Gender : " + customer.getGender()));
        doc.add(new Paragraph("Birth date : " + customer.getBirthDate().toString()));
        doc.add(new Paragraph("Telephone : " + customer.getTelephone()));
        doc.add(new Paragraph("Email : " + customer.getEmailAddress()));

        Address address = (Address) model.get("address");

        doc.add(new Paragraph("Address :", font));
        doc.add(new Paragraph("Address :" + address.getCustomerCountry() + " " +
                address.getCustomerCity() + " " + address.getCustomerStreet() + " " +
                address.getCustomerPostcode() + "."));

        CreditCard creditCard = (CreditCard) model.get("creditCard");

        doc.add(new Paragraph("Credit card :", font));
        doc.add(new Paragraph(creditCard.getCreditCardType().toString() + " " + creditCard.getCcNumber() + " " +
                creditCard.getCcCvv()));

        OrderDto order = (OrderDto) model.get("order");
        Delivery delivery = (Delivery) model.get("delivery");

        doc.add(new Paragraph("Delivery :", font));
        doc.add(new Paragraph("Shipping type :" + order.getShippingType().toString()));
        doc.add(new Paragraph("Country : " + delivery.getCountry() + " " + delivery.getCity() + " " +
                delivery.getAddress() + " " + delivery.getPostalCode()));

        PdfPCell cell = new PdfPCell();
        cell.setBackgroundColor(BaseColor.LIGHT_GRAY);
        cell.setPadding(5);

        PdfPTable table = new PdfPTable(4);
        table.setWidthPercentage(100.0f);
        table.setWidths(new float[]{3.0f, 2.0f, 2.0f, 2.0f});
        table.setSpacingBefore(10);

        cell.setPhrase(new Phrase("Book Title"));
        table.addCell(cell);

        cell.setPhrase(new Phrase("Author"));
        table.addCell(cell);

        cell.setPhrase(new Phrase("ISBN"));
        table.addCell(cell);

        cell.setPhrase(new Phrase("Price"));
        table.addCell(cell);

        List<OrderItem> products = (List<OrderItem>) model.get("products");
        products.forEach(System.out::println);

        for (OrderItem aBook : products) {
            table.addCell(aBook.getProduct().getTitle());
            table.addCell(aBook.getProduct().getAuthor());
            table.addCell(aBook.getProduct().getIsbn());
            table.addCell(String.valueOf(aBook.getProduct().getPrice()));
        }
        doc.add(table);
    }
}