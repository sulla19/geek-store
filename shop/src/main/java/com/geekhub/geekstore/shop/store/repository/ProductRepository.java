package com.geekhub.geekstore.shop.store.repository;

import com.geekhub.core.model.catalog.product.ProductDto;
import com.geekhub.core.model.catalog.product.review.Review;
import com.geekhub.core.model.common.PriceRange;
import com.geekhub.core.model.common.SortCriteria;

import java.util.List;

public interface ProductRepository {

    List<ProductDto> getProductsByCategoryRecursively(Integer id, PriceRange priceRange, SortCriteria criteria);

    ProductDto getProductById(Integer id);

    void addReview(Review review);
}
