package com.geekhub.geekstore.shop.store.security.repository;

import com.geekhub.core.model.customer.Customer;
import com.geekhub.core.model.order.payment.CreditCard;
import com.geekhub.core.repository.mapsqlparametersources.CreditCardParameterSource;
import com.geekhub.core.repository.mapsqlparametersources.CustomerParameterSource;
import com.geekhub.core.repository.rowmappers.CustomerRowMapper;
import com.geekhub.geekstore.shop.store.security.model.Role;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.core.simple.SimpleJdbcInsert;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import javax.sql.DataSource;
import java.util.List;

@Repository
public class UserRepositoryImpl implements UserRepository {

    private JdbcTemplate jdbcTemplate;
    private SimpleJdbcInsert jdbcInsertUser;
    private SimpleJdbcInsert jdbcInsertCreditCard;
    private NamedParameterJdbcTemplate namedJdbcTemplate;


    @Autowired
    public UserRepositoryImpl(DataSource dataSource) {
        namedJdbcTemplate = new NamedParameterJdbcTemplate(dataSource);
        jdbcTemplate = new JdbcTemplate(dataSource);
        jdbcInsertUser = new SimpleJdbcInsert(dataSource)
                .withTableName("geek_store.customers")
                .usingColumns("email", "password", "first_name", "last_name", "gender", "birth_date", "telephone")
                .usingGeneratedKeyColumns("id");
        jdbcInsertCreditCard = new SimpleJdbcInsert(dataSource)
                .withTableName("geek_store.credit_card")
                .usingColumns("customer_id", "cc_type", "cc_number", "cc_cvv");
    }

    @Override
    public Customer findByEmail(String email) {
        String sql = "SELECT customers.id, first_name, last_name, gender, birth_date, telephone, customers.email, password" +
                " FROM geek_store.customers " +
                "INNER JOIN geek_store.roles " +
                "ON geek_store.roles.email = geek_store.customers.email " +
                "WHERE geek_store.customers.email= ?";
        List<Customer> query = jdbcTemplate.query(sql, new CustomerRowMapper(), email);

        if (query.isEmpty()) {
            return null;
        } else {
            return query.get(0);
        }
    }

    @Override
    @Transactional
    public Customer save(Customer user) {
        Number number = jdbcInsertUser.executeAndReturnKey(new CustomerParameterSource().getParameterSource(user));
        String sql = "INSERT " +
                "INTO geek_store.roles (email, role) " +
                "VALUES (?, ? )";
        jdbcTemplate.update(sql, user.getEmailAddress(), Role.USER.toString());
        int id = number.intValue();
        user.setId(id);

        CreditCard blankCreditCard = CreditCard.getBlankCreditCard(id);
        jdbcInsertCreditCard.execute(new CreditCardParameterSource().getParameterSource(blankCreditCard));
        return user;
    }


    @Override
    @Transactional
    public void updateUser(int id, Customer customer) {
        String sql = "UPDATE geek_store.customers " +
                "SET first_name=:firstName, last_name=:lastName, gender=:gender, birth_date=:birthDate, " +
                "telephone=:telephone, email=:email, password=:password" +
                " WHERE id=" + id;
        String sql1 = "UPDATE geek_store.credit_card " +
                "SET cc_type=:cc_type, cc_number=:cc_number, cc_cvv=:cc_cvv " +
                "WHERE customer_id=" + id;
        namedJdbcTemplate.update(sql, new CustomerParameterSource().getParameterSource(customer));
        namedJdbcTemplate.update(sql1, new CreditCardParameterSource().getParameterSource(customer.getCreditCard()));
    }
}
