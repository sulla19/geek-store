package com.geekhub.geekstore.shop.store.controller;

import com.geekhub.core.model.customer.Address;
import com.geekhub.core.model.customer.Customer;
import com.geekhub.core.model.order.OrderDto;
import com.geekhub.core.model.order.OrderItem;
import com.geekhub.core.model.order.delivery.Delivery;
import com.geekhub.core.model.order.payment.CreditCard;
import com.geekhub.geekstore.shop.store.security.util.SecurityUtil;
import com.geekhub.geekstore.shop.store.service.CustomerService;
import com.geekhub.geekstore.shop.store.service.OrderService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpSession;
import java.util.List;

@RestController
public class OrderController {

    private OrderService orderService;
    private CustomerService customerService;

    @Autowired
    public OrderController(OrderService orderService, CustomerService customerService) {
        this.orderService = orderService;
        this.customerService = customerService;
    }

    @GetMapping("/order")
    public ModelAndView toOrder() {
        return new ModelAndView("order");
    }

    @PostMapping("/order")
    public ResponseEntity<?> saveOrder(Customer customer, Address address, CreditCard creditCard,
                                       OrderDto orderDto, Delivery delivery, HttpSession session) {
        List<OrderItem> products = (List<OrderItem>) session.getAttribute("cart");

        customer.setCreditCard(creditCard);
        customer.setCustomerAddress(address);
        orderService.saveOrderByUsername(SecurityUtil.getLoggedUsername(), orderDto, delivery, products);
        customerService.updateCustomer(SecurityUtil.getLoggedUserId(), customer);

        return new ResponseEntity<>(HttpStatus.OK);
    }

    @GetMapping("/order/pdf")
    public ModelAndView getOrderReport(Model model, Customer customer, Address address, CreditCard creditCard,
                                       OrderDto orderDto, Delivery delivery, HttpSession session) {
        model.addAttribute("customer", customer);
        model.addAttribute("address", address);
        model.addAttribute("creditCard", creditCard);
        model.addAttribute("order", orderDto);
        model.addAttribute("delivery", delivery);
        model.addAttribute("products", session.getAttribute("cart"));

        return new ModelAndView("PDFBuilder", "model", model);
    }
}
