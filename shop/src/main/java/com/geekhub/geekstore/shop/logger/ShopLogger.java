package com.geekhub.geekstore.shop.logger;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.AfterReturning;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Pointcut;
import org.springframework.stereotype.Component;

@Component
@Aspect
public class ShopLogger {

    private Logger log = LogManager.getLogger(ShopLogger.class);

    @Pointcut("within(com.geekhub.geekstore.shop.store.controller.*)")
    private void controller() {
    }

    @Around("controller()")
    public Object logController(ProceedingJoinPoint joinPoint) throws Throwable {

        StringBuilder sb = new StringBuilder();
        sb.append("args : [");

        for (Object arg : joinPoint.getArgs()) {
            sb.append(arg.getClass().getSimpleName()).append("=\'").append(arg).append("\', ");
        }
        sb.append("]");

        log.fatal(" method :" + joinPoint.getSignature().toShortString() + " " + sb.toString());
        return joinPoint.proceed();
    }

    @AfterReturning(pointcut = "controller()", returning = "obj")
    public void resultController(Object obj) {
        System.out.println("returned : " + obj);
    }


    @Pointcut("within(com.geekhub.geekstore.shop.store.repository.*)")
    private void product() {
    }

    @Around("product()")
    public Object logging(ProceedingJoinPoint joinPoint) throws Throwable {

        StringBuilder sb = new StringBuilder();
        sb.append("args : [");

        for (Object arg : joinPoint.getArgs()) {
            sb.append(arg.getClass().getSimpleName()).append(" :").append(arg).append("\", ");
        }
        sb.append("]");

        log.fatal(" method :" + joinPoint.getSignature().toShortString() + " " + sb.toString());
        return joinPoint.proceed();
    }

    @AfterReturning(pointcut = "product()", returning = "obj")
    public void getResult(Object obj) {
        System.out.println("returned : " + obj);
    }
}
