package com.geekhub.geekstore.shop.store.controller;

import com.geekhub.geekstore.shop.store.security.util.SecurityUtil;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class ChatController {

    @GetMapping("chat/username")
    public ResponseEntity<?> getUsername() {
        return new ResponseEntity<Object>(SecurityUtil.getLoggedUsername(), HttpStatus.OK);
    }
}
