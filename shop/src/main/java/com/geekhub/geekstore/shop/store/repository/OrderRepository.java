package com.geekhub.geekstore.shop.store.repository;

import com.geekhub.core.model.order.Order;
import com.geekhub.core.model.order.delivery.Delivery;

public interface OrderRepository {

    Integer saveOrderByUsername(String username, Order order, Delivery delivery);
}
