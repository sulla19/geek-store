package com.geekhub.geekstore.shop.store.security.model;


public enum Role {

    USER, ADMIN, ACTUATOR
}
