package com.geekhub.geekstore.shop.store.metric;

import com.geekhub.geekstore.shop.store.security.util.SecurityUtil;
import org.springframework.stereotype.Component;
import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.sql.Date;
import java.time.LocalDate;

@Component
public class MetricInterceptor extends HandlerInterceptorAdapter {

    private MetricRepository metricRepository;

    public MetricInterceptor(MetricRepository metricRepository) {
        this.metricRepository = metricRepository;
    }

    @Override
    public void afterCompletion(HttpServletRequest request, HttpServletResponse response, Object handler, Exception ex) throws Exception {
        Integer currentUserId = SecurityUtil.getLoggedUserId();
        Integer arg = (Integer) request.getAttribute("id");

        if (currentUserId != null && arg != null) {
            metricRepository.saveView(arg, currentUserId, Date.valueOf(LocalDate.now()));
        }
    }
}
