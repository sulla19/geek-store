package com.geekhub.geekstore.shop.store.controller;

import com.geekhub.core.model.order.OrderItem;
import com.geekhub.geekstore.shop.store.service.ShoppingCardService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.SessionAttributes;

import javax.servlet.http.HttpSession;
import java.util.Collections;
import java.util.List;

@RestController
@SessionAttributes("products")
public class ShoppingCartController {

    private ShoppingCardService cardService;

    @Autowired
    public ShoppingCartController(ShoppingCardService cardService) {
        this.cardService = cardService;
    }

    @GetMapping("/cart/all")
    public ResponseEntity<?> getCartProducts(HttpSession httpSession) {

        List<OrderItem> products = (List<OrderItem>) httpSession.getAttribute("cart");

        if (products == null) {
            return new ResponseEntity<>(Collections.emptyList(), HttpStatus.OK);
        } else {
            return new ResponseEntity<>(products, HttpStatus.OK);
        }
    }

    @GetMapping("/cart/add/{id}")
    public ResponseEntity<?> addItem(HttpSession session, @PathVariable Integer id) {

        List<OrderItem> products = cardService.addItem((List<OrderItem>) session.getAttribute("cart"), id);
        session.setAttribute("cart", products);

        return new ResponseEntity<>(products, HttpStatus.OK);
    }

    @GetMapping("/cart/remove/{id}")
    public ResponseEntity<?> removeFromCard(HttpSession httpSession, @PathVariable Integer id) {

        List<OrderItem> removed = cardService.removeItem((List<OrderItem>) httpSession.getAttribute("cart"), id);
        httpSession.setAttribute("cart", removed);
        return new ResponseEntity<>(removed, HttpStatus.OK);
    }
}
