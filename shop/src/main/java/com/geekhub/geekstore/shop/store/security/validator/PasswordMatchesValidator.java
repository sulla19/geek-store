package com.geekhub.geekstore.shop.store.security.validator;

import com.geekhub.geekstore.shop.store.security.model.UserCreateForm;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

public class PasswordMatchesValidator
        implements ConstraintValidator<PasswordMatches, Object> {

    @Override
    public void initialize(PasswordMatches constraintAnnotation) {
    }

    @Override
    public boolean isValid(Object obj, ConstraintValidatorContext context) {
        UserCreateForm user = (UserCreateForm) obj;
        return user.getPassword().equals(user.getPasswordRepeated());
    }
}
