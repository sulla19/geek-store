package com.geekhub.geekstore.shop.store.service;

import com.geekhub.core.model.catalog.product.ProductSearchResult;

import java.util.List;

public interface SearchService {

    List<ProductSearchResult> searchProduct(String text);
}
