package com.geekhub.geekstore.shop.store.security.validator;

import com.geekhub.geekstore.shop.store.security.model.UserCreateForm;
import com.geekhub.geekstore.shop.store.security.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;

@Component
public class UserValidator implements Validator {

    private UserService userService;

    @Autowired
    public UserValidator(UserService userService) {
        this.userService = userService;
    }

    @Override
    public boolean supports(Class<?> aClass) {
        return UserCreateForm.class.equals(aClass);
    }

    @Override
    public void validate(Object o, Errors errors) {
        UserCreateForm user = (UserCreateForm) o;

        if (userService.getUserByEmail(user.getEmail()) != null) {
            errors.rejectValue("email", "Duplicate.userForm.username", "This user already exist.");
        }

        if (user.getPassword().length() < 6 || user.getPassword().length() > 32) {
            errors.rejectValue("password", "Size.userForm.password", "Password must be over 6 characters.");
        }
    }
}
