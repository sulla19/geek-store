package com.geekhub.geekstore.shop.store.statistic.repository;

import com.geekhub.core.model.catalog.product.ProductDto;
import com.geekhub.core.model.common.PriceRange;
import com.geekhub.core.model.common.ReportPeriod;

import java.util.List;

public interface StatisticRepository {

    List<ProductDto> getBestSellingProductByPeriodInCategory(Integer categoryId, ReportPeriod fromDate, Integer limit, PriceRange priceRange);

    List<ProductDto> getMostViewedProductByPeriodInCategory(Integer categoryId, ReportPeriod fromDate, Integer limit, PriceRange priceRange);

    List<ProductDto> getMostReviewedProductsInCategory(Integer categoryId, int limit, PriceRange priceRange);
}
