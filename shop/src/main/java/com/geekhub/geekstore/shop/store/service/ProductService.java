package com.geekhub.geekstore.shop.store.service;

import com.geekhub.core.model.catalog.product.ProductDto;
import com.geekhub.core.model.catalog.product.review.Review;
import com.geekhub.core.model.common.PriceRange;
import com.geekhub.core.model.common.SortCriteria;

import java.util.List;

public interface ProductService {

    List<ProductDto> getProductsByRequest(Integer id, PriceRange priceRange, SortCriteria sortCriteria);

    List<ProductDto> getProductsByCategory(Integer id, PriceRange priceRange, SortCriteria criteria);

    ProductDto getProductById(Integer id);

    PriceRange getPriceRangeByCategory(Integer id);

    void addReview(Review review);
}
