package com.geekhub.geekstore.shop.store.repository;

import com.geekhub.core.model.catalog.category.Category;
import com.geekhub.core.model.catalog.category.CategoryTree;

import java.util.List;

public interface CategoryRepository {

    Category find(Integer id);

    String getCategoryName(Integer id);

    List<CategoryTree> getCategoryTree(Integer id);
}
