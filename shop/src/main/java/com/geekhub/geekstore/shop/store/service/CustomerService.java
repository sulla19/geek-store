package com.geekhub.geekstore.shop.store.service;

import com.geekhub.core.model.customer.Customer;

public interface CustomerService {

    void updateCustomer(Integer id, Customer customer);
}
