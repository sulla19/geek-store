package com.geekhub.geekstore.shop.store.security.service;

import com.geekhub.core.model.customer.Customer;
import com.geekhub.geekstore.shop.store.security.model.UserCreateForm;

public interface UserService {

    Customer getUserByEmail(String email) ;

    Customer create(UserCreateForm form);

    void autoLogin(String username, String password);
}
