package com.geekhub.geekstore.shop.store.security.model;

import com.geekhub.core.model.customer.Customer;
import org.springframework.security.core.authority.AuthorityUtils;

public class CurrentUser extends org.springframework.security.core.userdetails.User {

    private Customer user;

    public CurrentUser(Customer user) {
        super(user.getEmailAddress(), user.getPassword(), AuthorityUtils.createAuthorityList(Role.USER.toString()));
        this.user = user;
    }

    public Customer getUser() {
        return user;
    }

    public Integer getId() {
        return user.getId();
    }
}
