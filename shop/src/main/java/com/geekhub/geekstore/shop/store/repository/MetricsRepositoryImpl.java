package com.geekhub.geekstore.shop.store.repository;

import com.geekhub.core.model.common.PriceRange;
import com.geekhub.core.repository.rowmappers.PriceRangeRowMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import javax.sql.DataSource;

@Repository
public class MetricsRepositoryImpl implements MetricsRepository {

    private JdbcTemplate jdbcTemplate;

    @Autowired
    public MetricsRepositoryImpl(DataSource dataSource) {
        this.jdbcTemplate = new JdbcTemplate(dataSource);
    }

    @Override
    public PriceRange getPriceRangeByCategory(Integer id) {

        String sql = "WITH RECURSIVE r AS (" +
                "SELECT " +
                "geek_store.category.id " +
                "FROM geek_store.category " +
                "WHERE geek_store.category.id= " + id +
                " UNION ALL " +
                "SELECT " +
                "geek_store.category.id " +
                "FROM geek_store.category " +
                "JOIN r " +
                "ON geek_store.category.parent_id = r.id) " +
                "SELECT min(price) , MAX(price) " +
                " FROM r INNER JOIN geek_store.products ON geek_store.products.category_id=r.id";

        return jdbcTemplate.queryForObject(sql, new PriceRangeRowMapper());
    }
}
