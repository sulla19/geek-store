package com.geekhub.geekstore.shop.store.repository;

import com.geekhub.core.model.catalog.product.ProductDto;
import com.geekhub.core.model.catalog.product.review.Review;
import com.geekhub.core.model.common.PriceRange;
import com.geekhub.core.model.common.SortCriteria;
import com.geekhub.core.repository.mapsqlparametersources.ReviewParameterSource;
import com.geekhub.core.repository.rowmappers.ProductDtoRowMapper;
import com.geekhub.core.repository.rowmappers.ProductDtoWithReviewsRowMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.simple.SimpleJdbcInsert;
import org.springframework.stereotype.Repository;

import javax.sql.DataSource;
import java.util.List;

@Repository
public class ProductRepositoryImpl implements ProductRepository {

    private JdbcTemplate jdbcTemplate;
    private SimpleJdbcInsert jdbcInsertReview;

    @Autowired
    public ProductRepositoryImpl(DataSource dataSource) {
        this.jdbcTemplate = new JdbcTemplate(dataSource);
        this.jdbcInsertReview = new SimpleJdbcInsert(dataSource)
                .withTableName("geek_store.reviews")
                .usingColumns("date", "rating", "advantages", "disadvantages", "customer_id",
                        "product_id", "comment");
    }

    @Override
    public List<ProductDto> getProductsByCategoryRecursively(Integer id, PriceRange priceRange, SortCriteria criteria) {

        String sql = "WITH temp AS (WITH RECURSIVE r AS (" +
                "  SELECT geek_store.category.id AS cat_id " +
                "  FROM geek_store.category " +
                "  WHERE geek_store.category.id = ? " +
                "  UNION ALL " +
                "  SELECT geek_store.category.id " +
                "  FROM geek_store.category " +
                "    JOIN r " +
                "      ON geek_store.category.parent_id = r.cat_id) " +
                "SELECT " +
                "  r.cat_id, " +
                "  products.id AS prod_id, price AS prod_price " +
                "FROM r " +
                "  INNER JOIN geek_store.products " +
                "    ON geek_store.products.category_id = r.cat_id) " +
                "SELECT " +
                "  prod_id AS product_id, prod_price AS price, " +
                "  title, author, src_image " +
                "FROM temp " +
                "INNER JOIN geek_store.product_descriptions " +
                "  ON geek_store.product_descriptions.product_id = temp.prod_id " +
                "INNER JOIN geek_store.product_image " +
                "  ON geek_store.product_image.product_id = temp.prod_id " +
                "WHERE prod_price >= ? AND prod_price <= ? " +
                " ORDER BY " + criteria.getValue();

        return jdbcTemplate.query(sql, new ProductDtoRowMapper(), id, priceRange.getMinPrice(), priceRange.getMaxPrice());
    }

    @Override
    public ProductDto getProductById(Integer id) {

        String sql = "WITH temp AS (SELECT " +
                "  products.id AS prod_id,  type , weight,  height,  width,  length, price, " +
                "  product_image.src_image, " +
                "  product_descriptions.title AS prod_title, author, isbn, product_descriptions.description, " +
                "  category.id AS cat_id, category_descriptions.title AS cat_title " +
                "FROM geek_store.products " +
                "INNER JOIN geek_store.product_image " +
                "  ON geek_store.products.id = geek_store.product_image.product_id " +
                "INNER JOIN geek_store.product_descriptions " +
                "  ON geek_store.products.id = geek_store.product_descriptions.product_id " +
                "INNER JOIN geek_store.category " +
                "  ON geek_store.products.category_id = geek_store.category.id " +
                "LEFT JOIN geek_store.category_descriptions " +
                "  ON geek_store.category.id = geek_store.category_descriptions.category_id " +
                "WHERE products.id = ?)" +
                "SELECT " +
                "  DISTINCT (prod_id),  type , weight,  height,  width,  length, price, " +
                "  src_image, " +
                "  prod_title, author, description, isbn," +
                "  cat_id, cat_title, " +
                "  reviews.id, reviews.date, rating, advantages, disadvantages, comment, customer_id " +
                "FROM temp " +
                " LEFT JOIN  geek_store.reviews " +
                "  ON prod_id = geek_store.reviews.product_id";

        return jdbcTemplate.queryForObject(sql, new ProductDtoWithReviewsRowMapper(), id);
    }

    @Override
    public void addReview(Review review) {
        jdbcInsertReview.execute(new ReviewParameterSource().getParameterSource(review));
    }
}
