package com.geekhub.geekstore.shop.store.security.service;

import com.geekhub.core.model.customer.Customer;
import com.geekhub.geekstore.shop.store.security.model.CurrentUser;
import com.geekhub.geekstore.shop.store.security.model.UserCreateForm;
import com.geekhub.geekstore.shop.store.security.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

@Service
public class UserServiceImpl implements UserService {

    private UserRepository userRepository;

    @Autowired
    private AuthenticationManager authenticationManager;

    @Autowired
    private UserDetailsServiceImpl detailsService;

    @Autowired
    public UserServiceImpl(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    @Override
    public Customer getUserByEmail(String email) {
        return userRepository.findByEmail(email);
    }

    @Override
    public Customer create(UserCreateForm form) {
        Customer user = new Customer();
        user.setEmailAddress(form.getEmail());
        user.setPassword(new BCryptPasswordEncoder().encode(form.getPassword()));

        user.setFirstName(form.getFirstName());
        user.setLastName(form.getLastName());
        user.setGender(form.getGender());
        user.setBirthDate(form.getBirthDate());
        user.setTelephone(form.getTelephone());

        return userRepository.save(user);
    }

    @Override
    public void autoLogin(String username, String password) {
        CurrentUser userDetails = detailsService.loadUserByUsername(username);

        UsernamePasswordAuthenticationToken authenticationToken =
                new UsernamePasswordAuthenticationToken(userDetails, userDetails.getPassword(), userDetails.getAuthorities());

        authenticationManager.authenticate(authenticationToken);

        if (authenticationToken.isAuthenticated()) {
            SecurityContextHolder.getContext().setAuthentication(authenticationToken);

            System.out.println(String.format("Successfully %s auto logged in", username));
        }
    }
}
