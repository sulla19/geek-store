package com.geekhub.geekstore.shop.store.service;

import com.geekhub.core.model.catalog.product.ProductSearchResult;
import com.geekhub.core.search.SearchRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class SearchServiceImpl implements SearchService {

    private SearchRepository searchRepository;

    @Autowired
    public SearchServiceImpl(SearchRepository searchRepository) {
        this.searchRepository = searchRepository;
    }

    @Override
    public List<ProductSearchResult> searchProduct(String text) {
        return searchRepository.searchProduct(text);
    }
}
