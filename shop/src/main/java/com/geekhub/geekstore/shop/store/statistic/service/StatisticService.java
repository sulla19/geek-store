package com.geekhub.geekstore.shop.store.statistic.service;

import com.geekhub.core.model.catalog.product.ProductDto;
import com.geekhub.core.model.common.PriceRange;
import com.geekhub.core.model.common.ReportPeriod;

import java.util.List;

public interface StatisticService {

    List<ProductDto> getBestSellingProducts(Integer categoryId, ReportPeriod fromDate, Integer limit, PriceRange priceRange);

    List<ProductDto> getMostViewedProductsInCategory(Integer categoryId, ReportPeriod fromDate, Integer limit, PriceRange priceRange);

    List<ProductDto> getMostReviewedProducts(Integer id, int limit, PriceRange priceRange);
}
