package com.geekhub.geekstore.shop.store.service;

import com.geekhub.core.model.customer.Customer;
import com.geekhub.geekstore.shop.store.security.repository.UserRepository;
import com.geekhub.geekstore.shop.store.security.util.SecurityUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class CustomerServiceImpl implements CustomerService {

    private UserRepository userRepository;

    @Autowired
    public CustomerServiceImpl(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    @Override
    public void updateCustomer(Integer id, Customer customer) {
        System.out.println("in service password is " + SecurityUtil.getLoggedPassword());
        customer.setPassword(SecurityUtil.getLoggedPassword());
        System.out.println("customer " + id + " " + customer);
        userRepository.updateUser(id, customer);
    }
}
