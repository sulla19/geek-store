package com.geekhub.geekstore.shop.store.service;

import com.geekhub.core.model.order.OrderItem;
import com.geekhub.geekstore.shop.store.repository.ProductRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.*;

@Service
public class ShoppingCardServiceImpl implements ShoppingCardService {

    private ProductRepository productRepository;

    @Autowired
    public ShoppingCardServiceImpl(ProductRepository productRepository) {
        this.productRepository = productRepository;
    }

    @Override
    public List<OrderItem> addItem(List<OrderItem> products, Integer productId) {

        if (products == null) {
            List<OrderItem> items = new ArrayList<>();
            items.add(addOrderItem(productId));
            return items;
        } else {
            Optional<OrderItem> item = products.stream()
                    .filter(x -> Objects.equals(x.getProduct().getProductId(), productId))
                    .findFirst();

            if (item.isPresent()) {
                item.get().incrementCount();
            } else {
                products.add(addOrderItem(productId));
            }
            return products;
        }
    }

    @Override
    public List<OrderItem> removeItem(List<OrderItem> products, Integer productId) {

        if (productId == 0) {
            return new ArrayList<>();
        }

        products.stream()
                .filter(x -> Objects.equals(x.getProduct().getProductId(), productId))
                .findFirst()
                .ifPresent(OrderItem::decrementCount);

         products.removeIf(x -> Objects.equals(x.getCount(), 0));

        return products.size() == 0 ? new ArrayList<>() : products;
    }

    private OrderItem addOrderItem(Integer productId) {
        OrderItem orderItem = new OrderItem();
        orderItem.setProduct(productRepository.getProductById(productId));
        orderItem.incrementCount();
        return orderItem;
    }
}
