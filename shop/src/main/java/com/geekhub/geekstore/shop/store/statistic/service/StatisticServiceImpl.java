package com.geekhub.geekstore.shop.store.statistic.service;

import com.geekhub.core.model.catalog.product.ProductDto;
import com.geekhub.core.model.common.PriceRange;
import com.geekhub.core.model.common.ReportPeriod;
import com.geekhub.geekstore.shop.store.statistic.repository.StatisticRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class StatisticServiceImpl implements StatisticService {

    private StatisticRepository statisticRepository;

    @Autowired
    public StatisticServiceImpl(StatisticRepository statisticRepository) {
        this.statisticRepository = statisticRepository;
    }

    @Override
    public List<ProductDto> getBestSellingProducts(Integer categoryId, ReportPeriod fromDate, Integer limit, PriceRange priceRange) {
        return statisticRepository.getBestSellingProductByPeriodInCategory(categoryId, fromDate, limit, priceRange);
    }

    @Override
    public List<ProductDto> getMostViewedProductsInCategory(Integer categoryId, ReportPeriod fromDate, Integer limit, PriceRange priceRange) {
        return statisticRepository.getMostViewedProductByPeriodInCategory(categoryId, fromDate, limit, priceRange);
    }

    @Override
    public List<ProductDto> getMostReviewedProducts(Integer id, int limit, PriceRange priceRange) {
        return statisticRepository.getMostReviewedProductsInCategory(id, limit, priceRange);
    }
}
