package com.geekhub.geekstore.shop.store.controller;

import com.geekhub.core.model.common.PriceRange;
import com.geekhub.core.model.common.SortCriteria;
import com.geekhub.core.web.pagination.PageRequest;
import com.geekhub.geekstore.shop.store.service.CategoryService;
import com.geekhub.geekstore.shop.store.service.ProductService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

import java.util.HashMap;
import java.util.Map;

import static com.geekhub.core.web.pagination.PaginationUtils.getPage;

@RestController
public class CategoryController {

    private CategoryService categoryService;
    private ProductService productService;

    @Autowired
    public CategoryController(CategoryService categoryService, ProductService productService) {
        this.categoryService = categoryService;
        this.productService = productService;
    }

    @GetMapping("/category/{id}")
    public ResponseEntity<?> getCategoryPage(@PathVariable Integer id, PageRequest pageRequest, PriceRange priceRange, SortCriteria sort) {
        return new ResponseEntity<>(getPage(productService.getProductsByRequest(id, priceRange, sort), pageRequest), HttpStatus.OK);
    }

    @GetMapping("/categories/{id}")
    public ResponseEntity<?> getCategoryTree(@PathVariable Integer id) {
        return new ResponseEntity<>(categoryService.getCategoryTree(id), HttpStatus.OK);
    }

    @GetMapping("/category/{id}/range")
    public ResponseEntity<?> getPageAttributes(@PathVariable Integer id) {
        Map<String, Object> pageAttributes = new HashMap<>();
        pageAttributes.put("priceRange", productService.getPriceRangeByCategory(id));
        pageAttributes.put("categoryName", categoryService.getCategoryName(id));
        return new ResponseEntity<>(pageAttributes, HttpStatus.OK);
    }
}
