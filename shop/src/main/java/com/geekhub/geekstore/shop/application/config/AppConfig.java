package com.geekhub.geekstore.shop.application.config;

import com.geekhub.core.search.SearchRepository;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import javax.sql.DataSource;

@Configuration
public class AppConfig {

    @Bean
    public SearchRepository getSearchRepository(DataSource dataSource) {
        return new SearchRepository(dataSource);
    }
}
