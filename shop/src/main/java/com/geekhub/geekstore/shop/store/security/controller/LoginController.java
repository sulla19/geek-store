package com.geekhub.geekstore.shop.store.security.controller;

import com.geekhub.geekstore.shop.store.security.model.UserCreateForm;
import com.geekhub.geekstore.shop.store.security.service.UserService;
import com.geekhub.geekstore.shop.store.security.validator.UserValidator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.validation.Errors;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;

import javax.validation.Valid;

@RestController
public class LoginController {

    private UserService userService;
    private UserValidator validator;

    @Autowired
    public LoginController(UserService userService, UserValidator validator) {
        this.userService = userService;
        this.validator = validator;
    }

    @GetMapping("/user/create")
    public ModelAndView getUserCreatePage() {
        return new ModelAndView("user_create", "form", new UserCreateForm());
    }

    @PostMapping("/user/register")
    public ResponseEntity<?> handleUserCreateForm(@Valid UserCreateForm form, BindingResult bindingResult,
                                                  Errors errors) {
        validator.validate(form, bindingResult);

        if (bindingResult.hasErrors()) {
            return new ResponseEntity(errors.getAllErrors().get(0).getDefaultMessage(), HttpStatus.UNAUTHORIZED);
        }

        userService.create(form);
        userService.autoLogin(form.getEmail(), form.getPasswordRepeated());
        return new ResponseEntity(HttpStatus.OK);
    }
}