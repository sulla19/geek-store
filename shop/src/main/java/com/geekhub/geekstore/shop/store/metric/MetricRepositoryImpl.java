package com.geekhub.geekstore.shop.store.metric;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import javax.sql.DataSource;
import java.sql.Date;
import java.util.List;
import java.util.Map;

@Repository
public class MetricRepositoryImpl implements MetricRepository {

    private JdbcTemplate jdbcTemplate;

    @Autowired
    public MetricRepositoryImpl(DataSource dataSource) {
        jdbcTemplate = new JdbcTemplate(dataSource);
    }

    @Override
    public void saveView(Integer productId, Integer customerId, Date date) {
        jdbcTemplate.update("INSERT INTO geek_store.product_metric(product_id, cutomer_id, view_date) " +
                "VALUES (?,?,?)", productId, customerId, date);
    }

    @Override
    public void updateCount(Integer productId, Integer count, Integer customerId) {
        jdbcTemplate.update("" +
                "UPDATE geek_store.product_metric SET view_count=" + count + " " +
                "WHERE product_id=" + productId + " AND cutomer_id=" + customerId);
    }

    @Override
    public Integer getCount(Integer id, Integer customerId) {

        List<Integer> list = jdbcTemplate.queryForList("SELECT view_count FROM geek_store.product_metric " +
                        "WHERE product_id=" + id + " AND cutomer_id=" + customerId,
                Integer.class);

        if (list.isEmpty()) {
            return 0;
        } else {
            return list.get(0);
        }
    }

    @Override
    public Map<String, Object> findAll() {

        return jdbcTemplate.queryForMap("SELECT  product_id, view_count FROM geek_store.product_metric");
    }
}
