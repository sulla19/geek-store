package com.geekhub.geekstore.shop.store.repository;

import com.geekhub.core.model.catalog.product.Product;
import com.geekhub.core.model.order.Order;
import com.geekhub.core.model.order.delivery.Delivery;
import com.geekhub.core.repository.mapsqlparametersources.DeliveryParameterSource;
import com.geekhub.core.repository.mapsqlparametersources.OrderParameterSource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.simple.SimpleJdbcInsert;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import javax.sql.DataSource;
import java.util.List;

@Repository
public class OrderRepositoryImpl implements OrderRepository {

    private JdbcTemplate jdbcTemplate;
    private SimpleJdbcInsert jdbcInsertOrder;
    private SimpleJdbcInsert jdbcInsertDelivery;

    @Autowired
    public OrderRepositoryImpl(DataSource dataSource) {
        this.jdbcTemplate = new JdbcTemplate(dataSource);
        jdbcInsertOrder = new SimpleJdbcInsert(dataSource)
                .withTableName("geek_store.orders")
                .usingColumns("last_modified", "customer_id", "total_price",
                        "payment_type", "shipping_type", "contact_telephone")
                .usingGeneratedKeyColumns("id");
        jdbcInsertDelivery = new SimpleJdbcInsert(dataSource)
                .withTableName("geek_store.delivery")
                .usingColumns("order_id", "company", "country", "city", "address", "postal_code", "telephone");
    }

    @Override
    @Transactional
    public Integer saveOrderByUsername(String username, Order order, Delivery delivery) {
        order.getProducts().forEach(System.out::println);

        Number orderId = jdbcInsertOrder.executeAndReturnKey(new OrderParameterSource().getParameterSource(order));

        saveProductList(orderId.intValue(), order.getProducts());
        delivery.setOrderId(orderId.intValue());
        jdbcInsertDelivery.execute(new DeliveryParameterSource().getParameterSource(delivery));

        return orderId.intValue();
    }

    private void saveProductList(Integer orderId, List<Product> products) {

        for (Product product : products) {
            jdbcTemplate.update("INSERT " +
                            "INTO geek_store.orders_products (product_id, order_id) " +
                            "VALUES (?,?)",
                    product.getId(), orderId);
        }
    }
}
