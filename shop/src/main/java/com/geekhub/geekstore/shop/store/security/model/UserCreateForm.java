package com.geekhub.geekstore.shop.store.security.model;

import com.geekhub.core.model.customer.CustomerGender;
import com.geekhub.geekstore.shop.store.security.validator.PasswordMatches;
import com.geekhub.geekstore.shop.store.security.validator.ValidEmail;
import lombok.Data;

import java.time.LocalDate;

@Data
@PasswordMatches
public class UserCreateForm {

    @ValidEmail
    private String email;
    private String password;
    private String passwordRepeated;
    private Role role;
    private String firstName;
    private String lastName;
    private LocalDate birthDate;
    private CustomerGender gender;
    private String telephone;

    public UserCreateForm() {
        this.email = "none";
        this.password = "none";
        this.passwordRepeated = "none";
        this.role = Role.USER;
        this.firstName = "none";
        this.lastName = "none";
        this.birthDate = LocalDate.now();
        this.gender = CustomerGender.NONE;
        this.telephone = "none";
    }
}
