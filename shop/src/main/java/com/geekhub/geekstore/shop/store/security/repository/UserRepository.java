package com.geekhub.geekstore.shop.store.security.repository;

import com.geekhub.core.model.customer.Customer;

public interface UserRepository {

    Customer findByEmail(String email) ;

    void updateUser(int id, Customer customer);

    Customer save(Customer user);
}