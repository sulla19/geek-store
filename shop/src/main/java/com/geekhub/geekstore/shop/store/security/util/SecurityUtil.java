package com.geekhub.geekstore.shop.store.security.util;

import com.geekhub.geekstore.shop.store.security.model.CurrentUser;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;

public class SecurityUtil {

    public static Authentication getAuthentication() {
        return SecurityContextHolder.getContext().getAuthentication();
    }

    public static String getLoggedUsername() {
        return getAuthentication().getName();
    }

    public static Integer getLoggedUserId() {
        if (isLogged()) {
            CurrentUser currentUser = (CurrentUser) getAuthentication().getPrincipal();
            return currentUser.getId();
        }
        return null;
    }

    public static String getLoggedPassword() {
        if (isLogged()) {
            CurrentUser currentUser = (CurrentUser) getAuthentication().getPrincipal();
            return currentUser.getUser().getPassword();
        }
        return null;
    }

    public static boolean isLogged() {
        return !getAuthentication().getName().equals("anonymousUser");
    }
}
