package com.geekhub.geekstore.shop.store.metric;

import java.sql.Date;
import java.util.Map;

public interface MetricRepository {

    void saveView(Integer productId, Integer customerId, Date date);

    void updateCount(Integer productId, Integer count, Integer customerId);

    Integer getCount(Integer id, Integer customerId);

    Map<String, Object> findAll();
}
