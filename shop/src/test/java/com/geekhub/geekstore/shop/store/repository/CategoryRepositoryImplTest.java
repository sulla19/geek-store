package com.geekhub.geekstore.shop.store.repository;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@SpringBootTest
public class CategoryRepositoryImplTest {

    @Autowired
    private CategoryRepository categoryRepository;



    @Test
    public void findAllCategories() throws Exception {
        categoryRepository.getCategoryTree(0).forEach(System.out::println);
    }

}