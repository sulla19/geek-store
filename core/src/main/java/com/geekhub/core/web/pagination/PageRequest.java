package com.geekhub.core.web.pagination;

import lombok.Data;

@Data
public class PageRequest {
    private int pageId;
    private int perPage;

    public PageRequest() {
        this.pageId = 1;
        this.perPage = 18;
    }

    public int getPageId() {
        if (this.pageId == 0) {
            this.pageId = 1;
        }
        return pageId;
    }
}
