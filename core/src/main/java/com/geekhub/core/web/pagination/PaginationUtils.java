package com.geekhub.core.web.pagination;

import com.geekhub.core.model.generic.BaseEntity;

import java.util.List;

public class PaginationUtils {

    public static <T extends BaseEntity> Page<T> getPage(List<T> items, PageRequest pageRequest) {
        int allItemsCount = items.size();

        int pageId = pageRequest.getPageId();
        int perPage = pageRequest.getPerPage();

        int allPageCount = getAllPageCount(allItemsCount, perPage);
        int startItemId = getStartItemId(pageId, perPage);

        int endItemId = getEndItemId(allPageCount, pageId, allItemsCount, perPage, startItemId);

        List<T> currentPageItems;
        if (endItemId > items.size()) {
            currentPageItems = items.subList(startItemId, items.size());
        } else {
            currentPageItems = items.subList(startItemId, endItemId);
        }
        return new Page<T>(pageId, perPage, allPageCount, currentPageItems);
    }

    private static int getAllPageCount(int allItemsCount, int perPage) {
        int allPageCount = 1;

        if (allItemsCount % perPage > 0) {
            allPageCount = allItemsCount / perPage + 1;
        } else if (allItemsCount % perPage == 0) {
            allPageCount = allItemsCount / perPage;
        }
        return allPageCount;
    }

    private static int getStartItemId(int pageId, int perPage) {
        int startItemId = 0;
        if (pageId > 1) {
            startItemId = (pageId - 1) * perPage;
        }
        return startItemId;
    }

    private static int getEndItemId(int allPageCount, int pageId, int allItemsCount, int perPage, int startItemId) {
        int endItemId;
        if (allPageCount == pageId) {
            if (allItemsCount % perPage == 0) {
                endItemId = pageId * perPage;
            } else {
                endItemId = startItemId + (allItemsCount % perPage);
            }
        } else {
            endItemId = startItemId + perPage;
        }
        return endItemId;
    }
}
