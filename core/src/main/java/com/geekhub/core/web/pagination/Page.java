package com.geekhub.core.web.pagination;

import lombok.Data;

import java.util.List;

@Data
public class Page<T> {

    private final int page;
    private final int perPage;
    private final int pageCount;
    private final List<T> items;

    public Page(int page, int perPage, int pageCount, List<T> items) {
        this.page = page;
        this.perPage = perPage;
        this.pageCount = pageCount;
        this.items = items;
    }
}
