package com.geekhub.core.search;

import com.geekhub.core.model.catalog.product.ProductSearchResult;
import com.geekhub.core.model.customer.Customer;
import com.geekhub.core.repository.rowmappers.CustomerRowMapper;
import com.geekhub.core.repository.rowmappers.ProductSearchResultRowMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import javax.sql.DataSource;
import java.util.List;

@Repository
public class SearchRepository {
    private JdbcTemplate jdbcTemplate;

    @Autowired
    public SearchRepository(DataSource dataSource) {
        jdbcTemplate = new JdbcTemplate(dataSource);
    }

    public List<Customer> searchCustomer(String text) {
        String s = text.toLowerCase() + "%";

        return jdbcTemplate.query("SELECT * " +
                        "FROM geek_store.customers " +
                        "WHERE lower(first_name) LIKE ? OR lower(last_name) LIKE ?",
                new CustomerRowMapper(), s, s);
    }

    public List<ProductSearchResult> searchProduct(String text) {
        String s = "%" + text.toLowerCase() + "%";
        System.out.println(" s " + s);

        String sql = "SELECT products.id, title, author, src_image " +
                " FROM geek_store.products " +
                "INNER JOIN geek_store.product_descriptions\n" +
                "   ON geek_store.products.id = geek_store.product_descriptions.product_id " +
                "INNER JOIN geek_store.product_image " +
                "   ON geek_store.products.id = geek_store.product_image.product_id " +
                "WHERE lower(product_descriptions.title) LIKE ? " +
                "      OR lower(product_descriptions.isbn) LIKE ?";

        return jdbcTemplate.query(sql, new ProductSearchResultRowMapper(), s, s);
    }
}
