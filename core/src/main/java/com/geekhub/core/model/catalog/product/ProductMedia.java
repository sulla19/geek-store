package com.geekhub.core.model.catalog.product;

import com.geekhub.core.model.generic.BaseEntity;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.util.List;

@EqualsAndHashCode(callSuper = true)
@Data
public class ProductMedia extends BaseEntity {

    private Integer productId;
    private List<String> srcImages;
}
