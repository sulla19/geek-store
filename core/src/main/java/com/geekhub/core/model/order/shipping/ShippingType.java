package com.geekhub.core.model.order.shipping;

public enum ShippingType {

    NATIONAL, INTERNATIONAL
}
