package com.geekhub.core.model.order;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.datatype.jsr310.ser.LocalDateSerializer;
import com.geekhub.core.model.catalog.product.Product;
import com.geekhub.core.model.generic.BaseEntity;
import com.geekhub.core.model.order.delivery.Delivery;
import com.geekhub.core.model.order.payment.PaymentType;
import com.geekhub.core.model.order.shipping.ShippingType;
import lombok.Data;
import lombok.EqualsAndHashCode;
import org.springframework.format.annotation.DateTimeFormat;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.List;

@EqualsAndHashCode(callSuper = true)
@Data
public class Order extends BaseEntity {

    @DateTimeFormat(iso = DateTimeFormat.ISO.DATE)
    @JsonSerialize(using = LocalDateSerializer.class)
    private LocalDate lastModified;
    private Integer customerId;
    private List<Product> products;
    private BigDecimal totalPrice;
    private PaymentType paymentType;
    private ShippingType shippingType;
    private Delivery delivery;
    private String contactTelephone;
}
