package com.geekhub.core.model.catalog.category;

import com.geekhub.core.model.common.Description;
import lombok.Data;
import lombok.EqualsAndHashCode;

@EqualsAndHashCode(callSuper = true)
@Data
public class CategoryDescription extends Description {
}
