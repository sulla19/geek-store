package com.geekhub.core.model.order;

import com.geekhub.core.model.catalog.product.ProductDto;
import lombok.Data;

@Data
public class OrderItem {

    private ProductDto product;
    private int count;

    public void incrementCount() {
        this.count++;
    }

    public void decrementCount() {
        this.count--;
    }
}
