package com.geekhub.core.model.catalog.product;

import com.geekhub.core.model.common.Description;
import lombok.Data;
import lombok.EqualsAndHashCode;

@EqualsAndHashCode(callSuper = true)
@Data
public class ProductDescription extends Description {

    private Integer productId;
    private String author;
    private String isbn;
}
