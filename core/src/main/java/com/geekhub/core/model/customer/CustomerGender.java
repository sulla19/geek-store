package com.geekhub.core.model.customer;

public enum CustomerGender {

    M, F, NONE
}
