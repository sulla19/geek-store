package com.geekhub.core.model.generic;

import lombok.Data;

@Data
public abstract class BaseEntity {

    private Integer id;

    public boolean isNew() {
        return getId() == null;
    }
}
