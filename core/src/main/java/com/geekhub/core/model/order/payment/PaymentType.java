package com.geekhub.core.model.order.payment;

public enum PaymentType {

    CREDITCARD, FREE, COD, MONEYORDER, PAYPAL, STRIPE, WEPAY
}