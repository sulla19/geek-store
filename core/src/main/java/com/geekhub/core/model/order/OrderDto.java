package com.geekhub.core.model.order;

import lombok.Data;

import java.math.BigDecimal;

@Data
public class OrderDto {

    private String shippingType;
    private String paymentType;
    private BigDecimal totalPrice;
    private String telephone;
}
