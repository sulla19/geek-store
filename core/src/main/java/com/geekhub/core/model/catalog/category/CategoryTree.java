package com.geekhub.core.model.catalog.category;

import lombok.Data;

@Data
public class CategoryTree {

    private Integer categoryId;
    private String name;
    private Integer parentId;
}
