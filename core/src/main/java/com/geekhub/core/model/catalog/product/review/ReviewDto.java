package com.geekhub.core.model.catalog.product.review;

import lombok.Data;

import java.sql.Date;

@Data
public class ReviewDto {

    private String customerFirstName;
    private String customerLastName;
    private Date reviewDate;
    private int reviewRating;
    private String advantages;
    private String disadvantages;
    private String comment;
}
