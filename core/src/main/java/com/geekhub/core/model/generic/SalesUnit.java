package com.geekhub.core.model.generic;

import com.geekhub.core.model.catalog.product.ProductType;
import com.geekhub.core.model.common.Description;
import lombok.Data;
import lombok.EqualsAndHashCode;

@EqualsAndHashCode(callSuper = true)
@Data
public abstract class SalesUnit extends BaseEntity {

    private ProductType productType;
    private Description description;
}
