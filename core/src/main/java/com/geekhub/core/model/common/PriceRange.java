package com.geekhub.core.model.common;

import lombok.Data;

import java.math.BigDecimal;

@Data
public class PriceRange {

    private BigDecimal minPrice;
    private BigDecimal maxPrice;
}
