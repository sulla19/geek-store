package com.geekhub.core.model.catalog.product;

import com.geekhub.core.model.catalog.category.Category;
import com.geekhub.core.model.catalog.product.review.Review;
import com.geekhub.core.model.generic.SalesUnit;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.math.BigDecimal;
import java.util.List;

@EqualsAndHashCode(callSuper = true)
@Data
public class Product extends SalesUnit {

    private Category category;
    private List<Review> reviews;
    private ProductMedia productMedia;
    private BigDecimal productLength;
    private BigDecimal productWidth;
    private BigDecimal productHeight;
    private BigDecimal productWeight;
    private BigDecimal price;
}
