package com.geekhub.core.model.order.delivery;

import com.geekhub.core.model.generic.BaseEntity;
import lombok.Data;
import lombok.EqualsAndHashCode;

@EqualsAndHashCode(callSuper = true)
@Data
public class Delivery extends BaseEntity {

    private Integer orderId;
    private String company;
    private String country;
    private String city;
    private String address;
    private String postalCode;
    private String deliveryTelephone;
}
