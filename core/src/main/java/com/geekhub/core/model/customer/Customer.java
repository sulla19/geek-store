package com.geekhub.core.model.customer;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.datatype.jsr310.ser.LocalDateSerializer;
import com.geekhub.core.model.generic.BaseEntity;
import com.geekhub.core.model.order.payment.CreditCard;
import lombok.Data;
import lombok.EqualsAndHashCode;
import org.springframework.format.annotation.DateTimeFormat;

import java.time.LocalDate;

@EqualsAndHashCode(callSuper = true)
@Data
public class Customer extends BaseEntity {

    @DateTimeFormat(iso = DateTimeFormat.ISO.DATE)
    @JsonSerialize(using = LocalDateSerializer.class)
    private LocalDate birthDate;
    private String firstName;
    private String lastName;
    private CustomerGender gender;
    private String telephone;
    private String emailAddress;
    private CreditCard creditCard;
    private String password;
    private Address customerAddress;
}
