package com.geekhub.core.model.catalog.product.review;

import com.geekhub.core.model.generic.BaseEntity;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.time.LocalDate;

@EqualsAndHashCode(callSuper = true)
@Data
public class Review extends BaseEntity {

    private Integer customerId;
    private Integer productId;
    private LocalDate reviewDate;
    private int reviewRating;
    private String advantages;
    private String disadvantages;
    private String comment;
}
