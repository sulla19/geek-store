package com.geekhub.core.model.common;

public enum SortCriteria {

    PRICE_LOW_HIGH("price"), PRICE_HIGH_LOW("price DESC"), BEST_SELLING(""), MOST_VIEWED(""), REVIEW_COUNT("");

    private final String value;

     SortCriteria(String value) {
        this.value = value;
    }

    public String getValue() {
        return value;
    }
}
