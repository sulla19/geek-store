package com.geekhub.core.model.order.payment;

import lombok.Data;

@Data
public class CreditCard {

    private CreditCardType creditCardType;
    private String ccNumber;
    private String ccCvv;
    private Integer customerId;

    public static CreditCard getBlankCreditCard(Integer customerId) {
        CreditCard creditCard = new CreditCard();
        creditCard.setCustomerId(customerId);
        creditCard.setCreditCardType(CreditCardType.NONE);
        creditCard.setCcNumber("----");
        creditCard.setCcCvv("----");
        return creditCard;
    }
}
