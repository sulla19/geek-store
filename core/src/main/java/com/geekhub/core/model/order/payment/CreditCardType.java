package com.geekhub.core.model.order.payment;

public enum CreditCardType {

    AMEX, VISA, MASTERCARD, DINERS, DISCOVERY, NONE
}