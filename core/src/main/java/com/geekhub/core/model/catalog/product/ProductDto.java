package com.geekhub.core.model.catalog.product;

import com.geekhub.core.model.catalog.product.review.Review;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.math.BigDecimal;
import java.util.Collections;
import java.util.List;

@EqualsAndHashCode(callSuper = true)
@Data
public class ProductDto extends Product {

    private String productDtoType;
    private String title;
    private String dtoDescription;
    private Integer productId;
    private String author;
    private String isbn;
    private Integer categoryId;
    private String categoryTitle;
    private List<Review> reviews;
    private String srcImg;
    private BigDecimal productLength;
    private BigDecimal productWidth;
    private BigDecimal productHeight;
    private BigDecimal productWeight;
    private BigDecimal price;

    public List<Review> getReviews() {
        if (reviews == null) {
            return Collections.emptyList();
        }
        return this.reviews;
    }

    public void setReviews(List<Review> reviews) {
        if (reviews == null) {
            this.reviews = Collections.emptyList();
        } else {
            this.reviews = reviews;
        }
    }
}
