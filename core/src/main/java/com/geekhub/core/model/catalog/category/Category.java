package com.geekhub.core.model.catalog.category;

import com.geekhub.core.model.common.Description;
import com.geekhub.core.model.generic.BaseEntity;
import lombok.Data;
import lombok.EqualsAndHashCode;

@EqualsAndHashCode(callSuper = true)
@Data
public class Category extends BaseEntity {

    private String imgUrl;
    private Description description;
    private Category parent;
}

