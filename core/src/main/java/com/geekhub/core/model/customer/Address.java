package com.geekhub.core.model.customer;

import com.geekhub.core.model.generic.BaseEntity;
import lombok.Data;
import lombok.EqualsAndHashCode;

@EqualsAndHashCode(callSuper = true)
@Data
public class Address extends BaseEntity {

    private String customerCountry;
    private String customerCity;
    private String customerStreet;
    private String customerPostcode;
}
