package com.geekhub.core.model.customer;

import lombok.Data;

import java.math.BigDecimal;

@Data
public class CustomerMetrics {

    private Integer ordersCount;
    private Integer productsCount;
    private BigDecimal total;
}
