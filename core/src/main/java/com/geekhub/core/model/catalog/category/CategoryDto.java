package com.geekhub.core.model.catalog.category;

import lombok.Data;
import lombok.EqualsAndHashCode;


@EqualsAndHashCode(callSuper = true)
@Data
public class CategoryDto extends Category{

    private Integer currentId;
    private String imgUrl;
    private String currentTitle;
    private String currentDescription;
    private Integer parentId;
    private String parentTitle;

    public Category getCategory() {
        Category category = new Category();

        CategoryDescription categoryDescription = new CategoryDescription();
        categoryDescription.setTitle(this.currentTitle);

        categoryDescription.setDescription(this.currentDescription);
        category.setDescription(categoryDescription);

        category.setImgUrl(this.imgUrl);

        Category parent = new Category();
        parent.setId(this.parentId);
        category.setParent(parent);

        return category;
    }
}
