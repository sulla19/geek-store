package com.geekhub.core.model.common;

import java.sql.Date;

public enum ReportPeriod {

    REPORT_PERIOD_START("2010-01-01"), LAST_YEAR("2016-01-01"), LAST_SIX_MONTH("2016-06-01");

    private final String value;

    ReportPeriod(String value) {
        this.value = value;
    }

    public Date getValue() {
        return Date.valueOf(value);
    }
}
