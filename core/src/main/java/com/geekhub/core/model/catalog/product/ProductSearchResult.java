package com.geekhub.core.model.catalog.product;

import lombok.Data;

@Data
public class ProductSearchResult {

    private Integer productId;
    private String title;
    private String author;
    private String srcImg;
}
