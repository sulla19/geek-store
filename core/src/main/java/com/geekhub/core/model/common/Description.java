package com.geekhub.core.model.common;

import com.geekhub.core.model.generic.BaseEntity;
import lombok.Data;
import lombok.EqualsAndHashCode;

@EqualsAndHashCode(callSuper = true)
@Data
public class Description extends BaseEntity {

    private String title;
    private String description;
}
