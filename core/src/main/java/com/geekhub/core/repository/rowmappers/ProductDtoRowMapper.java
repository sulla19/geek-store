package com.geekhub.core.repository.rowmappers;

import com.geekhub.core.model.catalog.product.ProductDto;
import org.springframework.jdbc.core.RowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;

public class ProductDtoRowMapper implements RowMapper<ProductDto> {

    @Override
    public ProductDto mapRow(ResultSet rs, int rowNum) throws SQLException {
        ProductDto productDto = new ProductDto();
        productDto.setProductId(rs.getInt("product_id"));
        productDto.setTitle(rs.getString("title"));
        productDto.setAuthor(rs.getString("author"));
        productDto.setSrcImg(rs.getString("src_image"));
        productDto.setPrice(rs.getBigDecimal("price"));
        return productDto;
    }
}
