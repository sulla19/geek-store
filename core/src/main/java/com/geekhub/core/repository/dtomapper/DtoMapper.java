package com.geekhub.core.repository.dtomapper;

import com.geekhub.core.model.catalog.category.Category;
import com.geekhub.core.model.catalog.product.*;

import java.util.ArrayList;
import java.util.List;

public class DtoMapper {
    public static Product getProduct(ProductDto dto) {
        Product product = new Product();
        product.setId(dto.getProductId());
        product.setProductType(ProductType.valueOf(dto.getProductDtoType().toUpperCase()));

        ProductDescription description = new ProductDescription();
        description.setProductId(dto.getProductId());
        description.setTitle(dto.getTitle());
        description.setAuthor(dto.getAuthor());
        description.setIsbn(dto.getIsbn());
        description.setDescription(dto.getDtoDescription());
        product.setDescription(description);

        Category category = new Category();
        category.setId(dto.getCategoryId());
        product.setCategory(category);

        ProductMedia media = new ProductMedia();
        List<String> srcImages = new ArrayList<>();
        srcImages.add(dto.getSrcImg());
        media.setSrcImages(srcImages);
        product.setProductMedia(media);

        product.setReviews(dto.getReviews());
        product.setProductWeight(dto.getProductWeight());
        product.setProductWidth(dto.getProductWidth());
        product.setProductLength(dto.getProductLength());
        product.setProductHeight(dto.getProductHeight());
        product.setPrice(dto.getPrice());

        return product;
    }

    public static ProductDto getProductDto(Product product) {
        ProductDto productDto = new ProductDto();
        productDto.setProductDtoType(product.getProductType().toString());


        ProductDescription description = (ProductDescription) product.getDescription();
        productDto.setAuthor(description.getAuthor());
        productDto.setDtoDescription(description.getDescription());
        productDto.setTitle(description.getTitle());
        productDto.setIsbn(description.getIsbn());

        productDto.setProductId(product.getId());
        productDto.setSrcImg(product.getProductMedia().getSrcImages().get(0));

        productDto.setReviews(product.getReviews());

        productDto.setProductHeight(product.getProductHeight());
        productDto.setProductWeight(product.getProductWeight());
        productDto.setProductLength(product.getProductLength());
        productDto.setProductWidth(product.getProductWidth());
        productDto.setPrice(product.getPrice());
        productDto.setCategoryId(product.getCategory().getId());

        return productDto;
    }
}
