package com.geekhub.core.repository.rowmappers;

import com.geekhub.core.model.order.payment.CreditCard;
import com.geekhub.core.model.order.payment.CreditCardType;
import org.springframework.jdbc.core.RowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;

public class CreditCardRowMapper implements RowMapper<CreditCard> {
    @Override
    public CreditCard mapRow(ResultSet rs, int rowNum) throws SQLException {
        CreditCard creditCard = new CreditCard();
        creditCard.setCcNumber(rs.getString("cc_number"));
        creditCard.setCreditCardType(CreditCardType.valueOf(rs.getString("cc_type").toUpperCase()));
        creditCard.setCcCvv(rs.getString("cc_cvv"));
        return creditCard;
    }
}
