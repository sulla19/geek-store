package com.geekhub.core.repository.rowmappers;

import com.geekhub.core.model.catalog.product.review.Review;
import org.springframework.jdbc.core.RowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;

public class ReviewRowMapper implements RowMapper<Review> {

    @Override
    public Review mapRow(ResultSet rs, int rowNum) throws SQLException {
        Review review = new Review();
        review.setId(rs.getInt("id"));
        review.setReviewDate(rs.getDate("date").toLocalDate());
        review.setReviewRating(rs.getInt("rating"));
        review.setAdvantages(rs.getString("advantages"));
        review.setDisadvantages(rs.getString("disadvantages"));
        review.setComment(rs.getString("comment"));
        review.setCustomerId(rs.getInt("customer_id"));
        return review;
    }
}
