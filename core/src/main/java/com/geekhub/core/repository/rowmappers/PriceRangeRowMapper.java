package com.geekhub.core.repository.rowmappers;

import com.geekhub.core.model.common.PriceRange;
import org.springframework.jdbc.core.RowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;

public class PriceRangeRowMapper implements RowMapper<PriceRange> {
    @Override
    public PriceRange mapRow(ResultSet rs, int rowNum) throws SQLException {
        PriceRange priceRange = new PriceRange();
        priceRange.setMinPrice(rs.getBigDecimal("min"));
        priceRange.setMaxPrice(rs.getBigDecimal("max"));
        return priceRange;
    }
}
