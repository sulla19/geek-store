package com.geekhub.core.repository.mapsqlparametersources;

import com.geekhub.core.model.catalog.category.Category;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;

public class CategoryParameterSource extends MapSqlParameterSource {

    public MapSqlParameterSource getParameterSource(Category category) {
        MapSqlParameterSource parameterSource = new MapSqlParameterSource();
        parameterSource.addValue("img_url", category.getImgUrl());
        parameterSource.addValue("parent_id", category.getParent().getId());
        return parameterSource;
    }
}
