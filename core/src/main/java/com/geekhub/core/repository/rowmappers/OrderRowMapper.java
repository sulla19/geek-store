package com.geekhub.core.repository.rowmappers;

import com.geekhub.core.model.order.Order;
import com.geekhub.core.model.order.payment.PaymentType;
import com.geekhub.core.model.order.shipping.ShippingType;
import org.springframework.jdbc.core.RowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;

public class OrderRowMapper implements RowMapper<Order> {

    @Override
    public Order mapRow(ResultSet rs, int rowNum) throws SQLException {
        Order order = new Order();
        order.setId(rs.getInt("id"));
        order.setLastModified(rs.getDate("last_modified").toLocalDate());
        order.setCustomerId(rs.getInt("customer_id"));
        order.setTotalPrice(rs.getBigDecimal("total_price"));
        order.setShippingType(ShippingType.valueOf(rs.getString("shipping_type").toUpperCase()));
        order.setPaymentType(PaymentType.valueOf(rs.getString("payment_type").toUpperCase()));
        order.setContactTelephone(rs.getString("contact_telephone"));

        if (rs.getMetaData().getColumnCount() == 14) {
            order.setDelivery(new DeliveryRowMapper().mapRow(rs, rowNum));
        }
        return order;
    }
}
