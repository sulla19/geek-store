package com.geekhub.core.repository.rowmappers;

import com.geekhub.core.model.order.delivery.Delivery;
import org.springframework.jdbc.core.RowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;

public class DeliveryRowMapper implements RowMapper<Delivery> {
    @Override
    public Delivery mapRow(ResultSet rs, int rowNum) throws SQLException {
        Delivery delivery = new Delivery();
        delivery.setId(rs.getInt("id"));
        delivery.setCompany(rs.getString("company"));
        delivery.setCountry(rs.getString("country"));
        delivery.setAddress(rs.getString("address"));
        delivery.setCity(rs.getString("city"));
        delivery.setPostalCode(rs.getString("postal_code"));
        delivery.setDeliveryTelephone(rs.getString("telephone"));
        return delivery;
    }
}
