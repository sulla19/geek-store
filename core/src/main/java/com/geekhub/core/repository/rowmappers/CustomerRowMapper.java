package com.geekhub.core.repository.rowmappers;

import com.geekhub.core.model.customer.Customer;
import com.geekhub.core.model.customer.CustomerGender;
import org.springframework.jdbc.core.RowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;

public class CustomerRowMapper implements RowMapper<Customer> {

    @Override
    public Customer mapRow(ResultSet rs, int rowNum) throws SQLException {
        Customer customer = new Customer();
        customer.setId(rs.getInt("id"));
        customer.setFirstName(rs.getString("first_name"));
        customer.setLastName(rs.getString("last_name"));
        customer.setGender(CustomerGender.valueOf(rs.getString("gender").toUpperCase()));
        customer.setBirthDate(rs.getDate("birth_date").toLocalDate());
        customer.setTelephone(rs.getString("telephone"));
        customer.setEmailAddress(rs.getString("email"));
        customer.setPassword(rs.getString("password"));

        if( rs.getMetaData().getColumnCount() == 11) {
            customer.setCreditCard(new CreditCardRowMapper().mapRow(rs, rowNum));
        }
        return customer;
    }
}
