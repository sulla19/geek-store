package com.geekhub.core.repository.rowmappers;

import com.geekhub.core.model.catalog.category.Category;
import com.geekhub.core.model.catalog.category.CategoryDescription;
import com.geekhub.core.model.catalog.product.Product;
import com.geekhub.core.model.catalog.product.ProductType;
import org.springframework.jdbc.core.RowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;

public class ProductRowMapper implements RowMapper<Product> {

    @Override
    public Product mapRow(ResultSet rs, int rowNum) throws SQLException {
        Product product = new Product();
        product.setId(rs.getInt("prod_id"));
        product.setProductType(ProductType.valueOf(rs.getString("type").toUpperCase()));
        product.setProductWeight(rs.getBigDecimal("weight"));
        product.setProductWidth(rs.getBigDecimal("width"));
        product.setProductLength(rs.getBigDecimal("length"));
        product.setProductHeight(rs.getBigDecimal("height"));
        product.setPrice(rs.getBigDecimal("price"));

        product.setDescription(new ProductDescriptionRowMapper().mapRow(rs, rowNum));

        Category category = new Category();
        CategoryDescription catDescription = new CategoryDescription();
        catDescription.setTitle(rs.getString("cat_title"));
        category.setDescription(catDescription);

        product.setCategory(category);
        return product;
    }
}
