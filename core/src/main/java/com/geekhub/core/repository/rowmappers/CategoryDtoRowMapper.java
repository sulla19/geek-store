package com.geekhub.core.repository.rowmappers;

import com.geekhub.core.model.catalog.category.CategoryDto;
import org.springframework.jdbc.core.RowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;

public class CategoryDtoRowMapper implements RowMapper<CategoryDto> {
    @Override
    public CategoryDto mapRow(ResultSet rs, int rowNum) throws SQLException {
        int columnCount = rs.getMetaData().getColumnCount();

        if (columnCount == 3) {
            return getForFindAll(rs);
        } else {
            return getForFindOne(rs);
        }
    }

    private CategoryDto getForFindOne(ResultSet rs) throws SQLException {
        CategoryDto categoryDto = new CategoryDto();
        categoryDto.setCurrentId(rs.getInt("curr_id"));
        categoryDto.setImgUrl(rs.getString("curr_img_url"));
        categoryDto.setParentId(rs.getInt("parent_id"));

        categoryDto.setCurrentTitle(rs.getString("current_title"));
        categoryDto.setCurrentDescription(rs.getString("current_description"));
        categoryDto.setParentTitle(rs.getString("parent_title"));
        return categoryDto;
    }

    private CategoryDto getForFindAll(ResultSet rs) throws SQLException {
        CategoryDto categoryDto = new CategoryDto();
        categoryDto.setCurrentId(rs.getInt("curr_id"));
        categoryDto.setCurrentTitle(rs.getString("current_title"));
        categoryDto.setParentTitle(rs.getString("parent_title"));
        return categoryDto;
    }
}
