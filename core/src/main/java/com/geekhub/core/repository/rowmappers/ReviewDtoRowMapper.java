package com.geekhub.core.repository.rowmappers;

import com.geekhub.core.model.catalog.product.review.ReviewDto;
import org.springframework.jdbc.core.RowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;

public class ReviewDtoRowMapper implements RowMapper<ReviewDto> {

    @Override
    public ReviewDto mapRow(ResultSet rs, int rowNum) throws SQLException {
        ReviewDto review = new ReviewDto();
        review.setReviewDate(rs.getDate("date"));
        review.setReviewRating(rs.getInt("rating"));
        review.setAdvantages(rs.getString("advantages"));
        review.setDisadvantages(rs.getString("disadvantages"));
        review.setComment(rs.getString("comment"));
        review.setCustomerFirstName(rs.getString("first_name"));
        review.setCustomerLastName(rs.getString("last_name"));
        return review;
    }
}