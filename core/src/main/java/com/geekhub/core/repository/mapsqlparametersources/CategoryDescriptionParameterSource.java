package com.geekhub.core.repository.mapsqlparametersources;

import com.geekhub.core.model.common.Description;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;

public class CategoryDescriptionParameterSource extends MapSqlParameterSource {

    public MapSqlParameterSource getParameterSource(Description description) {
        MapSqlParameterSource parameterSource = new MapSqlParameterSource();
        parameterSource.addValue("title", description.getTitle());
        parameterSource.addValue("description", description.getDescription());
        return parameterSource;
    }
}
