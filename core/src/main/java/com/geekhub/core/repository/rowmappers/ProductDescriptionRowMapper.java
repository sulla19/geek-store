package com.geekhub.core.repository.rowmappers;

import com.geekhub.core.model.catalog.product.ProductDescription;
import org.springframework.jdbc.core.RowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;

public class ProductDescriptionRowMapper implements RowMapper<ProductDescription> {
    @Override
    public ProductDescription mapRow(ResultSet rs, int rowNum) throws SQLException {
        ProductDescription description = new ProductDescription();
        description.setProductId(rs.getInt("product_id"));
        description.setTitle(rs.getString("product_title"));
        description.setAuthor(rs.getString("author"));
        description.setIsbn(rs.getString("isbn"));
        description.setDescription(rs.getString("description"));
        return description;
    }
}
