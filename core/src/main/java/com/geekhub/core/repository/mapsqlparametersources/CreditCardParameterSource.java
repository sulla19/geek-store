package com.geekhub.core.repository.mapsqlparametersources;

import com.geekhub.core.model.order.payment.CreditCard;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;

public class CreditCardParameterSource {

    public MapSqlParameterSource getParameterSource(CreditCard creditCard) {
        MapSqlParameterSource parameterSource = new MapSqlParameterSource();
        parameterSource.addValue("cc_type", creditCard.getCreditCardType().toString());
        parameterSource.addValue("cc_number", creditCard.getCcNumber());
        parameterSource.addValue("cc_cvv", creditCard.getCcCvv());
        parameterSource.addValue("customer_id", creditCard.getCustomerId());
        return parameterSource;
    }
}
