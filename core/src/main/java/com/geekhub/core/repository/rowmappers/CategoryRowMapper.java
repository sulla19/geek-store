package com.geekhub.core.repository.rowmappers;

import com.geekhub.core.model.catalog.category.Category;
import com.geekhub.core.model.catalog.category.CategoryDescription;
import org.springframework.jdbc.core.RowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;

public class CategoryRowMapper implements RowMapper<Category> {

    @Override
    public Category mapRow(ResultSet rs, int rowNum) throws SQLException {

        Category category = new Category();
        category.setId(rs.getInt("cat_id"));
        category.setImgUrl(rs.getString("curr_img_url"));

        category.setDescription(new CategoryDescriptionRowMapper().mapRow(rs, rowNum));

        Category parent = new Category();
        parent.setId(rs.getInt("parent_id"));

        CategoryDescription parentDescription = new CategoryDescription();
        parentDescription.setTitle(rs.getString("parent_title"));
        parent.setDescription(parentDescription);

        category.setParent(parent);
        return category;
    }
}
