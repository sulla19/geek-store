package com.geekhub.core.repository.mapsqlparametersources;

import com.geekhub.core.model.catalog.product.ProductDescription;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;

public class ProductDescriptionParameterSource {

    public MapSqlParameterSource getParameterSource(ProductDescription description) {
        MapSqlParameterSource parameterSource = new MapSqlParameterSource();
        parameterSource.addValue("product_id", description.getProductId());
        parameterSource.addValue("title", description.getTitle());
        parameterSource.addValue("description", description.getDescription());
        parameterSource.addValue("author", description.getAuthor());
        parameterSource.addValue("isbn", description.getIsbn());
        return parameterSource;
    }
}
