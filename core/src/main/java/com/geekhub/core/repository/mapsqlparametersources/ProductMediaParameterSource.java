package com.geekhub.core.repository.mapsqlparametersources;

import com.geekhub.core.model.catalog.product.ProductMedia;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;

public class ProductMediaParameterSource {
    public MapSqlParameterSource getParameterSource(ProductMedia media) {
        MapSqlParameterSource parameterSource = new MapSqlParameterSource();
        parameterSource.addValue("product_id", media.getProductId());
        parameterSource.addValue("src_image", media.getSrcImages());
        return parameterSource;
    }
}
