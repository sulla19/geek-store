package com.geekhub.core.repository.mapsqlparametersources;

import com.geekhub.core.model.catalog.product.Product;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;

public class ProductParameterSource {
    public MapSqlParameterSource getParameterSource(Product product) {
        MapSqlParameterSource parameterSource = new MapSqlParameterSource();
        parameterSource.addValue("type", product.getProductType().toString());
        parameterSource.addValue("category_id", product.getCategory().getId());
        parameterSource.addValue("length", product.getProductLength());
        parameterSource.addValue("weight", product.getProductWeight());
        parameterSource.addValue("width", product.getProductWidth());
        parameterSource.addValue("height", product.getProductHeight());
        parameterSource.addValue("price", product.getPrice());
        return parameterSource;
    }
}
