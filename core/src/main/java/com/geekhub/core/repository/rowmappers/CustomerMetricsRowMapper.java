package com.geekhub.core.repository.rowmappers;

import com.geekhub.core.model.customer.CustomerMetrics;
import org.springframework.jdbc.core.RowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;

public class CustomerMetricsRowMapper implements RowMapper<CustomerMetrics> {

    @Override
    public CustomerMetrics mapRow(ResultSet rs, int rowNum) throws SQLException {
        CustomerMetrics metrics = new CustomerMetrics();
        metrics.setOrdersCount(rs.getInt("orders_num"));
        metrics.setProductsCount(rs.getInt("prod_num"));
        metrics.setTotal(rs.getBigDecimal("total_sum"));
        return metrics;
    }
}
