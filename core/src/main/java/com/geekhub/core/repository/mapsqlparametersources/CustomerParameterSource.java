package com.geekhub.core.repository.mapsqlparametersources;

import com.geekhub.core.model.customer.Customer;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;

import java.sql.Date;

public class CustomerParameterSource {

    public MapSqlParameterSource getParameterSource(Customer customer) {
        MapSqlParameterSource parameterSource = new MapSqlParameterSource();
        parameterSource.addValue("firstName", customer.getFirstName());
        parameterSource.addValue("lastName", customer.getLastName());
        parameterSource.addValue("gender", customer.getGender().toString());
        parameterSource.addValue("birthDate", Date.valueOf(customer.getBirthDate()));
        parameterSource.addValue("telephone", customer.getTelephone());
        parameterSource.addValue("email", customer.getEmailAddress());
        parameterSource.addValue("password", customer.getPassword());
        return parameterSource;
    }
}
