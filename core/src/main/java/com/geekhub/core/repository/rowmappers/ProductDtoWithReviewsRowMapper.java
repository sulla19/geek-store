package com.geekhub.core.repository.rowmappers;

import com.geekhub.core.model.catalog.product.ProductDto;
import com.geekhub.core.model.catalog.product.review.Review;
import org.springframework.jdbc.core.RowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class ProductDtoWithReviewsRowMapper implements RowMapper<ProductDto> {

    @Override
    public ProductDto mapRow(ResultSet rs, int rowNum) throws SQLException {

        ProductDto productDto = new ProductDto();

        productDto.setProductId(rs.getInt("prod_id"));
        productDto.setProductDtoType(rs.getString("type").toUpperCase());
        productDto.setProductWeight(rs.getBigDecimal("weight"));
        productDto.setProductWidth(rs.getBigDecimal("width"));
        productDto.setProductLength(rs.getBigDecimal("length"));
        productDto.setProductHeight(rs.getBigDecimal("height"));
        productDto.setPrice(rs.getBigDecimal("price"));

        productDto.setTitle(rs.getString("prod_title"));
        productDto.setAuthor(rs.getString("author"));
        productDto.setIsbn(rs.getString("isbn"));
        productDto.setDtoDescription(rs.getString("description"));

        productDto.setCategoryId(rs.getInt("cat_id"));
        productDto.setCategoryTitle(rs.getString("cat_title"));

        productDto.setSrcImg(rs.getString("src_image"));

        productDto.setReviews(fetchListReviews(rs, rowNum));
        return productDto;
    }

    private List<Review> fetchListReviews(ResultSet rs, int rowNum) throws SQLException {
        List<Review> list = new ArrayList<>();

        try {
            while (!rs.isAfterLast()) {
                list.add(new ReviewRowMapper().mapRow(rs, rowNum));
                rs.next();
            }
        } catch (NullPointerException e) {
            return Collections.emptyList();
        }
        return list;
    }
}
