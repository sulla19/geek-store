package com.geekhub.core.repository.mapsqlparametersources;

import com.geekhub.core.model.catalog.product.review.Review;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;

import java.sql.Date;

public class ReviewParameterSource {
    public MapSqlParameterSource getParameterSource(Review review) {
        MapSqlParameterSource parameterSource = new MapSqlParameterSource();
        parameterSource.addValue("date", Date.valueOf(review.getReviewDate()));
        parameterSource.addValue("rating", review.getReviewRating());
        parameterSource.addValue("advantages", review.getAdvantages());
        parameterSource.addValue("disadvantages", review.getDisadvantages());
        parameterSource.addValue("comment", review.getComment());
        parameterSource.addValue("customer_id", review.getCustomerId());
        parameterSource.addValue("product_id", review.getProductId());
        return parameterSource;
    }
}
