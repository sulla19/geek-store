package com.geekhub.core.repository.rowmappers;

import com.geekhub.core.model.catalog.category.CategoryDescription;
import org.springframework.jdbc.core.RowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;

public class CategoryDescriptionRowMapper implements RowMapper<CategoryDescription> {
    @Override
    public CategoryDescription mapRow(ResultSet rs, int rowNum) throws SQLException {
        CategoryDescription description = new CategoryDescription();
        description.setTitle(rs.getString("current_title"));
        description.setDescription(rs.getString("current_description"));
        return description;
    }
}
