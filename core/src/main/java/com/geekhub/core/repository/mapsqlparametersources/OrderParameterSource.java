package com.geekhub.core.repository.mapsqlparametersources;

import com.geekhub.core.model.order.Order;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;

import java.sql.Date;

public class OrderParameterSource {

    public MapSqlParameterSource getParameterSource(Order order) {
        MapSqlParameterSource parameterSource = new MapSqlParameterSource();
        parameterSource.addValue("last_modified", Date.valueOf(order.getLastModified()));
        parameterSource.addValue("customer_id", order.getCustomerId());
        parameterSource.addValue("total_price", order.getTotalPrice());
        parameterSource.addValue("payment_type", order.getPaymentType().toString());
        parameterSource.addValue("shipping_type", order.getShippingType().toString());
        parameterSource.addValue("contact_telephone", order.getContactTelephone());
        return parameterSource;
    }
}
