package com.geekhub.core.repository.mapsqlparametersources;

import com.geekhub.core.model.order.delivery.Delivery;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;

public class DeliveryParameterSource {
    public MapSqlParameterSource getParameterSource(Delivery delivery) {
        MapSqlParameterSource parameterSource = new MapSqlParameterSource();
        parameterSource.addValue("order_id", delivery.getOrderId());
        parameterSource.addValue("company", delivery.getCompany());
        parameterSource.addValue("country", delivery.getCountry());
        parameterSource.addValue("city", delivery.getCity());
        parameterSource.addValue("address", delivery.getAddress());
        parameterSource.addValue("postal_code", delivery.getPostalCode());
        parameterSource.addValue("telephone", delivery.getDeliveryTelephone());
        return parameterSource;
    }
}
