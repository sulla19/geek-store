package com.geekhub.core.repository.rowmappers;

import com.geekhub.core.model.catalog.product.ProductSearchResult;
import org.springframework.jdbc.core.RowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;

public class ProductSearchResultRowMapper implements RowMapper<ProductSearchResult> {

    @Override
    public ProductSearchResult mapRow(ResultSet rs, int rowNum) throws SQLException {
        ProductSearchResult productSearchResult = new ProductSearchResult();
        productSearchResult.setProductId(rs.getInt("id"));
        productSearchResult.setTitle(rs.getString("title"));
        productSearchResult.setAuthor(rs.getString("author"));
        productSearchResult.setSrcImg(rs.getString("src_image"));
        return productSearchResult;
    }
}
