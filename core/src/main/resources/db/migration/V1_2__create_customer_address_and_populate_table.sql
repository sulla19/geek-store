CREATE TABLE geek_store.customer_address (
  id            SERIAL PRIMARY KEY ,
  customer_id   INT NOT NULL ,
  country       VARCHAR(100) NOT NULL,
  city          VARCHAR(100) NOT NULL,
  streetAddress VARCHAR(100) NOT NULL,
  postcode      VARCHAR(100) NOT NULL,
  FOREIGN KEY (customer_id) REFERENCES geek_store.customers(id)
  ON UPDATE CASCADE ON DELETE CASCADE
);

INSERT INTO geek_store.customer_address(customer_id, country, city, streetaddress, postcode) VALUES (1, 'USA', 'New York', 'Str, 78', 02384);
INSERT INTO geek_store.customer_address(customer_id, country, city, streetaddress, postcode) VALUES (2, 'USA', 'Los Angeles', 'Str, 34', 234235);
INSERT INTO geek_store.customer_address(customer_id, country, city, streetaddress, postcode) VALUES (3, 'USA', 'Chicago', 'Str, 2314', 46747);
INSERT INTO geek_store.customer_address(customer_id, country, city, streetaddress, postcode) VALUES (4, 'USA', 'Houston', 'Str, 23', 24566);
INSERT INTO geek_store.customer_address(customer_id, country, city, streetaddress, postcode) VALUES (5, 'USA', 'Philadelphia', 'Str, 23', 7363);
INSERT INTO geek_store.customer_address(customer_id, country, city, streetaddress, postcode) VALUES (6, 'USA', 'Phoenix', 'Str, 234', 091723);
INSERT INTO geek_store.customer_address(customer_id, country, city, streetaddress, postcode) VALUES (7, 'USA', 'San Antonio', 'Str, 244', 52123);
INSERT INTO geek_store.customer_address(customer_id, country, city, streetaddress, postcode) VALUES (8, 'USA', 'San Diego', 'Str, 213', 25255);
INSERT INTO geek_store.customer_address(customer_id, country, city, streetaddress, postcode) VALUES (9, 'USA', 'Dallas', 'Str, 7345', 8656);
INSERT INTO geek_store.customer_address(customer_id, country, city, streetaddress, postcode) VALUES (10, 'USA', 'San Jose', 'Str, 134', 6674);
INSERT INTO geek_store.customer_address(customer_id, country, city, streetaddress, postcode) VALUES (11, 'USA', 'Austin', 'Str, 34', 234523);
INSERT INTO geek_store.customer_address(customer_id, country, city, streetaddress, postcode) VALUES (12, 'USA', 'Indianapolis', 'Str, 234', 2344);
INSERT INTO geek_store.customer_address(customer_id, country, city, streetaddress, postcode) VALUES (13, 'USA', 'Jacksonville', 'Str, 532', 3384);
INSERT INTO geek_store.customer_address(customer_id, country, city, streetaddress, postcode) VALUES (14, 'USA', 'San Francisco', 'Str, 728', 54384);
INSERT INTO geek_store.customer_address(customer_id, country, city, streetaddress, postcode) VALUES (15, 'USA', 'Columbus', 'Str, 238', 3634);
INSERT INTO geek_store.customer_address(customer_id, country, city, streetaddress, postcode) VALUES (16, 'USA', 'Charlotte', 'Str, 234', 584);
INSERT INTO geek_store.customer_address(customer_id, country, city, streetaddress, postcode) VALUES (17, 'USA', 'Detroit', 'Str, 2344', 5834);
INSERT INTO geek_store.customer_address(customer_id, country, city, streetaddress, postcode) VALUES (18, 'USA', 'Memphis', 'Str, 654', 23);
INSERT INTO geek_store.customer_address(customer_id, country, city, streetaddress, postcode) VALUES (19, 'USA', 'Seattle', 'Str, 23', 843);
INSERT INTO geek_store.customer_address(customer_id, country, city, streetaddress, postcode) VALUES (20, 'USA', 'Denver', 'Str, 24', 384);