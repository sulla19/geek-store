CREATE TABLE geek_store.products (
  id               SERIAL,
  type             VARCHAR(45)   NOT NULL,
  category_id      INT   NOT NULL,
  length           DOUBLE PRECISION        NOT NULL,
  weight           DOUBLE PRECISION        NOT NULL,
  height           DOUBLE PRECISION        NOT NULL,
  width            DOUBLE PRECISION        NOT NULL,
  price            DECIMAL(8, 2) NOT NULL,
  PRIMARY KEY (id),
  FOREIGN KEY (category_id) REFERENCES geek_store.category (id)
    ON UPDATE CASCADE
    ON DELETE CASCADE
);

CREATE TABLE geek_store.product_descriptions (
  product_id  INTEGER           NOT NULL,
  title       VARCHAR(500) NOT NULL,
  author      VARCHAR(100) NOT NULL,
  isbn        VARCHAR(100) NOT NULL,
  description VARCHAR(2000) NOT NULL,
  FOREIGN KEY (product_id) REFERENCES geek_store.products(id) ON UPDATE CASCADE ON DELETE CASCADE
);

-- Computer and technology 1- 21
INSERT INTO geek_store.products (type, category_id, length, weight, height, width, price)
VALUES ('book', 61, 40, 40, 40, 40, 49.90);
INSERT INTO geek_store.products (type, category_id, length, weight, height, width, price)
VALUES ('book',61, 40, 40, 40, 40, 450);
INSERT INTO geek_store.products (type, category_id, length, weight, height, width, price)
VALUES ('book',61, 40, 40, 40, 40, 4500);
INSERT INTO geek_store.products (type, category_id, length, weight, height, width, price)
VALUES ('book',61, 40, 40, 40, 40, 4500);
INSERT INTO geek_store.products (type, category_id, length, weight, height, width, price)
VALUES ('book',61, 40, 40, 40, 40, 4500);
INSERT INTO geek_store.products (type, category_id, length, weight, height, width, price)
VALUES ('book',61, 40, 40, 40, 40, 4500);
INSERT INTO geek_store.products (type, category_id, length, weight, height, width, price)
VALUES ('book', 61, 40, 40, 40, 40, 4500);
INSERT INTO geek_store.products (type, category_id, length, weight, height, width, price)
VALUES ('book', 61, 40, 40, 40, 40, 4500);
INSERT INTO geek_store.products (type, category_id, length, weight, height, width, price)
VALUES ('book', 61, 40, 40, 40, 40, 4500);
INSERT INTO geek_store.products (type, category_id, length, weight, height, width, price)
VALUES ('book', 61, 40, 40, 40, 40, 4500);
INSERT INTO geek_store.products (type, category_id, length, weight, height, width, price)
VALUES ('book', 54, 40, 40, 40, 40, 4500);
INSERT INTO geek_store.products (type, category_id, length, weight, height, width, price)
VALUES ('book', 54, 40, 40, 40, 40, 4500);
INSERT INTO geek_store.products (type, category_id, length, weight, height, width, price)
VALUES ('book', 54, 40, 40, 40, 40, 4500);
INSERT INTO geek_store.products (type, category_id, length, weight, height, width, price)
VALUES ('book', 54, 40, 40, 40, 40, 4500);
INSERT INTO geek_store.products (type, category_id, length, weight, height, width, price)
VALUES ('book', 54, 40, 40, 40, 40, 4500);
INSERT INTO geek_store.products (type, category_id, length, weight, height, width, price)
VALUES ('book', 54, 40, 40, 40, 40, 4500);
INSERT INTO geek_store.products (type, category_id, length, weight, height, width, price)
VALUES ('book', 54, 40, 40, 40, 40, 4500);
INSERT INTO geek_store.products (type, category_id, length, weight, height, width, price)
VALUES ('book', 54, 40, 40, 40, 40, 4500);
INSERT INTO geek_store.products (type, category_id, length, weight, height, width, price)
VALUES ('book', 54, 40, 40, 40, 40, 4500);
INSERT INTO geek_store.products (type, category_id, length, weight, height, width, price)
VALUES ('book', 54, 40, 40, 40, 40, 4500);
INSERT INTO geek_store.products (type, category_id, length, weight, height, width, price)
VALUES ('book', 54, 40, 40, 40, 40, 4500);

-- Art and architecture 22-
INSERT INTO geek_store.products (type, category_id, length, weight, height, width, price)
VALUES ('book', 11, 40, 40, 40, 40, 49.90);
INSERT INTO geek_store.products (type, category_id, length, weight, height, width, price)
VALUES ('book', 11, 40, 40, 40, 40, 49.90);
INSERT INTO geek_store.products (type, category_id, length, weight, height, width, price)
VALUES ('book', 11, 40, 40, 40, 40, 49.90);
INSERT INTO geek_store.products (type, category_id, length, weight, height, width, price)
VALUES ('book', 12, 40, 40, 40, 40, 49.90);
INSERT INTO geek_store.products (type, category_id, length, weight, height, width, price)
VALUES ('book', 12, 40, 40, 40, 40, 49.90);
INSERT INTO geek_store.products (type, category_id, length, weight, height, width, price)
VALUES ('book', 12, 40, 40, 40, 40, 49.90);
INSERT INTO geek_store.products (type, category_id, length, weight, height, width, price)
VALUES ('book', 13, 40, 40, 40, 40, 49.90);
INSERT INTO geek_store.products (type, category_id, length, weight, height, width, price)
VALUES ('book', 13, 40, 40, 40, 40, 49.90);
INSERT INTO geek_store.products (type, category_id, length, weight, height, width, price)
VALUES ('book', 13, 40, 40, 40, 40, 49.90);
INSERT INTO geek_store.products (type, category_id, length, weight, height, width, price)
VALUES ('book', 14, 40, 40, 40, 40, 49.90);
INSERT INTO geek_store.products (type, category_id, length, weight, height, width, price)
VALUES ('book', 14, 40, 40, 40, 40, 49.90);
INSERT INTO geek_store.products (type, category_id, length, weight, height, width, price)
VALUES ('book', 14, 40, 40, 40, 40, 49.90);
INSERT INTO geek_store.products (type, category_id, length, weight, height, width, price)
VALUES ('book', 15, 40, 40, 40, 40, 49.90);
INSERT INTO geek_store.products (type, category_id, length, weight, height, width, price)
VALUES ('book', 15, 40, 40, 40, 40, 49.90);
INSERT INTO geek_store.products (type, category_id, length, weight, height, width, price)
VALUES ('book', 15, 40, 40, 40, 40, 49.90);
INSERT INTO geek_store.products (type, category_id, length, weight, height, width, price)
VALUES ('book', 16, 40, 40, 40, 40, 49.90);
INSERT INTO geek_store.products (type, category_id, length, weight, height, width, price)
VALUES ('book', 16, 40, 40, 40, 40, 49.90);
INSERT INTO geek_store.products (type, category_id, length, weight, height, width, price)
VALUES ('book', 16, 40, 40, 40, 40, 49.90);
INSERT INTO geek_store.products (type, category_id, length, weight, height, width, price)
VALUES ('book', 17, 40, 40, 40, 40, 49.90);
INSERT INTO geek_store.products (type, category_id, length, weight, height, width, price)
VALUES ('book', 17, 40, 40, 40, 40, 49.90);
INSERT INTO geek_store.products (type, category_id, length, weight, height, width, price)
VALUES ('book', 17, 40, 40, 40, 40, 49.90);
INSERT INTO geek_store.products (type, category_id, length, weight, height, width, price)
VALUES ('book', 18, 40, 40, 40, 40, 49.90);
INSERT INTO geek_store.products (type, category_id, length, weight, height, width, price)
VALUES ('book', 18, 40, 40, 40, 40, 49.90);
INSERT INTO geek_store.products (type, category_id, length, weight, height, width, price)
VALUES ('book', 19, 40, 40, 40, 40, 49.90);
INSERT INTO geek_store.products (type, category_id, length, weight, height, width, price)
VALUES ('book', 19, 40, 40, 40, 40, 49.90);
INSERT INTO geek_store.products (type, category_id, length, weight, height, width, price)
VALUES ('book', 20, 40, 40, 40, 40, 49.90);
INSERT INTO geek_store.products (type, category_id, length, weight, height, width, price)
VALUES ('book', 20, 40, 40, 40, 40, 49.90);
INSERT INTO geek_store.products (type, category_id, length, weight, height, width, price)
VALUES ('book', 21, 40, 40, 40, 40, 49.90);
INSERT INTO geek_store.products (type, category_id, length, weight, height, width, price)
VALUES ('book', 21, 40, 40, 40, 40, 49.90);
INSERT INTO geek_store.products (type, category_id, length, weight, height, width, price)
VALUES ('book', 22, 40, 40, 40, 40, 49.90);
INSERT INTO geek_store.products (type, category_id, length, weight, height, width, price)
VALUES ('book', 22, 40, 40, 40, 40, 49.90);
INSERT INTO geek_store.products (type, category_id, length, weight, height, width, price)
VALUES ('book', 23, 40, 40, 40, 40, 49.90);
INSERT INTO geek_store.products (type, category_id, length, weight, height, width, price)
VALUES ('book', 23, 40, 40, 40, 40, 49.90);
INSERT INTO geek_store.products (type, category_id, length, weight, height, width, price)
VALUES ('book', 24, 40, 40, 40, 40, 49.90);
INSERT INTO geek_store.products (type, category_id, length, weight, height, width, price)
VALUES ('book', 24, 40, 40, 40, 40, 49.90);
INSERT INTO geek_store.products (type, category_id, length, weight, height, width, price)
VALUES ('book', 25, 40, 40, 40, 40, 49.90);
INSERT INTO geek_store.products (type, category_id, length, weight, height, width, price)
VALUES ('book', 25, 40, 40, 40, 40, 49.90);
INSERT INTO geek_store.products (type, category_id, length, weight, height, width, price)
VALUES ('book', 26, 40, 40, 40, 40, 49.90);
INSERT INTO geek_store.products (type, category_id, length, weight, height, width, price)
VALUES ('book', 26, 40, 40, 40, 40, 49.90);
INSERT INTO geek_store.products (type, category_id, length, weight, height, width, price)
VALUES ('book', 27, 40, 40, 40, 40, 49.90);
INSERT INTO geek_store.products (type, category_id, length, weight, height, width, price)
VALUES ('book', 27, 40, 40, 40, 40, 49.90);


-- Computer and technology 1- 21
INSERT INTO geek_store.product_descriptions (product_id, title, author, isbn, description)
VALUES (1, 'Effective Java (2nd Edition)', 'Joshua Bloch', '304503955', 'Test description one');
INSERT INTO geek_store.product_descriptions (product_id, title, author, isbn, description)
VALUES (2, 'Spring in Action: Covers Spring 3.0', 'Craig Walls ', '304503955', 'Test description one');
INSERT INTO geek_store.product_descriptions (product_id, title, author, isbn, description)
VALUES (3, 'Clean Code: A Handbook of Agile Software Craftsmanship', 'Robert Martin', '304503955', 'Test description one');
INSERT INTO geek_store.product_descriptions (product_id, title, author, isbn, description)
VALUES (4, 'Design Patterns: Elements of Reusable Object-Oriented Software', 'GoF', '304503955', 'Test description one');
INSERT INTO geek_store.product_descriptions (product_id, title, author, isbn, description)
VALUES (5, 'Head First Java, 2nd Edition', ' Kathy Sierra', '304503955', 'Test description one');
INSERT INTO geek_store.product_descriptions (product_id, title, author, isbn, description)
VALUES (6, 'Java: A Beginner''s Guide, Sixth Edition', 'Herbert Schildt', '304503955', 'Test description one');
INSERT INTO geek_store.product_descriptions (product_id, title, author, isbn, description)
VALUES (7, 'Core Java, Volume II--Advanced Features (10th Edition)', 'Cay S. Horstmann', '304503955', 'Test description one');
INSERT INTO geek_store.product_descriptions (product_id, title, author, isbn, description)
VALUES (8, 'Head First Design Patterns: A Brain-Friendly Guide', ' Eric Freeman', '304503955', 'Test description one');
INSERT INTO geek_store.product_descriptions (product_id, title, author, isbn, description)
VALUES (9, 'Regular Expression Pocket Reference', 'Joshua Bloch', '304503955', 'Test description one');
INSERT INTO geek_store.product_descriptions (product_id, title, author, isbn, description)
VALUES (10, 'The Java Native Interface: Programmer''s Guide and Specification', 'Joshua Bloch', '304503955', 'Test description one');
INSERT INTO geek_store.product_descriptions (product_id, title, author, isbn, description)
VALUES (11, 'The C Programming Language', 'Brian W. Kernighan and Dennis M. Ritchie', '304503955', 'C description');
INSERT INTO geek_store.product_descriptions (product_id, title, author, isbn, description)
VALUES (12, 'Pro Spring Security', 'Carlo Scarioni', '304503955', 'Test description one');
INSERT INTO geek_store.product_descriptions (product_id, title, author, isbn, description)
VALUES (13, 'Spring Boot in Action 1st Edition', 'Craig Walls ', '304503955', 'Test description one');
INSERT INTO geek_store.product_descriptions (product_id, title, author, isbn, description)
VALUES (14, 'Pro Spring Boot 1st ed. Edition', ' Felipe Gutierrez', '304503955', 'Test description one');
INSERT INTO geek_store.product_descriptions (product_id, title, author, isbn, description)
VALUES (15, 'Java 8 in Action: Lambdas, Streams, and functional-style programming', ' Raoul-Gabriel Urma ', '304503955', 'Test description one');
INSERT INTO geek_store.product_descriptions (product_id, title, author, isbn, description)
VALUES (16, 'Java Concurrency in Practice 1st Edition', ' Doug Lea, Joshua Bloch ', '304503955', 'Test description one');
INSERT INTO geek_store.product_descriptions (product_id, title, author, isbn, description)
VALUES (17, 'ava: The Ultimate Beginners Guide to Java Programming', 'Steve Tale ', '304503955', 'Test description one');
INSERT INTO geek_store.product_descriptions (product_id, title, author, isbn, description)
VALUES (18, 'Intro to Java Programming, Comprehensive Version', 'Intro to Java Programming, Comprehensive Version', '304503955', 'Test description one');
INSERT INTO geek_store.product_descriptions (product_id, title, author, isbn, description)
VALUES (19, 'Head First Design Patterns: A Brain-Friendly Guide', ' Eric Freeman', '304503955', 'Test description one');
INSERT INTO geek_store.product_descriptions (product_id, title, author, isbn, description)
VALUES (20, 'Regular Expression Pocket Reference', 'Joshua Bloch', '304503955', 'Test description one');
INSERT INTO geek_store.product_descriptions (product_id, title, author, isbn, description)
VALUES (21, 'The Java Native Interface', 'Joshua Bloch', '304503955', 'Test description one');

-- Art and architecture 22
INSERT INTO geek_store.product_descriptions (product_id, title, author, isbn, description)
VALUES (22, 'Styled: Secrets for Arranging Rooms, from Tabletops to Bookshelves', 'Emily Henderson', '304503955', 'Test description one');
INSERT INTO geek_store.product_descriptions (product_id, title, author, isbn, description)
VALUES (23, 'Wabi Sabi: The Japanese Art of Impermanence', 'Craig Walls ', '304503955', 'Test description one');
INSERT INTO geek_store.product_descriptions (product_id, title, author, isbn, description)
VALUES (24, 'A Guide to the Good Life: The Ancient Art of Stoic Joy', 'Robert Martin', '304503955', 'Test description one');
INSERT INTO geek_store.product_descriptions (product_id, title, author, isbn, description)
VALUES (25, 'How to Sell Your Art Online', 'GoF', '304503955', 'Test description one');
INSERT INTO geek_store.product_descriptions (product_id, title, author, isbn, description)
VALUES (26, 'ART/WORK: Everything You Need to Know ', ' Kathy Sierra', '304503955', 'Test description one');
INSERT INTO geek_store.product_descriptions (product_id, title, author, isbn, description)
VALUES (27, '33 Artists in 3 Acts', 'Herbert Schildt', '304503955', 'Test description one');
INSERT INTO geek_store.product_descriptions (product_id, title, author, isbn, description)
VALUES (28, 'You Say to Brick: The Life of Louis Kahn', 'Cay S. Horstmann', '304503955', 'Test description one');
INSERT INTO geek_store.product_descriptions (product_id, title, author, isbn, description)
VALUES (29, 'Alexander McQueen: Savage Beauty', ' Eric Freeman', '304503955', 'Test description one');
INSERT INTO geek_store.product_descriptions (product_id, title, author, isbn, description)
VALUES (30, 'Matisse/Diebenkorn', 'Joshua Bloch', '304503955', 'Test description one');
INSERT INTO geek_store.product_descriptions (product_id, title, author, isbn, description)
VALUES (31, 'Actionable Gamification: Beyond Points, Badges, and Leaderboards', 'Joshua Bloch', '304503955', 'Test description one');
INSERT INTO geek_store.product_descriptions (product_id, title, author, isbn, description)
VALUES (32, 'Creating Home: Design for Living ', 'Brian W. Kernighan and Dennis M. Ritchie', '304503955', 'C description');
INSERT INTO geek_store.product_descriptions (product_id, title, author, isbn, description)
VALUES (33, 'Our Q&A a Day: 3-Year Journal for 2 People', 'Carlo Scarioni', '304503955', 'Test description one');
INSERT INTO geek_store.product_descriptions (product_id, title, author, isbn, description)
VALUES (34, 'The Road Rage Coloring Book: Color Away Your Frustration', 'Craig Walls ', '304503955', 'Test description one');
INSERT INTO geek_store.product_descriptions (product_id, title, author, isbn, description)
VALUES (35, 'Release Your Anger: An Adult Coloring Book', ' Felipe Gutierrez', '304503955', 'Test description one');
INSERT INTO geek_store.product_descriptions (product_id, title, author, isbn, description)
VALUES (36, 'Adult Coloring Book Designs: Stress Relief Coloring Book', ' Raoul-Gabriel Urma ', '304503955', 'Test description one');
INSERT INTO geek_store.product_descriptions (product_id, title, author, isbn, description)
VALUES (37, 'In Fine Style: The Art of Tudor and Stuart Fashion', ' Doug Lea, Joshua Bloch ', '304503955', 'Test description one');
INSERT INTO geek_store.product_descriptions (product_id, title, author, isbn, description)
VALUES (38, 'Kylie Fashion', 'Steve Tale ', '304503955', 'Test description one');
INSERT INTO geek_store.product_descriptions (product_id, title, author, isbn, description)
VALUES (39, 'Paper Illusions: The Art of Isabelle de Borchgrave', 'Intro to Java Programming, Comprehensive Version', '304503955', 'Test description one');
INSERT INTO geek_store.product_descriptions (product_id, title, author, isbn, description)
VALUES (40, 'Yves Saint Laurent ', ' Eric Freeman', '304503955', 'Test description one');
INSERT INTO geek_store.product_descriptions (product_id, title, author, isbn, description)
VALUES (41, 'Kaleidoscope Wonders: Color Art for Everyone', 'Joshua Bloch', '304503955', 'Test description one');
INSERT INTO geek_store.product_descriptions (product_id, title, author, isbn, description)
VALUES (42, 'Notebook Doodles Super Cute: Coloring & Activity Book', 'Joshua Bloch', '304503955', 'Test description one');
INSERT INTO geek_store.product_descriptions (product_id, title, author, isbn, description)
VALUES (43, 'Treasures of the Royal Courts: Tudors, Stuarts and Russian Tsars', ' Eric Freeman', '304503955', 'Test description one');
INSERT INTO geek_store.product_descriptions (product_id, title, author, isbn, description)
VALUES (44, 'Medieval Costume and Fashion', 'Joshua Bloch', '304503955', 'Test description one');
INSERT INTO geek_store.product_descriptions (product_id, title, author, isbn, description)
VALUES (45, 'Strong Is the New Pretty: A Celebration of Girls Being Themselves ', 'Joshua Bloch', '304503955', 'Test description one');
INSERT INTO geek_store.product_descriptions (product_id, title, author, isbn, description)
VALUES (46, 'The Artist''s Way: A Spiritual Path to Higher Creativity', 'Joshua Bloch', '304503955', 'Test description one');
INSERT INTO geek_store.product_descriptions (product_id, title, author, isbn, description)
VALUES (47, 'The Artist''s Way: A Spiritual Path to Higher Creativity', 'Joshua Bloch', '304503955', 'Test description one');
INSERT INTO geek_store.product_descriptions (product_id, title, author, isbn, description)
VALUES (48, 'The Keys', 'Joshua Bloch', '304503955', 'Test description one');
INSERT INTO geek_store.product_descriptions (product_id, title, author, isbn, description)
VALUES (49, 'The Art of Spirited Away', 'Joshua Bloch', '304503955', 'Test description one');
INSERT INTO geek_store.product_descriptions (product_id, title, author, isbn, description)
VALUES (50, 'The Art of Over the Garden Wall ', 'Joshua Bloch', '304503955', 'Test description one');
INSERT INTO geek_store.product_descriptions (product_id, title, author, isbn, description)
VALUES (51, 'Drawing for the Absolute Beginner', 'Joshua Bloch', '304503955', 'Test description one');
INSERT INTO geek_store.product_descriptions (product_id, title, author, isbn, description)
VALUES (52, 'Everyday Watercolor: Learn to Paint Watercolor in 30 Days', 'Joshua Bloch', '304503955', 'Test description one');
INSERT INTO geek_store.product_descriptions (product_id, title, author, isbn, description)
VALUES (53, 'The Art of Howl''s Moving Castle', 'Joshua Bloch', '304503955', 'Test description one');
INSERT INTO geek_store.product_descriptions (product_id, title, author, isbn, description)
VALUES (54, 'The Art of Princess Mononoke', 'Joshua Bloch', '304503955', 'Test description one');
INSERT INTO geek_store.product_descriptions (product_id, title, author, isbn, description)
VALUES (55, 'Paris in Bloom', 'Joshua Bloch', '304503955', 'Test description one');
INSERT INTO geek_store.product_descriptions (product_id, title, author, isbn, description)
VALUES (56, 'Tabitha Soren: Fantasy Life: Baseball and the American Dream', 'Joshua Bloch', '304503955', 'Test description one');
INSERT INTO geek_store.product_descriptions (product_id, title, author, isbn, description)
VALUES (57, 'James Tissot: The Life of Christ', 'Joshua Bloch', '304503955', 'Test description one');
INSERT INTO geek_store.product_descriptions (product_id, title, author, isbn, description)
VALUES (58, 'The Dore Bible Illustrations', 'Joshua Bloch', '304503955', 'Test description one');
INSERT INTO geek_store.product_descriptions (product_id, title, author, isbn, description)
VALUES (59, 'Beginner''s Guide to Sculpting Characters in Clay', 'Joshua Bloch', '304503955', 'Test description one');
INSERT INTO geek_store.product_descriptions (product_id, title, author, isbn, description)
VALUES (60, 'You Must Change Your Life', 'Joshua Bloch', '304503955', 'Test description one');
INSERT INTO geek_store.product_descriptions (product_id, title, author, isbn, description)
VALUES (61, 'Creating Origami', 'Joshua Bloch', '304503955', 'Test description one');
INSERT INTO geek_store.product_descriptions (product_id, title, author, isbn, description)
VALUES (62, 'A Girl Called Ana Teaches Kittens How To Draw', 'Joshua Bloch', '304503955', 'Test description one');





CREATE TABLE geek_store.reviews (
  id            SERIAL,
  date          DATE NOT NULL,
  rating        INT,
  advantages    VARCHAR(45),
  disadvantages VARCHAR(45),
  comment       VARCHAR(500),
  customer_id   INT  NOT NULL,
  product_id    INT  NOT NULL,
  FOREIGN KEY (customer_id) REFERENCES geek_store.customers (id)
  ON UPDATE CASCADE
  ON DELETE CASCADE,
  FOREIGN KEY (product_id) REFERENCES geek_store.products (id)
  ON UPDATE CASCADE
  ON DELETE CASCADE,
  PRIMARY KEY (id)
);

INSERT INTO geek_store.reviews (date, rating, advantages, disadvantages, customer_id, product_id)
VALUES (CURRENT_DATE, 5, 'super', 'none', 1, 1);
INSERT INTO geek_store.reviews (date, rating, advantages, disadvantages, customer_id, product_id)
VALUES (CURRENT_DATE, 2, 'none', 'bad', 2, 2);
INSERT INTO geek_store.reviews (date, rating, advantages, disadvantages, customer_id, product_id)
VALUES ('2016-05-06', 5, 'Good ', 'none', 3, 3);
INSERT INTO geek_store.reviews (date, rating, advantages, disadvantages, customer_id, product_id)
VALUES ('2012-05-06', 5, 'Super', 'none', 4, 4);
INSERT INTO geek_store.reviews (date, rating, advantages, disadvantages, customer_id, product_id)
VALUES ('2012-05-06', 5, 'Super', 'none', 5, 5);
INSERT INTO geek_store.reviews (date, rating, advantages, disadvantages, customer_id, product_id)
VALUES ('2012-05-06', 5, 'Super', 'none', 6, 6);
INSERT INTO geek_store.reviews (date, rating, advantages, disadvantages, customer_id, product_id)
VALUES ('2012-05-06', 5, 'Super', 'none', 7, 7);
INSERT INTO geek_store.reviews (date, rating, advantages, disadvantages, customer_id, product_id)
VALUES ('2012-05-06', 5, 'Super', 'none', 8, 8);
INSERT INTO geek_store.reviews (date, rating, advantages, disadvantages, customer_id, product_id)
VALUES ('2012-05-06', 5, 'Super', 'none', 9, 9);
INSERT INTO geek_store.reviews (date, rating, advantages, disadvantages, customer_id, product_id)
VALUES ('2012-05-06', 5, 'Super', 'none', 10, 10);
INSERT INTO geek_store.reviews (date, rating, advantages, disadvantages, customer_id, product_id)
VALUES (CURRENT_DATE, 5, 'super', 'none', 11, 1);
INSERT INTO geek_store.reviews (date, rating, advantages, disadvantages, customer_id, product_id)
VALUES (CURRENT_DATE, 2, 'none', 'bad', 12, 2);
INSERT INTO geek_store.reviews (date, rating, advantages, disadvantages, customer_id, product_id)
VALUES ('2016-05-06', 5, 'Good ', 'none', 13, 3);
INSERT INTO geek_store.reviews (date, rating, advantages, disadvantages, customer_id, product_id)
VALUES ('2012-05-06', 5, 'Super', 'none', 14, 4);
INSERT INTO geek_store.reviews (date, rating, advantages, disadvantages, customer_id, product_id)
VALUES ('2012-05-06', 5, 'Super', 'none', 15, 5);
INSERT INTO geek_store.reviews (date, rating, advantages, disadvantages, customer_id, product_id)
VALUES ('2012-05-06', 5, 'Super', 'none', 16, 6);
INSERT INTO geek_store.reviews (date, rating, advantages, disadvantages, customer_id, product_id)
VALUES ('2012-05-06', 5, 'Super', 'none', 17, 7);
INSERT INTO geek_store.reviews (date, rating, advantages, disadvantages, customer_id, product_id)
VALUES ('2012-05-06', 5, 'Super', 'none', 18, 8);
INSERT INTO geek_store.reviews (date, rating, advantages, disadvantages, customer_id, product_id)
VALUES ('2012-05-06', 5, 'Super', 'none', 9, 9);
INSERT INTO geek_store.reviews (date, rating, advantages, disadvantages, customer_id, product_id)
VALUES ('2012-05-06', 5, 'Super', 'none', 10, 10);

CREATE TABLE geek_store.product_image (
  product_id INT NOT NULL,
  src_image  VARCHAR(1000),
  FOREIGN KEY (product_id) REFERENCES geek_store.products (id)
  ON UPDATE CASCADE
  ON DELETE CASCADE
);

INSERT INTO geek_store.product_image (product_id, src_image) VALUES (1, '/img/books/1.jpg');
INSERT INTO geek_store.product_image (product_id, src_image) VALUES (2, '/img/books/7.jpg');
INSERT INTO geek_store.product_image (product_id, src_image) VALUES (3, '/img/books/4.jpg');
INSERT INTO geek_store.product_image (product_id, src_image) VALUES (4, '/img/books/6.jpg');
INSERT INTO geek_store.product_image (product_id, src_image) VALUES (5, '/img/books/5.jpg');
INSERT INTO geek_store.product_image (product_id, src_image) VALUES (6, '/img/books/2.jpg');
INSERT INTO geek_store.product_image (product_id, src_image) VALUES (7, '/img/books/3.jpg');
INSERT INTO geek_store.product_image (product_id, src_image) VALUES (8, '/img/books/8.jpg');
INSERT INTO geek_store.product_image (product_id, src_image) VALUES (9, '/img/books/9.jpg');
INSERT INTO geek_store.product_image (product_id, src_image) VALUES (10, '/img/books/10.jpg');
INSERT INTO geek_store.product_image (product_id, src_image) VALUES (11, '/img/books/11.jpg');
INSERT INTO geek_store.product_image (product_id, src_image) VALUES (12, '/img/books/12.jpg');
INSERT INTO geek_store.product_image (product_id, src_image) VALUES (13, '/img/books/13.jpg');
INSERT INTO geek_store.product_image (product_id, src_image) VALUES (14, '/img/books/14.jpg');
INSERT INTO geek_store.product_image (product_id, src_image) VALUES (15, '/img/books/15.jpg');
INSERT INTO geek_store.product_image (product_id, src_image) VALUES (16, '/img/books/16.jpg');
INSERT INTO geek_store.product_image (product_id, src_image) VALUES (17, '/img/books/17.jpg');
INSERT INTO geek_store.product_image (product_id, src_image) VALUES (18, '/img/books/18.jpg');
INSERT INTO geek_store.product_image (product_id, src_image) VALUES (19, '/img/books/1.jpg');
INSERT INTO geek_store.product_image (product_id, src_image) VALUES (20, '/img/books/1.jpg');
INSERT INTO geek_store.product_image (product_id, src_image) VALUES (21, '/img/books/1.jpg');

INSERT INTO geek_store.product_image (product_id, src_image) VALUES (22, '/img/books/art_&_architecture/22.jpg');
INSERT INTO geek_store.product_image (product_id, src_image) VALUES (23, '/img/books/art_&_architecture/23.jpg');
INSERT INTO geek_store.product_image (product_id, src_image) VALUES (24, '/img/books/art_&_architecture/24.jpg');
INSERT INTO geek_store.product_image (product_id, src_image) VALUES (25, '/img/books/art_&_architecture/25.jpg');
INSERT INTO geek_store.product_image (product_id, src_image) VALUES (26, '/img/books/art_&_architecture/26.jpg');
INSERT INTO geek_store.product_image (product_id, src_image) VALUES (27, '/img/books/art_&_architecture/27.jpg');
INSERT INTO geek_store.product_image (product_id, src_image) VALUES (28, '/img/books/art_&_architecture/28.jpg');
INSERT INTO geek_store.product_image (product_id, src_image) VALUES (29, '/img/books/art_&_architecture/29.jpg');
INSERT INTO geek_store.product_image (product_id, src_image) VALUES (30, '/img/books/art_&_architecture/30.jpg');
INSERT INTO geek_store.product_image (product_id, src_image) VALUES (31, '/img/books/art_&_architecture/31.jpg');
INSERT INTO geek_store.product_image (product_id, src_image) VALUES (32, '/img/books/art_&_architecture/32.jpg');
INSERT INTO geek_store.product_image (product_id, src_image) VALUES (33, '/img/books/art_&_architecture/33.jpg');
INSERT INTO geek_store.product_image (product_id, src_image) VALUES (34, '/img/books/art_&_architecture/34.jpg');
INSERT INTO geek_store.product_image (product_id, src_image) VALUES (35, '/img/books/art_&_architecture/35.jpg');
INSERT INTO geek_store.product_image (product_id, src_image) VALUES (36, '/img/books/art_&_architecture/36.jpg');
INSERT INTO geek_store.product_image (product_id, src_image) VALUES (37, '/img/books/art_&_architecture/37.jpg');
INSERT INTO geek_store.product_image (product_id, src_image) VALUES (38, '/img/books/art_&_architecture/38.jpg');
INSERT INTO geek_store.product_image (product_id, src_image) VALUES (39, '/img/books/art_&_architecture/39.jpg');
INSERT INTO geek_store.product_image (product_id, src_image) VALUES (40, '/img/books/art_&_architecture/40.jpg');
INSERT INTO geek_store.product_image (product_id, src_image) VALUES (41, '/img/books/art_&_architecture/41.jpg');
INSERT INTO geek_store.product_image (product_id, src_image) VALUES (42, '/img/books/art_&_architecture/42.jpg');
INSERT INTO geek_store.product_image (product_id, src_image) VALUES (43, '/img/books/art_&_architecture/43.jpg');
INSERT INTO geek_store.product_image (product_id, src_image) VALUES (44, '/img/books/art_&_architecture/44.jpg');
INSERT INTO geek_store.product_image (product_id, src_image) VALUES (45, '/img/books/art_&_architecture/45.jpg');
INSERT INTO geek_store.product_image (product_id, src_image) VALUES (46, '/img/books/art_&_architecture/46.jpg');
INSERT INTO geek_store.product_image (product_id, src_image) VALUES (47, '/img/books/art_&_architecture/47.jpg');
INSERT INTO geek_store.product_image (product_id, src_image) VALUES (48, '/img/books/art_&_architecture/48.jpg');
INSERT INTO geek_store.product_image (product_id, src_image) VALUES (49, '/img/books/art_&_architecture/49.jpg');
INSERT INTO geek_store.product_image (product_id, src_image) VALUES (50, '/img/books/art_&_architecture/50.jpg');
INSERT INTO geek_store.product_image (product_id, src_image) VALUES (51, '/img/books/art_&_architecture/51.jpg');
INSERT INTO geek_store.product_image (product_id, src_image) VALUES (52, '/img/books/art_&_architecture/52.jpg');
INSERT INTO geek_store.product_image (product_id, src_image) VALUES (53, '/img/books/art_&_architecture/53.jpg');
INSERT INTO geek_store.product_image (product_id, src_image) VALUES (54, '/img/books/art_&_architecture/54.jpg');
INSERT INTO geek_store.product_image (product_id, src_image) VALUES (55, '/img/books/art_&_architecture/55.jpg');
INSERT INTO geek_store.product_image (product_id, src_image) VALUES (56, '/img/books/art_&_architecture/56.jpg');
INSERT INTO geek_store.product_image (product_id, src_image) VALUES (57, '/img/books/art_&_architecture/57.jpg');
INSERT INTO geek_store.product_image (product_id, src_image) VALUES (58, '/img/books/art_&_architecture/58.jpg');
INSERT INTO geek_store.product_image (product_id, src_image) VALUES (59, '/img/books/art_&_architecture/59.jpg');
INSERT INTO geek_store.product_image (product_id, src_image) VALUES (60, '/img/books/art_&_architecture/60.jpg');
INSERT INTO geek_store.product_image (product_id, src_image) VALUES (61, '/img/books/art_&_architecture/61.jpg');
INSERT INTO geek_store.product_image (product_id, src_image) VALUES (62, '/img/books/art_&_architecture/62.jpg');
