CREATE  TABLE geek_store.users (
  username VARCHAR(45) NOT NULL ,
  password VARCHAR(100) NOT NULL ,
  enabled BOOLEAN NOT NULL DEFAULT TRUE ,
  PRIMARY KEY (username));

INSERT INTO geek_store.users(username,password,enabled)
VALUES ('admin','$2a$10$EblZqNptyYvcLm/VwDCVAuBjzZOI7khzdyGPBr08PpIi0na624b8.', true);


CREATE  TABLE geek_store.roles (
  id SERIAL,
  email VARCHAR(45) NOT NULL ,
  role VARCHAR(45) NOT NULL ,
  PRIMARY KEY (id));

INSERT INTO geek_store.roles(email,role)
VALUES ('admin', 'ADMIN');
INSERT INTO geek_store.roles(email,role)
VALUES ('user@mail.com', 'USER');