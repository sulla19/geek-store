CREATE TABLE geek_store.orders (
  id                SERIAL,
  last_modified     DATE          NOT NULL,
  customer_id       INT           NOT NULL,
  total_price       DECIMAL(8, 2) NOT NULL,
  payment_type      VARCHAR(45)   NOT NULL,
  shipping_type     VARCHAR(45)   NOT NULL,
  contact_telephone VARCHAR(50)   NOT NULL,
  PRIMARY KEY (id)
);

CREATE TABLE geek_store.delivery (
  id          SERIAL,
  order_id    INT         NOT NULL,
  company     VARCHAR(45) NOT NULL,
  country     VARCHAR(45) NOT NULL,
  city        VARCHAR(45) NOT NULL,
  address     VARCHAR(45) NOT NULL,
  postal_code VARCHAR(45) NOT NULL,
  telephone   VARCHAR(45) NOT NULL,
  FOREIGN KEY (order_id) REFERENCES geek_store.orders (id) ON UPDATE CASCADE ON DELETE CASCADE,
  PRIMARY KEY (id)
);



INSERT INTO geek_store.orders (last_modified, customer_id, total_price, payment_type, shipping_type,  contact_telephone)
VALUES ('2016-01-07', 1, 455, 'CREDITCARD', 'NATIONAL',  '(066)679-48-27');
INSERT INTO geek_store.orders (last_modified, customer_id, total_price, payment_type, shipping_type,  contact_telephone)
VALUES ('2016-01-25', 2, 2655, 'CREDITCARD', 'NATIONAL', '(066)679-48-27');
INSERT INTO geek_store.orders (last_modified, customer_id, total_price, payment_type, shipping_type,  contact_telephone)
VALUES ('2016-02-03', 3, 3455, 'CREDITCARD', 'NATIONAL', '(066)679-48-27');
INSERT INTO geek_store.orders (last_modified, customer_id, total_price, payment_type, shipping_type,  contact_telephone)
VALUES ('2016-02-03', 4, 3455, 'CREDITCARD', 'NATIONAL','(066)679-48-27');
INSERT INTO geek_store.orders (last_modified, customer_id, total_price, payment_type, shipping_type,  contact_telephone)
VALUES ('2016-03-03', 5, 3455, 'CREDITCARD', 'NATIONAL', '(066)679-48-27');
INSERT INTO geek_store.orders (last_modified, customer_id, total_price, payment_type, shipping_type,  contact_telephone)
VALUES ('2016-03-03', 6, 3455, 'CREDITCARD', 'NATIONAL', '(066)679-48-27');
INSERT INTO geek_store.orders (last_modified, customer_id, total_price, payment_type, shipping_type,  contact_telephone)
VALUES ('2016-04-03', 7, 3455, 'CREDITCARD', 'NATIONAL', '(066)679-48-27');
INSERT INTO geek_store.orders (last_modified, customer_id, total_price, payment_type, shipping_type,  contact_telephone)
VALUES ('2016-04-03', 8, 3455, 'CREDITCARD', 'NATIONAL', '(066)679-48-27');
INSERT INTO geek_store.orders (last_modified, customer_id, total_price, payment_type, shipping_type,  contact_telephone)
VALUES ('2016-05-03', 9, 3455, 'CREDITCARD', 'NATIONAL', '(066)679-48-27');
INSERT INTO geek_store.orders (last_modified, customer_id, total_price, payment_type, shipping_type,  contact_telephone)
VALUES ('2016-05-03', 10, 3455, 'CREDITCARD', 'NATIONAL', '(066)679-48-27');
INSERT INTO geek_store.orders (last_modified, customer_id, total_price, payment_type, shipping_type,  contact_telephone)
VALUES ('2016-06-03', 11, 3455, 'CREDITCARD', 'NATIONAL', '(066)679-48-27');
INSERT INTO geek_store.orders (last_modified, customer_id, total_price, payment_type, shipping_type,  contact_telephone)
VALUES ('2016-06-03', 12, 3455, 'CREDITCARD', 'NATIONAL', '(066)679-48-27');
INSERT INTO geek_store.orders (last_modified, customer_id, total_price, payment_type, shipping_type,  contact_telephone)
VALUES ('2016-07-03', 13, 3455, 'CREDITCARD', 'NATIONAL',  '(066)679-48-27');
INSERT INTO geek_store.orders (last_modified, customer_id, total_price, payment_type, shipping_type,  contact_telephone)
VALUES ('2016-07-03', 14, 3455, 'CREDITCARD', 'NATIONAL',  '(066)679-48-27');
INSERT INTO geek_store.orders (last_modified, customer_id, total_price, payment_type, shipping_type,  contact_telephone)
VALUES ('2016-08-03', 15, 3455, 'CREDITCARD', 'NATIONAL',  '(066)679-48-27');
INSERT INTO geek_store.orders (last_modified, customer_id, total_price, payment_type, shipping_type,  contact_telephone)
VALUES ('2016-08-03', 16, 3455, 'CREDITCARD', 'NATIONAL',  '(066)679-48-27');
INSERT INTO geek_store.orders (last_modified, customer_id, total_price, payment_type, shipping_type,  contact_telephone)
VALUES ('2016-09-03', 17, 3455, 'CREDITCARD', 'NATIONAL',  '(066)679-48-27');
INSERT INTO geek_store.orders (last_modified, customer_id, total_price, payment_type, shipping_type,  contact_telephone)
VALUES ('2016-09-03', 18, 3455, 'CREDITCARD', 'NATIONAL',  '(066)679-48-27');
INSERT INTO geek_store.orders (last_modified, customer_id, total_price, payment_type, shipping_type,  contact_telephone)
VALUES ('2016-10-03', 19, 3455, 'CREDITCARD', 'NATIONAL',  '(066)679-48-27');
INSERT INTO geek_store.orders (last_modified, customer_id, total_price, payment_type, shipping_type,  contact_telephone)
VALUES ('2016-10-03', 20, 3455, 'CREDITCARD', 'NATIONAL',  '(066)679-48-27');
INSERT INTO geek_store.orders (last_modified, customer_id, total_price, payment_type, shipping_type,  contact_telephone)
VALUES ('2016-11-07', 21, 455, 'CREDITCARD', 'NATIONAL',  '(066)679-48-27');
INSERT INTO geek_store.orders (last_modified, customer_id, total_price, payment_type, shipping_type,  contact_telephone)
VALUES ('2016-11-03', 22, 2655, 'CREDITCARD', 'NATIONAL',  '(066)679-48-27');
INSERT INTO geek_store.orders (last_modified, customer_id, total_price, payment_type, shipping_type,  contact_telephone)
VALUES ('2016-12-03', 23, 3455, 'CREDITCARD', 'NATIONAL', '(066)679-48-27');
INSERT INTO geek_store.orders (last_modified, customer_id, total_price, payment_type, shipping_type,  contact_telephone)
VALUES ('2016-12-03', 24, 3455, 'CREDITCARD', 'NATIONAL',  '(066)679-48-27');
INSERT INTO geek_store.orders (last_modified, customer_id, total_price, payment_type, shipping_type,  contact_telephone)
VALUES ('2016-01-03', 25, 3455, 'CREDITCARD', 'NATIONAL',  '(066)679-48-27');
INSERT INTO geek_store.orders (last_modified, customer_id, total_price, payment_type, shipping_type,  contact_telephone)
VALUES ('2016-01-03', 26, 3455, 'CREDITCARD', 'NATIONAL',  '(066)679-48-27');
INSERT INTO geek_store.orders (last_modified, customer_id, total_price, payment_type, shipping_type,  contact_telephone)
VALUES ('2016-02-03', 27, 3455, 'CREDITCARD', 'NATIONAL', '(066)679-48-27');
INSERT INTO geek_store.orders (last_modified, customer_id, total_price, payment_type, shipping_type,  contact_telephone)
VALUES ('2016-02-03', 28, 3455, 'CREDITCARD', 'NATIONAL',  '(066)679-48-27');
INSERT INTO geek_store.orders (last_modified, customer_id, total_price, payment_type, shipping_type,  contact_telephone)
VALUES ('2016-03-03', 29, 3455, 'CREDITCARD', 'NATIONAL',  '(066)679-48-27');
INSERT INTO geek_store.orders (last_modified, customer_id, total_price, payment_type, shipping_type,  contact_telephone)
VALUES ('2016-03-03', 30, 3455, 'CREDITCARD', 'NATIONAL', '(066)679-48-27');
INSERT INTO geek_store.orders (last_modified, customer_id, total_price, payment_type, shipping_type,  contact_telephone)
VALUES ('2016-04-03', 31, 3455, 'CREDITCARD', 'NATIONAL',  '(066)679-48-27');
INSERT INTO geek_store.orders (last_modified, customer_id, total_price, payment_type, shipping_type,  contact_telephone)
VALUES ('2016-04-03', 32, 3455, 'CREDITCARD', 'NATIONAL',  '(066)679-48-27');
INSERT INTO geek_store.orders (last_modified, customer_id, total_price, payment_type, shipping_type,  contact_telephone)
VALUES ('2016-05-03', 33, 3455, 'CREDITCARD', 'NATIONAL',  '(066)679-48-27');
INSERT INTO geek_store.orders (last_modified, customer_id, total_price, payment_type, shipping_type,  contact_telephone)
VALUES ('2016-05-03', 34, 3455, 'CREDITCARD', 'NATIONAL',  '(066)679-48-27');
INSERT INTO geek_store.orders (last_modified, customer_id, total_price, payment_type, shipping_type,  contact_telephone)
VALUES ('2016-06-03', 35, 3455, 'CREDITCARD', 'NATIONAL',  '(066)679-48-27');
INSERT INTO geek_store.orders (last_modified, customer_id, total_price, payment_type, shipping_type,  contact_telephone)
VALUES ('2016-06-03', 36, 3455, 'CREDITCARD', 'NATIONAL',  '(066)679-48-27');
INSERT INTO geek_store.orders (last_modified, customer_id, total_price, payment_type, shipping_type,  contact_telephone)
VALUES ('2016-07-03', 37, 3455, 'CREDITCARD', 'NATIONAL',  '(066)679-48-27');
INSERT INTO geek_store.orders (last_modified, customer_id, total_price, payment_type, shipping_type,  contact_telephone)
VALUES ('2016-07-03', 38, 3455, 'CREDITCARD', 'NATIONAL',  '(066)679-48-27');
INSERT INTO geek_store.orders (last_modified, customer_id, total_price, payment_type, shipping_type,  contact_telephone)
VALUES ('2016-08-03', 39, 3455, 'CREDITCARD', 'NATIONAL',  '(066)679-48-27');
INSERT INTO geek_store.orders (last_modified, customer_id, total_price, payment_type, shipping_type,  contact_telephone)
VALUES ('2016-08-03', 40, 3455, 'CREDITCARD', 'NATIONAL',  '(066)679-48-27');
INSERT INTO geek_store.orders (last_modified, customer_id, total_price, payment_type, shipping_type,  contact_telephone)
VALUES ('2016-08-03', 2, 3455, 'CREDITCARD', 'NATIONAL',  '(066)679-48-27');
INSERT INTO geek_store.orders (last_modified, customer_id, total_price, payment_type, shipping_type,  contact_telephone)
VALUES ('2016-08-03', 2, 3455, 'CREDITCARD', 'NATIONAL',  '(066)679-48-27');
INSERT INTO geek_store.orders (last_modified, customer_id, total_price, payment_type, shipping_type,  contact_telephone)
VALUES ('2016-08-03', 3, 3455, 'CREDITCARD', 'NATIONAL',  '(066)679-48-27');
INSERT INTO geek_store.orders (last_modified, customer_id, total_price, payment_type, shipping_type,  contact_telephone)
VALUES ('2016-08-03', 3, 3455, 'CREDITCARD', 'NATIONAL',  '(066)679-48-27');
INSERT INTO geek_store.orders (last_modified, customer_id, total_price, payment_type, shipping_type,  contact_telephone)
VALUES ('2016-08-03', 4, 3455, 'CREDITCARD', 'NATIONAL',  '(066)679-48-27');
INSERT INTO geek_store.orders (last_modified, customer_id, total_price, payment_type, shipping_type,  contact_telephone)
VALUES ('2016-08-03', 4, 3455, 'CREDITCARD', 'NATIONAL',  '(066)679-48-27');


INSERT INTO geek_store.delivery(order_id, company, country, city, address, postal_code, telephone)
VALUES(1, 'Delivery', 'USA', 'Belfast', 'Washington str, 76', '46795', '(9789)555-55-55');
INSERT INTO geek_store.delivery(order_id, company, country, city, address, postal_code, telephone)
VALUES(2, 'Delivery', 'USA', 'Belfast', 'Washington str, 76', '46795', '(9789)555-55-55');
INSERT INTO geek_store.delivery(order_id, company, country, city, address, postal_code, telephone)
VALUES(3, 'Delivery', 'USA', 'Belfast', 'Washington str, 76', '46795', '(9789)555-55-55');
INSERT INTO geek_store.delivery(order_id, company, country, city, address, postal_code, telephone)
VALUES(4, 'Delivery', 'USA', 'Belfast', 'Washington str, 76', '46795', '(9789)555-55-55');
INSERT INTO geek_store.delivery(order_id, company, country, city, address, postal_code, telephone)
VALUES(5, 'Delivery', 'USA', 'Belfast', 'Washington str, 76', '46795', '(9789)555-55-55');
INSERT INTO geek_store.delivery(order_id, company, country, city, address, postal_code, telephone)
VALUES(6, 'Delivery', 'USA', 'Belfast', 'Washington str, 76', '46795', '(9789)555-55-55');
INSERT INTO geek_store.delivery(order_id, company, country, city, address, postal_code, telephone)
VALUES(7, 'Delivery', 'USA', 'Belfast', 'Washington str, 76', '46795', '(9789)555-55-55');
INSERT INTO geek_store.delivery(order_id, company, country, city, address, postal_code, telephone)
VALUES(8, 'Delivery', 'USA', 'Belfast', 'Washington str, 76', '46795', '(9789)555-55-55');
INSERT INTO geek_store.delivery(order_id, company, country, city, address, postal_code, telephone)
VALUES(9, 'Delivery', 'USA', 'Belfast', 'Washington str, 76', '46795', '(9789)555-55-55');
INSERT INTO geek_store.delivery(order_id, company, country, city, address, postal_code, telephone)
VALUES(10, 'Delivery', 'USA', 'Belfast', 'Washington str, 76', '46795', '(9789)555-55-55');
INSERT INTO geek_store.delivery(order_id, company, country, city, address, postal_code, telephone)
VALUES(11, 'Delivery', 'USA', 'Belfast', 'Washington str, 76', '46795', '(9789)555-55-55');
INSERT INTO geek_store.delivery(order_id, company, country, city, address, postal_code, telephone)
VALUES(12, 'Delivery', 'USA', 'Belfast', 'Washington str, 76', '46795', '(9789)555-55-55');
INSERT INTO geek_store.delivery(order_id, company, country, city, address, postal_code, telephone)
VALUES(13, 'Delivery', 'USA', 'Belfast', 'Washington str, 76', '46795', '(9789)555-55-55');
INSERT INTO geek_store.delivery(order_id, company, country, city, address, postal_code, telephone)
VALUES(14, 'Delivery', 'USA', 'Belfast', 'Washington str, 76', '46795', '(9789)555-55-55');
INSERT INTO geek_store.delivery(order_id, company, country, city, address, postal_code, telephone)
VALUES(15, 'Delivery', 'USA', 'Belfast', 'Washington str, 76', '46795', '(9789)555-55-55');
INSERT INTO geek_store.delivery(order_id, company, country, city, address, postal_code, telephone)
VALUES(16, 'Delivery', 'USA', 'Belfast', 'Washington str, 76', '46795', '(9789)555-55-55');
INSERT INTO geek_store.delivery(order_id, company, country, city, address, postal_code, telephone)
VALUES(17, 'Delivery', 'USA', 'Belfast', 'Washington str, 76', '46795', '(9789)555-55-55');
INSERT INTO geek_store.delivery(order_id, company, country, city, address, postal_code, telephone)
VALUES(18, 'Delivery', 'USA', 'Belfast', 'Washington str, 76', '46795', '(9789)555-55-55');
INSERT INTO geek_store.delivery(order_id, company, country, city, address, postal_code, telephone)
VALUES(19, 'Delivery', 'USA', 'Belfast', 'Washington str, 76', '46795', '(9789)555-55-55');
INSERT INTO geek_store.delivery(order_id, company, country, city, address, postal_code, telephone)
VALUES(20, 'Delivery', 'USA', 'Belfast', 'Washington str, 76', '46795', '(9789)555-55-55');
INSERT INTO geek_store.delivery(order_id, company, country, city, address, postal_code, telephone)
VALUES(21, 'Delivery', 'USA', 'Belfast', 'Washington str, 76', '46795', '(9789)555-55-55');
INSERT INTO geek_store.delivery(order_id, company, country, city, address, postal_code, telephone)
VALUES(22, 'Delivery', 'USA', 'Belfast', 'Washington str, 76', '46795', '(9789)555-55-55');
INSERT INTO geek_store.delivery(order_id, company, country, city, address, postal_code, telephone)
VALUES(23, 'Delivery', 'USA', 'Belfast', 'Washington str, 76', '46795', '(9789)555-55-55');
INSERT INTO geek_store.delivery(order_id, company, country, city, address, postal_code, telephone)
VALUES(24, 'Delivery', 'USA', 'Belfast', 'Washington str, 76', '46795', '(9789)555-55-55');
INSERT INTO geek_store.delivery(order_id, company, country, city, address, postal_code, telephone)
VALUES(25, 'Delivery', 'USA', 'Belfast', 'Washington str, 76', '46795', '(9789)555-55-55');
INSERT INTO geek_store.delivery(order_id, company, country, city, address, postal_code, telephone)
VALUES(26, 'Delivery', 'USA', 'Belfast', 'Washington str, 76', '46795', '(9789)555-55-55');
INSERT INTO geek_store.delivery(order_id, company, country, city, address, postal_code, telephone)
VALUES(27, 'Delivery', 'USA', 'Belfast', 'Washington str, 76', '46795', '(9789)555-55-55');
INSERT INTO geek_store.delivery(order_id, company, country, city, address, postal_code, telephone)
VALUES(28, 'Delivery', 'USA', 'Belfast', 'Washington str, 76', '46795', '(9789)555-55-55');
INSERT INTO geek_store.delivery(order_id, company, country, city, address, postal_code, telephone)
VALUES(29, 'Delivery', 'USA', 'Belfast', 'Washington str, 76', '46795', '(9789)555-55-55');
INSERT INTO geek_store.delivery(order_id, company, country, city, address, postal_code, telephone)
VALUES(30, 'Delivery', 'USA', 'Belfast', 'Washington str, 76', '46795', '(9789)555-55-55');
INSERT INTO geek_store.delivery(order_id, company, country, city, address, postal_code, telephone)
VALUES(31, 'Delivery', 'USA', 'Belfast', 'Washington str, 76', '46795', '(9789)555-55-55');
INSERT INTO geek_store.delivery(order_id, company, country, city, address, postal_code, telephone)
VALUES(32, 'Delivery', 'USA', 'Belfast', 'Washington str, 76', '46795', '(9789)555-55-55');
INSERT INTO geek_store.delivery(order_id, company, country, city, address, postal_code, telephone)
VALUES(33, 'Delivery', 'USA', 'Belfast', 'Washington str, 76', '46795', '(9789)555-55-55');
INSERT INTO geek_store.delivery(order_id, company, country, city, address, postal_code, telephone)
VALUES(34, 'Delivery', 'USA', 'Belfast', 'Washington str, 76', '46795', '(9789)555-55-55');
INSERT INTO geek_store.delivery(order_id, company, country, city, address, postal_code, telephone)
VALUES(35, 'Delivery', 'USA', 'Belfast', 'Washington str, 76', '46795', '(9789)555-55-55');
INSERT INTO geek_store.delivery(order_id, company, country, city, address, postal_code, telephone)
VALUES(36, 'Delivery', 'USA', 'Belfast', 'Washington str, 76', '46795', '(9789)555-55-55');
INSERT INTO geek_store.delivery(order_id, company, country, city, address, postal_code, telephone)
VALUES(37, 'Delivery', 'USA', 'Belfast', 'Washington str, 76', '46795', '(9789)555-55-55');
INSERT INTO geek_store.delivery(order_id, company, country, city, address, postal_code, telephone)
VALUES(38, 'Delivery', 'USA', 'Belfast', 'Washington str, 76', '46795', '(9789)555-55-55');
INSERT INTO geek_store.delivery(order_id, company, country, city, address, postal_code, telephone)
VALUES(39, 'Delivery', 'USA', 'Belfast', 'Washington str, 76', '46795', '(9789)555-55-55');
INSERT INTO geek_store.delivery(order_id, company, country, city, address, postal_code, telephone)
VALUES(40, 'Delivery', 'USA', 'Belfast', 'Washington str, 76', '46795', '(9789)555-55-55');
INSERT INTO geek_store.delivery(order_id, company, country, city, address, postal_code, telephone)
VALUES(41, 'Delivery', 'USA', 'Belfast', 'Washington str, 76', '46795', '(9789)555-55-55');
INSERT INTO geek_store.delivery(order_id, company, country, city, address, postal_code, telephone)
VALUES(42, 'Delivery', 'USA', 'Belfast', 'Washington str, 76', '46795', '(9789)555-55-55');
INSERT INTO geek_store.delivery(order_id, company, country, city, address, postal_code, telephone)
VALUES(43, 'Delivery', 'USA', 'Belfast', 'Washington str, 76', '46795', '(9789)555-55-55');
INSERT INTO geek_store.delivery(order_id, company, country, city, address, postal_code, telephone)
VALUES(44, 'Delivery', 'USA', 'Belfast', 'Washington str, 76', '46795', '(9789)555-55-55');
INSERT INTO geek_store.delivery(order_id, company, country, city, address, postal_code, telephone)
VALUES(45, 'Delivery', 'USA', 'Belfast', 'Washington str, 76', '46795', '(9789)555-55-55');
INSERT INTO geek_store.delivery(order_id, company, country, city, address, postal_code, telephone)
VALUES(46, 'Delivery', 'USA', 'Belfast', 'Washington str, 76', '46795', '(9789)555-55-55');

CREATE TABLE geek_store.orders_products (
  id                SERIAL,
  product_id       INT NOT NULL,
  order_id         INT NOT NULL,
  FOREIGN KEY (product_id) REFERENCES geek_store.products(id),
  FOREIGN KEY (order_id) REFERENCES geek_store.orders(id),
  PRIMARY KEY (id)
);

INSERT INTO geek_store.orders_products (product_id, order_id) VALUES (2,1);
INSERT INTO geek_store.orders_products (product_id, order_id) VALUES (4,1);
INSERT INTO geek_store.orders_products (product_id, order_id) VALUES (5,1);
INSERT INTO geek_store.orders_products (product_id, order_id) VALUES (17,2);
INSERT INTO geek_store.orders_products (product_id, order_id) VALUES (14,2);
INSERT INTO geek_store.orders_products (product_id, order_id) VALUES (5,3);
INSERT INTO geek_store.orders_products (product_id, order_id) VALUES (7,3);
INSERT INTO geek_store.orders_products (product_id, order_id) VALUES (20,3);
INSERT INTO geek_store.orders_products (product_id, order_id) VALUES (12,10);
INSERT INTO geek_store.orders_products (product_id, order_id) VALUES (12,35);
INSERT INTO geek_store.orders_products (product_id, order_id) VALUES (11,4);
INSERT INTO geek_store.orders_products (product_id, order_id) VALUES (14,13);
INSERT INTO geek_store.orders_products (product_id, order_id) VALUES (7,14);
INSERT INTO geek_store.orders_products (product_id, order_id) VALUES (20,15);
INSERT INTO geek_store.orders_products (product_id, order_id) VALUES (12,16);
INSERT INTO geek_store.orders_products (product_id, order_id) VALUES (12,17);
INSERT INTO geek_store.orders_products (product_id, order_id) VALUES (11,18);
INSERT INTO geek_store.orders_products (product_id, order_id) VALUES (14,19);