CREATE TABLE geek_store.customers (
  id         SERIAL,
  first_name VARCHAR(45)  NOT NULL,
  last_name  VARCHAR(45)  NOT NULL,
  gender     VARCHAR(10)   NOT NULL,
  birth_date DATE         NOT NULL,
  telephone  VARCHAR(45)  NOT NULL,
  email      VARCHAR(45)  NOT NULL,
  password   VARCHAR(100) NOT NULL,
  PRIMARY KEY (id)
);

CREATE TABLE geek_store.credit_card (
  customer_id INT         NOT NULL,
  cc_type     VARCHAR(45) NOT NULL,
  cc_number   VARCHAR(45) NOT NULL,
  cc_cvv      VARCHAR(45) NOT NULL,
  FOREIGN KEY (customer_id) REFERENCES geek_store.customers (id) ON UPDATE CASCADE ON DELETE CASCADE
);


INSERT INTO geek_store.customers (first_name, last_name, gender, birth_date, telephone, email, password)
VALUES ('user', 'user', 'M', '1967-01-15', '(097)545-15-44', 'user@mail.com', '$2a$10$EblZqNptyYvcLm/VwDCVAuBjzZOI7khzdyGPBr08PpIi0na624b8.');
INSERT INTO geek_store.customers (first_name, last_name, gender, birth_date, telephone, email, password)
VALUES ('John', 'Petrucci', 'M', '1978-05-17', '(099)978-45-74', 'jpMail@jmail.com', '123456');
INSERT INTO geek_store.customers (first_name, last_name, gender, birth_date, telephone, email, password)
VALUES ('Jimmy', 'Page', 'M', '1958-08-15', '(099)345-45-74', 'jpageMail@jmail.com', '123456');
INSERT INTO geek_store.customers (first_name, last_name, gender, birth_date, telephone, email, password)
VALUES ('Ritchie', 'Blackmore', 'M', '1960-01-25', '(099)345-15-44', 'rblack@jmail.com', '123456');
INSERT INTO geek_store.customers (first_name, last_name, gender, birth_date, telephone, email, password)
VALUES ('Joe', 'Satriani', 'M', '1967-01-15', '(097)545-15-44', 'jsatriani@jmail.com', '123456');
INSERT INTO geek_store.customers (first_name, last_name, gender, birth_date, telephone, email, password)
VALUES ('Steve', 'Vai', 'M', '1977-01-15', '(067)645-15-74', 'stevevai@jmail.com', '123456');
INSERT INTO geek_store.customers (first_name, last_name, gender, birth_date, telephone, email, password)
VALUES ('James', 'Last', 'M', '1967-01-15', '(097)545-15-44', 'jsatriani@jmail.com', '123456');
INSERT INTO geek_store.customers (first_name, last_name, gender, birth_date, telephone, email, password)
VALUES ('John', 'Doe', 'M', '1967-01-15', '(097)545-15-44', 'jsatriani@jmail.com', '123456');
INSERT INTO geek_store.customers (first_name, last_name, gender, birth_date, telephone, email, password)
VALUES ('Peter', 'Pen', 'M', '1967-01-15', '(097)545-15-44', 'jsatriani@jmail.com', '123456');
INSERT INTO geek_store.customers (first_name, last_name, gender, birth_date, telephone, email, password)
VALUES ('Steven', 'Tyler', 'M', '1967-01-15', '(097)545-15-44', 'jsatriani@jmail.com', '123456');
INSERT INTO geek_store.customers (first_name, last_name, gender, birth_date, telephone, email, password)
VALUES ('Mike', 'Tyson', 'M', '1967-01-15', '(097)545-15-44', 'jsatriani@jmail.com', '123456');
INSERT INTO geek_store.customers (first_name, last_name, gender, birth_date, telephone, email, password)
VALUES ('Lennox', 'Lewis', 'M', '1967-01-15', '(097)545-15-44', 'jsatriani@jmail.com', '123456');
INSERT INTO geek_store.customers (first_name, last_name, gender, birth_date, telephone, email, password)
VALUES ('Barak', 'Obama', 'M', '1967-01-15', '(097)545-15-44', 'jsatriani@jmail.com', '123456');
INSERT INTO geek_store.customers (first_name, last_name, gender, birth_date, telephone, email, password)
VALUES ('Donald', 'Trump', 'M', '1967-01-15', '(097)545-15-44', 'jsatriani@jmail.com', '123456');
INSERT INTO geek_store.customers (first_name, last_name, gender, birth_date, telephone, email, password)
VALUES ('Bill', 'Klinton', 'M', '1967-01-15', '(097)545-15-44', 'jsatriani@jmail.com', '123456');
INSERT INTO geek_store.customers (first_name, last_name, gender, birth_date, telephone, email, password)
VALUES ('Joshua', 'Bloch', 'M', '1967-01-15', '(097)545-15-44', 'jsatriani@jmail.com', '123456');
INSERT INTO geek_store.customers (first_name, last_name, gender, birth_date, telephone, email, password)
VALUES ('Freddie', 'Mercury', 'M', '1967-01-15', '(097)545-15-44', 'jsatriani@jmail.com', '123456');
INSERT INTO geek_store.customers (first_name, last_name, gender, birth_date, telephone, email, password)
VALUES ('Chuck', 'Berry', 'M', '1967-01-15', '(097)545-15-44', 'jsatriani@jmail.com', '123456');
INSERT INTO geek_store.customers (first_name, last_name, gender, birth_date, telephone, email, password)
VALUES ('Eric', 'Clapton', 'M', '1967-01-15', '(097)545-15-44', 'jsatriani@jmail.com', '123456');
INSERT INTO geek_store.customers (first_name, last_name, gender, birth_date, telephone, email, password)
VALUES ('John', 'Bohnam', 'M', '1967-01-15', '(097)545-15-44', 'jsatriani@jmail.com', '123456');
INSERT INTO geek_store.customers (first_name, last_name, gender, birth_date, telephone, email, password)
VALUES ('Ian', 'Paice', 'M', '1967-01-15', '(097)545-15-44', 'jsatriani@jmail.com', '123456');
INSERT INTO geek_store.customers (first_name, last_name, gender, birth_date, telephone, email, password)
VALUES ('Ian', 'Gillan', 'M', '1967-01-15', '(097)545-15-44', 'jsatriani@jmail.com', '123456');
INSERT INTO geek_store.customers (first_name, last_name, gender, birth_date, telephone, email, password)
VALUES ('John', 'Lord', 'M', '1967-01-15', '(097)545-15-44', 'jsatriani@jmail.com', '123456');
INSERT INTO geek_store.customers (first_name, last_name, gender, birth_date, telephone, email, password)
VALUES ('Bi Bi', 'King', 'M', '1967-01-15', '(097)545-15-44', 'jsatriani@jmail.com', '123456');
INSERT INTO geek_store.customers (first_name, last_name, gender, birth_date, telephone, email, password)
VALUES ('Larry', 'Carlton', 'M', '1967-01-15', '(097)545-15-44', 'jsatriani@jmail.com', '123456');
INSERT INTO geek_store.customers (first_name, last_name, gender, birth_date, telephone, email, password)
VALUES ('Carlos', 'Santana', 'M', '1967-01-15', '(097)545-15-44', 'jsatriani@jmail.com', '123456');
INSERT INTO geek_store.customers (first_name, last_name, gender, birth_date, telephone, email, password)
VALUES ('Jeorge', 'Benson', 'M', '1967-01-15', '(097)545-15-44', 'jsatriani@jmail.com', '123456');
INSERT INTO geek_store.customers (first_name, last_name, gender, birth_date, telephone, email, password)
VALUES ('Yngwie', 'Malmsteen', 'M', '1967-01-15', '(097)545-15-44', 'jsatriani@jmail.com', '123456');
INSERT INTO geek_store.customers (first_name, last_name, gender, birth_date, telephone, email, password)
VALUES ('Jason', 'Becker', 'M', '1967-01-15', '(097)545-15-44', 'jsatriani@jmail.com', '123456');
INSERT INTO geek_store.customers (first_name, last_name, gender, birth_date, telephone, email, password)
VALUES ('Marty', 'Friedman', 'M', '1967-01-15', '(097)545-15-44', 'jsatriani@jmail.com', '123456');
INSERT INTO geek_store.customers (first_name, last_name, gender, birth_date, telephone, email, password)
VALUES ('Bill', 'Gates', 'M', '1967-01-15', '(097)545-15-44', 'jsatriani@jmail.com', '123456');
INSERT INTO geek_store.customers (first_name, last_name, gender, birth_date, telephone, email, password)
VALUES ('Mark', 'Zucerberg', 'M', '1967-01-15', '(097)545-15-44', 'jsatriani@jmail.com', '123456');
INSERT INTO geek_store.customers (first_name, last_name, gender, birth_date, telephone, email, password)
VALUES ('Ilon', 'Mask', 'M', '1967-01-15', '(097)545-15-44', 'jsatriani@jmail.com', '123456');
INSERT INTO geek_store.customers (first_name, last_name, gender, birth_date, telephone, email, password)
VALUES ('Tony', 'Stark', 'M', '1967-01-15', '(097)545-15-44', 'jsatriani@jmail.com', '123456');
INSERT INTO geek_store.customers (first_name, last_name, gender, birth_date, telephone, email, password)
VALUES ('Rob', 'Stark', 'M', '1967-01-15', '(097)545-15-44', 'jsatriani@jmail.com', '123456');
INSERT INTO geek_store.customers (first_name, last_name, gender, birth_date, telephone, email, password)
VALUES ('Arya', 'Stark', 'M', '1967-01-15', '(097)545-15-44', 'jsatriani@jmail.com', '123456');
INSERT INTO geek_store.customers (first_name, last_name, gender, birth_date, telephone, email, password)
VALUES ('Joe', 'Satriani', 'M', '1967-01-15', '(097)545-15-44', 'jsatriani@jmail.com', '123456');
INSERT INTO geek_store.customers (first_name, last_name, gender, birth_date, telephone, email, password)
VALUES ('Jeff', 'Beck', 'M', '1967-01-15', '(097)545-15-44', 'jsatriani@jmail.com', '123456');
INSERT INTO geek_store.customers (first_name, last_name, gender, birth_date, telephone, email, password)
VALUES ('Brandon', 'Stark', 'M', '1967-01-15', '(097)545-15-44', 'jsatriani@jmail.com', '123456');



INSERT INTO geek_store.credit_card(customer_id, cc_type, cc_number, cc_cvv) VALUES (1,'MASTERCARD', '------', '----');
INSERT INTO geek_store.credit_card(customer_id, cc_type, cc_number, cc_cvv) VALUES (2,'VISA', '----', '-----');
INSERT INTO geek_store.credit_card(customer_id, cc_type, cc_number, cc_cvv) VALUES (3,'MASTERCARD', '6547984563', '7845');
INSERT INTO geek_store.credit_card(customer_id, cc_type, cc_number, cc_cvv) VALUES (4,'VISA', '6547984563', '7845');
INSERT INTO geek_store.credit_card(customer_id, cc_type, cc_number, cc_cvv) VALUES (5,'MASTERCARD', '6547984563', '7845');
INSERT INTO geek_store.credit_card(customer_id, cc_type, cc_number, cc_cvv) VALUES (6,'VISA', '6547984563', '7845');
INSERT INTO geek_store.credit_card(customer_id, cc_type, cc_number, cc_cvv) VALUES (7,'MASTERCARD', '6547984563', '7845');
INSERT INTO geek_store.credit_card(customer_id, cc_type, cc_number, cc_cvv) VALUES (8,'VISA', '6547984563', '7845');
INSERT INTO geek_store.credit_card(customer_id, cc_type, cc_number, cc_cvv) VALUES (9,'MASTERCARD', '6547984563', '7845');
INSERT INTO geek_store.credit_card(customer_id, cc_type, cc_number, cc_cvv) VALUES (10,'VISA', '6547984563', '7845');
INSERT INTO geek_store.credit_card(customer_id, cc_type, cc_number, cc_cvv) VALUES (11,'MASTERCARD', '6547984563', '7845');
INSERT INTO geek_store.credit_card(customer_id, cc_type, cc_number, cc_cvv) VALUES (12,'VISA', '6547984563', '7845');
INSERT INTO geek_store.credit_card(customer_id, cc_type, cc_number, cc_cvv) VALUES (13,'MASTERCARD', '6547984563', '7845');
INSERT INTO geek_store.credit_card(customer_id, cc_type, cc_number, cc_cvv) VALUES (14,'VISA', '6547984563', '7845');
INSERT INTO geek_store.credit_card(customer_id, cc_type, cc_number, cc_cvv) VALUES (15,'MASTERCARD', '6547984563', '7845');
INSERT INTO geek_store.credit_card(customer_id, cc_type, cc_number, cc_cvv) VALUES (16,'VISA', '6547984563', '7845');
INSERT INTO geek_store.credit_card(customer_id, cc_type, cc_number, cc_cvv) VALUES (17,'MASTERCARD', '6547984563', '7845');
INSERT INTO geek_store.credit_card(customer_id, cc_type, cc_number, cc_cvv) VALUES (18,'VISA', '6547984563', '7845');
INSERT INTO geek_store.credit_card(customer_id, cc_type, cc_number, cc_cvv) VALUES (19,'MASTERCARD', '6547984563', '7845');
INSERT INTO geek_store.credit_card(customer_id, cc_type, cc_number, cc_cvv) VALUES (20,'VISA', '6547984563', '7845');
INSERT INTO geek_store.credit_card(customer_id, cc_type, cc_number, cc_cvv) VALUES (21,'MASTERCARD', '6547984563', '7845');
INSERT INTO geek_store.credit_card(customer_id, cc_type, cc_number, cc_cvv) VALUES (22,'VISA', '6547984563', '7845');
INSERT INTO geek_store.credit_card(customer_id, cc_type, cc_number, cc_cvv) VALUES (23,'MASTERCARD', '6547984563', '7845');
INSERT INTO geek_store.credit_card(customer_id, cc_type, cc_number, cc_cvv) VALUES (24,'VISA', '6547984563', '7845');
INSERT INTO geek_store.credit_card(customer_id, cc_type, cc_number, cc_cvv) VALUES (25,'MASTERCARD', '6547984563', '7845');
INSERT INTO geek_store.credit_card(customer_id, cc_type, cc_number, cc_cvv) VALUES (26,'VISA', '6547984563', '7845');
INSERT INTO geek_store.credit_card(customer_id, cc_type, cc_number, cc_cvv) VALUES (27,'MASTERCARD', '6547984563', '7845');
INSERT INTO geek_store.credit_card(customer_id, cc_type, cc_number, cc_cvv) VALUES (28,'VISA', '6547984563', '7845');
INSERT INTO geek_store.credit_card(customer_id, cc_type, cc_number, cc_cvv) VALUES (29,'MASTERCARD', '6547984563', '7845');
INSERT INTO geek_store.credit_card(customer_id, cc_type, cc_number, cc_cvv) VALUES (30,'VISA', '6547984563', '7845');
INSERT INTO geek_store.credit_card(customer_id, cc_type, cc_number, cc_cvv) VALUES (31,'MASTERCARD', '6547984563', '7845');
INSERT INTO geek_store.credit_card(customer_id, cc_type, cc_number, cc_cvv) VALUES (32,'VISA', '6547984563', '7845');
INSERT INTO geek_store.credit_card(customer_id, cc_type, cc_number, cc_cvv) VALUES (33,'MASTERCARD', '6547984563', '7845');
INSERT INTO geek_store.credit_card(customer_id, cc_type, cc_number, cc_cvv) VALUES (34,'VISA', '6547984563', '7845');
INSERT INTO geek_store.credit_card(customer_id, cc_type, cc_number, cc_cvv) VALUES (35,'MASTERCARD', '6547984563', '7845');
INSERT INTO geek_store.credit_card(customer_id, cc_type, cc_number, cc_cvv) VALUES (36,'VISA', '6547984563', '7845');
INSERT INTO geek_store.credit_card(customer_id, cc_type, cc_number, cc_cvv) VALUES (37,'MASTERCARD', '6547984563', '7845');
INSERT INTO geek_store.credit_card(customer_id, cc_type, cc_number, cc_cvv) VALUES (38,'VISA', '6547984563', '7845');
INSERT INTO geek_store.credit_card(customer_id, cc_type, cc_number, cc_cvv) VALUES (39,'MASTERCARD', '6547984563', '7845');


