CREATE TABLE geek_store.product_metric (
  id         SERIAL,
  product_id INT NOT NULL,
  cutomer_id INT NOT NULL,
  view_date DATE NOT NULL ,
  FOREIGN KEY (cutomer_id) REFERENCES geek_store.customers (id),
  PRIMARY KEY (id)
);


INSERT INTO geek_store.product_metric(product_id, cutomer_id, view_date) VALUES (1,1,'2016-01-01');
INSERT INTO geek_store.product_metric(product_id, cutomer_id, view_date) VALUES (2,2,'2016-01-01');
INSERT INTO geek_store.product_metric(product_id, cutomer_id, view_date) VALUES (3,3,'2016-02-01');
INSERT INTO geek_store.product_metric(product_id, cutomer_id, view_date) VALUES (4,4,'2016-02-01');
INSERT INTO geek_store.product_metric(product_id, cutomer_id, view_date) VALUES (5,5,'2016-03-01');
INSERT INTO geek_store.product_metric(product_id, cutomer_id, view_date) VALUES (6,6,'2016-03-01');
INSERT INTO geek_store.product_metric(product_id, cutomer_id, view_date) VALUES (7,7,'2016-04-01');
INSERT INTO geek_store.product_metric(product_id, cutomer_id, view_date) VALUES (8,8,'2016-04-01');
INSERT INTO geek_store.product_metric(product_id, cutomer_id, view_date) VALUES (9,9,'2016-05-01');
INSERT INTO geek_store.product_metric(product_id, cutomer_id, view_date) VALUES (10,10,'2016-05-01');
INSERT INTO geek_store.product_metric(product_id, cutomer_id, view_date) VALUES (11,11,'2016-06-01');
INSERT INTO geek_store.product_metric(product_id, cutomer_id, view_date) VALUES (12,12,'2016-06-01');
INSERT INTO geek_store.product_metric(product_id, cutomer_id, view_date) VALUES (13,13,'2016-07-01');
INSERT INTO geek_store.product_metric(product_id, cutomer_id, view_date) VALUES (13,14,'2016-07-01');
INSERT INTO geek_store.product_metric(product_id, cutomer_id, view_date) VALUES (14,14,'2016-07-01');
INSERT INTO geek_store.product_metric(product_id, cutomer_id, view_date) VALUES (15,15,'2016-08-01');
INSERT INTO geek_store.product_metric(product_id, cutomer_id, view_date) VALUES (16,16,'2016-08-01');
INSERT INTO geek_store.product_metric(product_id, cutomer_id, view_date) VALUES (17,17,'2016-09-01');
INSERT INTO geek_store.product_metric(product_id, cutomer_id, view_date) VALUES (18,18,'2016-09-01');
INSERT INTO geek_store.product_metric(product_id, cutomer_id, view_date) VALUES (19,19,'2016-10-01');
INSERT INTO geek_store.product_metric(product_id, cutomer_id, view_date) VALUES (20,20,'2016-10-01');
INSERT INTO geek_store.product_metric(product_id, cutomer_id, view_date) VALUES (1,21,'2016-11-01');
INSERT INTO geek_store.product_metric(product_id, cutomer_id, view_date) VALUES (2,22,'2016-11-01');
INSERT INTO geek_store.product_metric(product_id, cutomer_id, view_date) VALUES (3,23,'2016-12-01');
INSERT INTO geek_store.product_metric(product_id, cutomer_id, view_date) VALUES (4,24,'2016-12-01');