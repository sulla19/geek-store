function paginCarousel(page, perPage, pageCount) {

    jQuery(document).ready(function ($) {

        var start = startIndex(page);
        var end = endIndex(start, pageCount);

        function startIndex(page) {
            var condition = (page - startIndex) >= (7 / 2 + 7 % 2);

            if (condition) {
                var checkToFirstPage = page - 7 / 2;
                if (checkToFirstPage < 1) {
                    start = 1;
                } else {
                    start = checkToFirstPage;
                }
            } else {
                start = 1;
            }
            return start;
        }

        function endIndex(startIndex, pageCount) {

            if ((startIndex + 6) > pageCount) {
                end = pageCount;
                if (pageCount > 7) {
                    start = pageCount - 6;
                } else {
                    start = 1;
                }
            } else {
                end = startIndex + 6;
            }
            return end;
        }

        var items = "";
        var path = getIdFromPath();

        for (var i = start; i <= end; i++) {
            items += '<a class="btn btn-default" href="/' + path + '?pageId=' + i + '&perPage=' + perPage + '">' + i + '</a>';
        }
        $('#pagin_carousel').html(items);
    })
}

function getIdFromPath() {
    var url = window.location.pathname;
    var url_array = url.split('/');
    return url_array[1];
}

