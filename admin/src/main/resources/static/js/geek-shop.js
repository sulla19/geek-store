// -------------------------Autocomplete--------------------------------
jQuery(document).ready(function ($) {

    var engine = new Bloodhound({
        remote: {
            url: '/search?q=%QUERY%',
            wildcard: '%QUERY%'
        },
        datumTokenizer: Bloodhound.tokenizers.whitespace('q'),
        queryTokenizer: Bloodhound.tokenizers.whitespace
    });

    $(".search-input").typeahead({
        hint: true,
        highlight: true,
        minLength: 1
    }, {
        limit: Infinity,
        readonly : true,
        source: engine.ttAdapter(),

        name: 'usersList',

        templates: {
            empty: [
                '<div class="list-group search-results-dropdown"><div class="list-group-item">Nothing found.</div></div>'
            ],
            header: [
                '<div class="list-group search-results-dropdown">'

            ],
            suggestion: function (data) {
                console.log(JSON.stringify(data));
                return '<a href=/customers/' + data.id + '/edit' + ' class="list-group-item">' + data.id + '. ' + data.firstName + ' ' + data.lastName + '  '
                    + data.telephone + '</a>'
            }
        }
    });

});

 // -------------------Table row as href----------------
jQuery(document).ready(function($) {
    $(".clickable-row").click(function() {
        window.location = $(this).attr("href");
    });
});


$(document).ready(function() {

    var showText='Show';
    var hideText='Hide';

    var is_visible = false;

    $('.toggle').prev().append(' <a href="#" class="toggleLink">'+hideText+'</a>');

    $('.toggle').show();

    $('a.toggleLink').click(function() {

        is_visible = !is_visible;

        if ($(this).text()==showText) {
            $(this).text(hideText);
            $(this).parent().next('.toggle').slideDown('slow');
        }
        else {
            $(this).text(showText);
            $(this).parent().next('.toggle').slideUp('slow');
        }
        return false;
    });
});
