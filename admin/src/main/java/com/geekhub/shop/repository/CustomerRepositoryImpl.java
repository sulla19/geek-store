package com.geekhub.shop.repository;

import com.geekhub.core.model.customer.Customer;
import com.geekhub.core.model.customer.CustomerMetrics;
import com.geekhub.core.model.order.payment.CreditCard;
import com.geekhub.core.repository.mapsqlparametersources.CreditCardParameterSource;
import com.geekhub.core.repository.mapsqlparametersources.CustomerParameterSource;
import com.geekhub.core.repository.rowmappers.CustomerMetricsRowMapper;
import com.geekhub.core.repository.rowmappers.CustomerRowMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.core.simple.SimpleJdbcInsert;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import javax.sql.DataSource;
import java.util.List;

@Repository
public class CustomerRepositoryImpl implements CustomerRepository {

    private JdbcTemplate jdbcTemplate;
    private SimpleJdbcInsert jdbcInsertCustomer;
    private SimpleJdbcInsert jdbcInsertCreditCard;
    private NamedParameterJdbcTemplate namedJdbcTemplate;

    @Autowired
    public CustomerRepositoryImpl(DataSource dataSource) {
        namedJdbcTemplate = new NamedParameterJdbcTemplate(dataSource);
        jdbcTemplate = new JdbcTemplate(dataSource);
        jdbcInsertCustomer = new SimpleJdbcInsert(dataSource)
                .withTableName("geek_store.customers")
                .usingColumns("first_name", "last_name", "gender", "birth_date", "telephone", "email", "password")
                .usingGeneratedKeyColumns("id");
        jdbcInsertCreditCard = new SimpleJdbcInsert(dataSource)
                .withTableName("geek_store.credit_card")
                .usingColumns("customer_id", "cc_type", "cc_number", "cc_cvv");
    }

    @Override
    public List<Customer> findAll() {

        String sql = "SELECT " +
                "  id, first_name, last_name, gender, birth_date, telephone, email, password " +
                "FROM geek_store.customers";

        return namedJdbcTemplate.query(sql, new CustomerRowMapper());
    }

    @Override
    public Customer find(int id) {

        String sql = "SELECT " +
                "  id, first_name, last_name, gender, birth_date, telephone, email, password, " +
                "  cc_type, cc_number, cc_cvv " +
                "FROM geek_store.customers " +
                "INNER JOIN geek_store.credit_card " +
                "  ON geek_store.customers.id = geek_store.credit_card.customer_id " +
                "WHERE geek_store.customers.id = ?";

        return jdbcTemplate.queryForObject(sql, new CustomerRowMapper(), id);
    }

    @Override
    @Transactional
    public void delete(int id) {
        jdbcTemplate.update("DELETE FROM geek_store.customers WHERE id=" + id);
    }

    @Override
    @Transactional
    public void update(int id, Customer customer) {

        String sql = "UPDATE geek_store.customers " +
                "SET first_name=:firstName, last_name=:lastName, gender=:gender, " +
                "birth_date=:birthDate, telephone=:telephone, email=:email, password=:password " +
                "WHERE id=" + id;

        String sql1 = "UPDATE geek_store.credit_card " +
                "SET cc_type=:cc_type, cc_number=:cc_number, cc_cvv=:cc_cvv " +
                "WHERE customer_id=" + id;

        namedJdbcTemplate.update(sql, new CustomerParameterSource().getParameterSource(customer));
        namedJdbcTemplate.update(sql1, new CreditCardParameterSource().getParameterSource(customer.getCreditCard()));
    }

    @Override
    @Transactional
    public Integer create(Customer customer) {
        Number key = jdbcInsertCustomer.executeAndReturnKey(new CustomerParameterSource().getParameterSource(customer));
        customer.setId(key.intValue());
        CreditCard blankCreditCard = CreditCard.getBlankCreditCard(key.intValue());
        jdbcInsertCreditCard.execute(new CreditCardParameterSource().getParameterSource(blankCreditCard));

        return key.intValue();
    }

    @Override
    public CustomerMetrics getCustomerMetrics(Integer id) {

        String sql = "WITH temp AS (\n" +
                "    SELECT COUNT(product_id) AS prod_num \n" +
                "FROM geek_store.orders\n" +
                "INNER JOIN geek_store.orders_products\n" +
                "  ON geek_store.orders.id = geek_store.orders_products.order_id\n" +
                "WHERE customer_id = ?)\n" +
                "SELECT \n" +
                "  temp.prod_num, \n" +
                "  (SELECT COUNT(orders.id) FROM geek_store.orders WHERE customer_id = ?) AS orders_num,\n" +
                "  (SELECT SUM(orders.total_price) FROM geek_store.orders WHERE customer_id = ?) AS total_sum\n" +
                "FROM temp";

        return jdbcTemplate.queryForObject(sql, new CustomerMetricsRowMapper(), id, id, id);
    }
}
