package com.geekhub.shop.repository;

import com.geekhub.core.model.customer.Customer;
import com.geekhub.core.model.customer.CustomerMetrics;

public interface CustomerRepository extends CrudRepository<Customer> {

    CustomerMetrics getCustomerMetrics(Integer id);
}
