package com.geekhub.shop.repository;

import com.geekhub.core.model.catalog.category.Category;
import com.geekhub.core.model.catalog.category.CategoryDescription;
import com.geekhub.core.model.catalog.category.CategoryDto;
import com.geekhub.core.model.common.Description;
import com.geekhub.core.repository.mapsqlparametersources.CategoryDescriptionParameterSource;
import com.geekhub.core.repository.mapsqlparametersources.CategoryParameterSource;
import com.geekhub.core.repository.rowmappers.CategoryDtoRowMapper;
import com.geekhub.core.repository.rowmappers.CategoryRowMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.core.simple.SimpleJdbcInsert;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import javax.sql.DataSource;
import java.util.List;

@Repository
public class CategoryRepositoryImpl implements CrudRepository<Category> {

    private JdbcTemplate jdbcTemplate;
    private SimpleJdbcInsert jdbcInsertEntry;
    private NamedParameterJdbcTemplate namedJdbcTemplate;

    @Autowired
    public CategoryRepositoryImpl(DataSource dataSource) {
        jdbcTemplate = new JdbcTemplate(dataSource);
        namedJdbcTemplate = new NamedParameterJdbcTemplate(dataSource);
        jdbcInsertEntry = new SimpleJdbcInsert(dataSource)
                .withTableName("postgres.geek_store.category")
                .usingColumns("img_url", "parent_id")
                .usingGeneratedKeyColumns("id");
    }

    @Override
    public List<CategoryDto> findAll() {

        String sql = "WITH temp AS ( " +
                "    SELECT " +
                "      geek_store.category.id                 AS curr_id, " +
                "      geek_store.category.parent_id          AS parent_id, " +
                "      geek_store.category_descriptions.title AS current_title " +
                "    FROM geek_store.category " +
                "      INNER JOIN geek_store.category_descriptions " +
                "        ON geek_store.category.id = geek_store.category_descriptions.category_id " +
                ") SELECT " +
                "    temp.curr_id, " +
                "    temp.current_title, " +
                "    geek_store.category_descriptions.title AS parent_title " +
                "  FROM temp " +
                "    INNER JOIN geek_store.category_descriptions " +
                "      ON temp.parent_id = geek_store.category_descriptions.category_id " +
                "  ORDER BY curr_id";

        return jdbcTemplate.query(sql, new CategoryDtoRowMapper());
    }

    @Override
    public Category find(int id) {

        String sql = "WITH temp AS ( " +
                "    SELECT " +
                "      geek_store.category.id                       AS cat_id, " +
                "      geek_store.category.img_url                  AS curr_img_url, " +
                "      geek_store.category.parent_id                AS parent_id, " +
                "      geek_store.category_descriptions.title       AS current_title, " +
                "      geek_store.category_descriptions.description AS current_description " +
                "    FROM geek_store.category " +
                "      INNER JOIN geek_store.category_descriptions " +
                "        ON geek_store.category.id = geek_store.category_descriptions.category_id " +
                ") SELECT " +
                "    temp.cat_id, " +
                "    temp.curr_img_url, " +
                "    temp.parent_id, " +
                "    temp.current_title, " +
                "    temp.current_description, " +
                "    geek_store.category_descriptions.title AS parent_title " +
                "  FROM temp " +
                "    INNER JOIN geek_store.category_descriptions " +
                "      ON temp.parent_id = geek_store.category_descriptions.category_id " +
                " WHERE cat_id = ?";

        return jdbcTemplate.queryForObject(sql, new CategoryRowMapper(), id);
    }

    @Override
    @Transactional
    public Integer create(Category category) {

        Number key = jdbcInsertEntry.executeAndReturnKey(new CategoryParameterSource().getParameterSource(category));
        category.setId(key.intValue());

        String sql = "INSERT " +
                "INTO postgres.geek_store.category_descriptions (category_id, title, description ) " +
                "VALUES (" + key.intValue() + ", :title, :description) ";

        Description description = category.getDescription();
        namedJdbcTemplate.update(sql, new CategoryDescriptionParameterSource().getParameterSource(description));
        return key.intValue();
    }

    @Override
    @Transactional
    public void update(int id, Category category) {

        String sql = "UPDATE geek_store.category " +
                "SET " +
                "  img_url   = :img_url, " +
                "  parent_id = :parent_id " +
                "WHERE id = " + id;

        String sql1 = "UPDATE geek_store.category_descriptions " +
                "SET " +
                "  description = :description, " +
                "  title       = :title " +
                "WHERE category_id =" + id;

        namedJdbcTemplate.update(sql, new CategoryParameterSource().getParameterSource(category));
        CategoryDescription description = (CategoryDescription) category.getDescription();
        namedJdbcTemplate.update(sql1, new CategoryDescriptionParameterSource().getParameterSource(description));
    }

    @Override
    @Transactional
    public void delete(int id) {
        jdbcTemplate.update("DELETE FROM postgres.geek_store.category WHERE id=" + id);
    }

    public List<Category> getSubcategories(Integer id) {

        String sql = "WITH temp AS (" +
                "    SELECT " +
                "      geek_store.category.id                       AS cat_id, " +
                "      geek_store.category.img_url                  AS curr_img_url, " +
                "      geek_store.category.parent_id                AS parent_id, " +
                "      geek_store.category_descriptions.title       AS current_title, " +
                "      geek_store.category_descriptions.description AS current_description " +
                "    FROM geek_store.category " +
                "      INNER JOIN geek_store.category_descriptions " +
                "        ON geek_store.category.id = geek_store.category_descriptions.category_id " +
                ") SELECT " +
                "    temp.cat_id, " +
                "    temp.curr_img_url, " +
                "    temp.parent_id, " +
                "    temp.current_title, " +
                "    temp.current_description, " +
                "    geek_store.category_descriptions.title AS parent_title " +
                "  FROM temp " +
                "    INNER JOIN geek_store.category_descriptions " +
                "      ON temp.parent_id = geek_store.category_descriptions.category_id " +
                "  WHERE temp.parent_id = ? " +
                "  ORDER BY cat_id;";

        return jdbcTemplate.query(sql, new CategoryRowMapper(), id);
    }
}
