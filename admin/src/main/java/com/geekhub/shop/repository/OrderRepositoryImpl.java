package com.geekhub.shop.repository;

import com.geekhub.core.model.order.Order;
import com.geekhub.core.model.order.delivery.Delivery;
import com.geekhub.core.repository.mapsqlparametersources.DeliveryParameterSource;
import com.geekhub.core.repository.mapsqlparametersources.OrderParameterSource;
import com.geekhub.core.repository.rowmappers.OrderRowMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.core.simple.SimpleJdbcInsert;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import javax.sql.DataSource;
import java.util.List;

@Repository
public class OrderRepositoryImpl implements CrudRepository<Order> {

    private SimpleJdbcInsert jdbcInsertOrder;
    private SimpleJdbcInsert jdbcInsertDelivery;
    private NamedParameterJdbcTemplate namedJdbcTemplate;
    private JdbcTemplate jdbcTemplate;

    @Autowired
    public OrderRepositoryImpl(DataSource dataSource) {
        jdbcTemplate = new JdbcTemplate(dataSource);
        namedJdbcTemplate = new NamedParameterJdbcTemplate(dataSource);
        jdbcInsertOrder = new SimpleJdbcInsert(dataSource)
                .withTableName("orders")
                .usingGeneratedKeyColumns("id");
        jdbcInsertDelivery = new SimpleJdbcInsert(dataSource)
                .withTableName("delivery");
    }

    @Override
    public List<Order> findAll() {

        String sql = "SELECT" +
                "  orders.id,  last_modified,  customer_id,  total_price,  shipping_type,  payment_type,  contact_telephone, " +
                "  delivery.id, delivery.company, country, address, city, postal_code, telephone " +
                "FROM geek_store.orders " +
                "INNER JOIN geek_store.delivery " +
                "  ON geek_store.orders.id = geek_store.delivery.order_id";

        return jdbcTemplate.query(sql, new OrderRowMapper());
    }

    @Override
    public Order find(int id) {

        String orderSql = "SELECT" +
                "  orders.id,  last_modified,  customer_id,  total_price,  shipping_type,  payment_type,  contact_telephone, " +
                "  delivery.id, delivery.company, country, address, city, postal_code, telephone " +
                "FROM geek_store.orders " +
                "INNER JOIN geek_store.delivery " +
                "  ON geek_store.orders.id = geek_store.delivery.order_id" +
                " WHERE order_id = ?";

        return jdbcTemplate.queryForObject(orderSql, new OrderRowMapper(), id);
    }

    public List<Order> findByCustomerId(int id) {

        String sql = "SELECT " +
                "id, last_modified, customer_id, total_price, shipping_type, payment_type, contact_telephone" +
                " FROM geek_store.orders " +
                "WHERE customer_id= ?";

        return jdbcTemplate.query(sql, new OrderRowMapper(), id);
    }

    @Override
    @Transactional
    public void delete(int id) {
        jdbcTemplate.update("DELETE FROM geek_store.orders WHERE id=" + id);
    }

    @Override
    @Transactional
    public void update(int id, Order order) {

        String sql = "UPDATE geek_store.orders " +
                "SET last_modified=:last_modified, customer_id=:customer_id, total_price=:total_price, " +
                "payment_type=:payment_type, shipping_type=:shipping_type," +
                " contact_telephone=:contact_telephone " +
                "WHERE id=" + id;

        namedJdbcTemplate.update(sql, new OrderParameterSource().getParameterSource(order));
    }

    @Override
    @Transactional
    public Integer create(Order order) {
        Number key = jdbcInsertOrder.executeAndReturnKey(new OrderParameterSource().getParameterSource(order));
        order.setId(key.intValue());
        Delivery delivery = order.getDelivery();
        delivery.setOrderId(key.intValue());
        jdbcInsertDelivery.execute(new DeliveryParameterSource().getParameterSource(delivery));
        return key.intValue();
    }
}
