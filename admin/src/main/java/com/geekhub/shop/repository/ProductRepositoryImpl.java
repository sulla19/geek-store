package com.geekhub.shop.repository;

import com.geekhub.core.model.catalog.product.Product;
import com.geekhub.core.model.catalog.product.ProductDescription;
import com.geekhub.core.model.catalog.product.ProductDto;
import com.geekhub.core.model.catalog.product.review.Review;
import com.geekhub.core.repository.mapsqlparametersources.ProductDescriptionParameterSource;
import com.geekhub.core.repository.mapsqlparametersources.ProductParameterSource;
import com.geekhub.core.repository.mapsqlparametersources.ReviewParameterSource;
import com.geekhub.core.repository.rowmappers.ProductDtoRowMapper;
import com.geekhub.core.repository.rowmappers.ProductDtoWithReviewsRowMapper;
import com.geekhub.core.repository.rowmappers.ProductRowMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.core.simple.SimpleJdbcInsert;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import javax.sql.DataSource;
import java.util.List;

@Repository
public class ProductRepositoryImpl implements ProductRepository {

    private SimpleJdbcInsert jdbcInsertProduct;
    private SimpleJdbcInsert jdbcInsertProductDescription;
    private NamedParameterJdbcTemplate namedJdbcTemplate;
    private JdbcTemplate jdbcTemplate;

    @Autowired
    public ProductRepositoryImpl(DataSource dataSource) {
        jdbcTemplate = new JdbcTemplate(dataSource);
        namedJdbcTemplate = new NamedParameterJdbcTemplate(dataSource);
        jdbcInsertProduct = new SimpleJdbcInsert(dataSource)
                .withTableName("geek_store.products")
                .usingColumns("type", "category_id", "length", "weight", "width", "height", "price")
                .usingGeneratedKeyColumns("id");
        jdbcInsertProductDescription = new SimpleJdbcInsert(dataSource)
                .withTableName("geek_store.product_descriptions")
                .usingColumns("product_id", "title", "author", "isbn", "description");
    }

    @Override
    public List<Product> findAll() {

        String sql = "SELECT " +
                "  products.id AS prod_id,  type , weight,  height,  width,  length, price, " +
                "  product_image.src_image, " +
                "  product_descriptions.product_id, product_descriptions.title AS product_title, author, isbn, product_descriptions.description, " +
                "  category_descriptions.title AS cat_title " +
                "FROM geek_store.products " +
                "INNER JOIN geek_store.product_image " +
                "  ON geek_store.products.id = geek_store.product_image.product_id " +
                "INNER JOIN geek_store.product_descriptions " +
                "  ON geek_store.products.id = geek_store.product_descriptions.product_id " +
                "INNER JOIN geek_store.category " +
                "  ON geek_store.products.category_id = geek_store.category.id " +
                "LEFT JOIN geek_store.category_descriptions " +
                "  ON geek_store.category.id = geek_store.category_descriptions.category_id ";

        return jdbcTemplate.query(sql, new ProductRowMapper());
    }

    @Override
    public ProductDto find(int id) {

        String sql = "WITH temp AS (SELECT " +
                "  products.id AS prod_id,  type , weight,  height,  width,  length, price, " +
                "  product_image.src_image, " +
                "  product_descriptions.title AS prod_title, author, isbn, product_descriptions.description, " +
                "  category.id AS cat_id, category_descriptions.title AS cat_title " +
                "FROM geek_store.products " +
                "INNER JOIN geek_store.product_image " +
                "  ON geek_store.products.id = geek_store.product_image.product_id " +
                "INNER JOIN geek_store.product_descriptions " +
                "  ON geek_store.products.id = geek_store.product_descriptions.product_id " +
                "INNER JOIN geek_store.category " +
                "  ON geek_store.products.category_id = geek_store.category.id " +
                "LEFT JOIN geek_store.category_descriptions " +
                "  ON geek_store.category.id = geek_store.category_descriptions.category_id " +
                "WHERE products.id = ?)" +
                "SELECT " +
                "  DISTINCT (prod_id),  type , weight,  height,  width,  length, price, " +
                "  src_image, " +
                "  prod_title, author, description, isbn," +
                "  cat_id, cat_title, " +
                "  reviews.id, reviews.date, rating, advantages, disadvantages, comment, customer_id " +
                "FROM temp " +
                " LEFT JOIN  geek_store.reviews " +
                "  ON prod_id = geek_store.reviews.product_id";

        return jdbcTemplate.queryForObject(sql, new ProductDtoWithReviewsRowMapper(), id);
    }

    @Override
    @Transactional
    public void delete(int id) {

        jdbcTemplate.update("DELETE FROM geek_store.products WHERE id=" + id);
    }

    @Override
    @Transactional
    public void update(int id, Product product) {

        String productUpdate = "UPDATE geek_store.products " +
                "SET type=:type, category_id=:category_id, length=:length," +
                "height=:height, weight=:weight, width=:width, price=:price " +
                "WHERE id=" + id;

        namedJdbcTemplate.update(productUpdate, new ProductParameterSource().getParameterSource(product));

        String descriptionUpdate = "UPDATE geek_store.product_descriptions " +
                "SET title=:title, author=:author, isbn=:isbn," +
                "description=:description " +
                "WHERE product_id=" + id;

        namedJdbcTemplate.update(descriptionUpdate, new ProductDescriptionParameterSource().getParameterSource((ProductDescription) product.getDescription()));


        List<Review> reviews = product.getReviews();
        String reviewUpdate = "UPDATE geek_store.reviews " +
                "SET product_id=" + id + ", date=:date, rating=:rating, advantages=:advantages" +
                ", disadvantages=:disadvantages, customer_id=:customer_id" +
                " WHERE product_id=" + id;

        for (Review review : reviews) {
            namedJdbcTemplate.update(reviewUpdate, new ReviewParameterSource().getParameterSource(review));
        }

        List<String> srcImages = product.getProductMedia().getSrcImages();

        for (String src : srcImages) {
            jdbcTemplate.update("UPDATE geek_store.product_image SET product_id = ?, src_image = ? WHERE product_id = ?",
                    id, src, id);
        }
    }

    @Override
    @Transactional
    public Integer create(Product product) {
        Number key = jdbcInsertProduct.executeAndReturnKey(new ProductParameterSource().getParameterSource(product));
        int productId = key.intValue();
        product.setId(productId);

        ProductDescription description = (ProductDescription) product.getDescription();
        description.setProductId(productId);
        jdbcInsertProductDescription.execute(new ProductDescriptionParameterSource().getParameterSource(description));

        List<String> srcImages = product.getProductMedia().getSrcImages();
        for (String srcImage : srcImages) {
            jdbcTemplate.update("INSERT INTO geek_store.product_image (product_id, src_image) " +
                    "VALUES ('" + productId + "', '" + srcImage + "')");

        }
        return key.intValue();
    }

    @Override
    public List<ProductDto> getListProducts(Integer orderId) {

        String sql = "SELECT " +
                "  orders_products.product_id, price, " +
                "  title, author, isbn, " +
                "  src_image " +
                "FROM geek_store.orders_products " +
                "  INNER JOIN geek_store.products " +
                "  ON geek_store.orders_products.product_id = geek_store.products.id " +
                "  INNER JOIN geek_store.product_descriptions " +
                "  ON geek_store.orders_products.product_id = geek_store.product_descriptions.product_id " +
                "  INNER JOIN geek_store.product_image " +
                "  ON geek_store.orders_products.product_id = geek_store.product_image.product_id " +
                "WHERE order_id = ?";

        return jdbcTemplate.query(sql, new ProductDtoRowMapper(), orderId);
    }
}
