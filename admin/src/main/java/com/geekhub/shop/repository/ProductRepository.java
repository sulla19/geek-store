package com.geekhub.shop.repository;

import com.geekhub.core.model.catalog.product.Product;
import com.geekhub.core.model.catalog.product.ProductDto;

import java.util.List;

public interface ProductRepository extends CrudRepository<Product> {

    List<ProductDto> getListProducts(Integer orderId);
}
