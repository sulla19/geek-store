package com.geekhub.shop.repository;

import com.geekhub.core.model.generic.BaseEntity;

import java.util.List;

public interface CrudRepository<T extends BaseEntity> {

    List<? extends T> findAll();

    T find(int id);

    void delete(int id);

    void update(int id, T t);

    Integer create(T t);
}
