package com.geekhub.shop.statistic.repository;

import com.geekhub.core.model.common.PriceRange;
import com.geekhub.core.model.common.ReportPeriod;
import com.geekhub.shop.statistic.model.ItemsCounter;
import com.geekhub.shop.statistic.model.ItemsCounterRowMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import javax.sql.DataSource;
import java.util.List;

@Repository
public class StatisticRepositoryImpl implements StatisticRepository {
    private JdbcTemplate jdbcTemplate;

    @Autowired
    public StatisticRepositoryImpl(DataSource dataSource) {
        this.jdbcTemplate = new JdbcTemplate(dataSource);
    }

    @Override
    public List<ItemsCounter> getBestSellingProductByPeriod(ReportPeriod fromDate, Integer limit) {
        String byPeriod = "WITH base AS (" +
                "SELECT " +
                "product_id, " +
                "COUNT(product_id) AS count " +
                "FROM geek_store.orders_products " +
                "INNER JOIN geek_store.orders " +
                "ON geek_store.orders.id = geek_store.orders_products.order_id " +
                "WHERE geek_store.orders.last_modified :: DATE > ? " +
                "GROUP BY product_id " +
                "ORDER BY count DESC " +
                "LIMIT ?) " +
                "SELECT " +
                "product_id, " +
                "count, " +
                "(count / (SELECT COUNT(*) " +
                "FROM geek_store.orders_products) :: DECIMAL) * 100 AS percent_total " +
                "FROM base " +
                "ORDER BY count DESC";

        return jdbcTemplate.query(byPeriod, new ItemsCounterRowMapper(), fromDate, limit);
    }

    @Override
    public List<ItemsCounter> getMostViewedProductByPeriodInCategory(Integer categoryId, ReportPeriod fromDate, Integer limit, PriceRange priceRange) {

        String sql = "WITH RECURSIVE r AS ( " +
                "                 SELECT geek_store.category.id AS cat_id " +
                "                 FROM geek_store.category " +
                "                 WHERE geek_store.category.id = ? " +
                "                 UNION ALL " +
                "                 SELECT geek_store.category.id " +
                "                 FROM geek_store.category " +
                "                  JOIN r " +
                "                  ON geek_store.category.parent_id = r.cat_id)" +
                "SELECT cat_id, temp.product_id, temp.count, temp.price, temp.percent_total FROM r " +
                "INNER JOIN ( " +
                "WITH base AS ( " +
                "                SELECT product_id, COUNT(product_id) AS count " +
                "                FROM geek_store.product_metric              " +
                "                WHERE geek_store.product_metric.view_date :: DATE > ? " +
                "                GROUP BY product_id " +
                "                ORDER BY count DESC " +
                "                LIMIT ?) " +
                "                SELECT product_id, count, price, category_id ," +
                "                (count / (SELECT COUNT(*) FROM geek_store.product_metric) :: DECIMAL) * 100 AS percent_total " +
                "                FROM base" +
                "                  INNER JOIN geek_store.products" +
                "                    ON base.product_id = geek_store.products.id           " +
                "                ) AS temp " +
                "  ON r.cat_id = temp.category_id " +
                "WHERE price > ? AND price < ? " +
                "ORDER BY count DESC;";

        return jdbcTemplate.query(sql, new ItemsCounterRowMapper(), categoryId, fromDate.getValue(), limit,
                priceRange.getMinPrice(), priceRange.getMaxPrice());
    }

    @Override
    public List<ItemsCounter> getOrdersByCities() {
        String sql = "WITH base AS (" +
                "SELECT city, COUNT(city) AS city_count " +
                "FROM geek_store.customer_address " +
                "INNER JOIN geek_store.orders " +
                "ON geek_store.customer_address.customer_id=geek_store.orders.customer_id " +
                "GROUP BY geek_store.customer_address.city) " +
                "SELECT " +
                "city, city_count, " +
                "(city_count / (SELECT COUNT(*) FROM base) :: DECIMAL) * 100 AS percent_total " +
                "FROM base " +
                "ORDER BY city_count DESC";

        return jdbcTemplate.query(sql, new ItemsCounterRowMapper());
    }
}
