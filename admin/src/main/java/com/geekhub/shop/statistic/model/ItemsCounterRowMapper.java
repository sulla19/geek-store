package com.geekhub.shop.statistic.model;

import org.springframework.jdbc.core.RowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;

public class ItemsCounterRowMapper implements RowMapper<ItemsCounter> {

    @Override
    public ItemsCounter mapRow(ResultSet rs, int rowNum) throws SQLException {
        ItemsCounter itemsCounter = new ItemsCounter();
        itemsCounter.setId(rs.getInt("product_id"));
        itemsCounter.setCount(rs.getInt("count"));
        itemsCounter.setPercentOfTotal(rs.getDouble("percent_total"));
        return itemsCounter;
    }
}
