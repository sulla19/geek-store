package com.geekhub.shop.statistic.repository;

import com.geekhub.core.model.common.PriceRange;
import com.geekhub.core.model.common.ReportPeriod;
import com.geekhub.shop.statistic.model.ItemsCounter;

import java.util.List;

public interface StatisticRepository {

    List<ItemsCounter> getBestSellingProductByPeriod(ReportPeriod fromDate, Integer limit);

    List<ItemsCounter> getMostViewedProductByPeriodInCategory(Integer categoryId, ReportPeriod fromDate, Integer limit, PriceRange priceRange);

    List<ItemsCounter> getOrdersByCities();
}
