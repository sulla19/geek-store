package com.geekhub.shop.statistic.model;

import lombok.Data;

@Data
public class ItemsCounter {

    private Integer id;
    private Integer count;
    private double percentOfTotal;
}
