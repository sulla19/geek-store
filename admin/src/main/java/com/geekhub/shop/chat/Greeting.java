package com.geekhub.shop.chat;

import lombok.Data;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

@Data
public class Greeting {

    private String time;
    private String username;
    private String content;

    public Greeting() {
    }

    public Greeting(String username, String content) {
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("HH:mm:ss ");
        this.time = LocalDateTime.now().format(formatter);
        this.username = username;
        this.content = content;
    }
}
