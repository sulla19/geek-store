package com.geekhub.shop.controller;

import com.geekhub.core.model.catalog.category.CategoryDto;
import com.geekhub.core.web.pagination.Page;
import com.geekhub.core.web.pagination.PageRequest;
import com.geekhub.core.web.pagination.PaginationUtils;
import com.geekhub.shop.repository.CategoryRepositoryImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.servlet.ModelAndView;

import java.util.List;

@Controller
public class CategoryController {

    private CategoryRepositoryImpl categoryRepositoryImpl;

    @Autowired
    public CategoryController(CategoryRepositoryImpl categoryRepositoryImpl) {
        this.categoryRepositoryImpl = categoryRepositoryImpl;
    }

    @GetMapping("/category")
    public String getAllCategories(Model model, PageRequest pageRequest) {
        List<CategoryDto> categories = categoryRepositoryImpl.findAll();
        Page page = PaginationUtils.getPage(categories, pageRequest);
        model.addAttribute("page", page);
        return "categories";
    }

    @GetMapping("/category/{id}/edit")
    public String editCategory(Model model, @PathVariable(required = false) Integer id) {
        model.addAttribute("category", categoryRepositoryImpl.find(id));
        model.addAttribute("subcategories", categoryRepositoryImpl.getSubcategories(id));
        return "editCategory";
    }

    @PostMapping("category/{id}")
    public String updateCategory(@PathVariable Integer id, CategoryDto categoryDto) {
        categoryRepositoryImpl.update(id, categoryDto.getCategory());
        return "redirect:/category";
    }

    @PostMapping("/category")
    public String createCategory(CategoryDto categoryDto) {
        categoryRepositoryImpl.create(categoryDto.getCategory());
        return "redirect:/category";
    }

    @GetMapping("category/new")
    public ModelAndView toCustomer(){
        return new ModelAndView("newCategory");
    }

    @GetMapping("/category/{id}/delete")
    public String deleteCategory(@PathVariable Integer id) {
        categoryRepositoryImpl.delete(id);
        return "redirect:/category";
    }
}
