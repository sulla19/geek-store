package com.geekhub.shop.controller;

import com.geekhub.core.model.generic.BaseEntity;
import com.geekhub.core.model.order.Order;
import com.geekhub.core.web.pagination.Page;
import com.geekhub.core.web.pagination.PageRequest;
import com.geekhub.core.web.pagination.PaginationUtils;
import com.geekhub.shop.repository.OrderRepositoryImpl;
import com.geekhub.shop.repository.ProductRepositoryImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;

import java.util.List;

@Controller
public class OrderController {

    private OrderRepositoryImpl orderRepositoryImpl;
    private ProductRepositoryImpl productRepository;

    @Autowired
    public OrderController(OrderRepositoryImpl orderRepositoryImpl, ProductRepositoryImpl productRepository) {
        this.orderRepositoryImpl = orderRepositoryImpl;
        this.productRepository = productRepository;
    }

    @GetMapping("/orders")
    public String getAllOrders(Model model, PageRequest pageRequest) {
        List<Order> orders = orderRepositoryImpl.findAll();
        Page page = getPage(orders, pageRequest);
        model.addAttribute("page", page);
        return "orders";
    }

    @GetMapping("/orders/{id}/edit")
    public String editOrder(Model model, @PathVariable(required = false) Integer id) {
        Order order = orderRepositoryImpl.find(id);
        model.addAttribute("order", order);
        model.addAttribute("products", productRepository.getListProducts(id));
        return "editOrder";
    }

    @PostMapping("/orders/{orderId}")
    public String updateOrder(Order order, @PathVariable Integer orderId) {
        orderRepositoryImpl.update(orderId, order);
        return "redirect:/orders";
    }

    private <T extends BaseEntity> Page getPage(List<T> list, PageRequest pageRequest) {
        return PaginationUtils.getPage(list, pageRequest);
    }
}
