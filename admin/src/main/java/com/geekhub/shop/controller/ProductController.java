package com.geekhub.shop.controller;

import com.geekhub.core.model.generic.BaseEntity;
import com.geekhub.core.model.catalog.product.Product;
import com.geekhub.core.model.catalog.product.ProductDto;
import com.geekhub.core.repository.dtomapper.DtoMapper;
import com.geekhub.core.web.pagination.Page;
import com.geekhub.core.web.pagination.PageRequest;
import com.geekhub.core.web.pagination.PaginationUtils;
import com.geekhub.shop.repository.ProductRepositoryImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.servlet.ModelAndView;

import java.util.List;

@Controller
public class ProductController {

    private ProductRepositoryImpl productRepository;

    @Autowired
    public ProductController(ProductRepositoryImpl productRepository) {
        this.productRepository = productRepository;
    }

    @GetMapping("/product")
    public String getAllProducts(Model model, PageRequest pageRequest) {
        List<Product> products = productRepository.findAll();
        Page page = getPage(products, pageRequest);
        model.addAttribute("page", page);
        return "products";
    }

    @GetMapping("/product/{id}/edit")
    public String editProduct(Model model, @PathVariable(required = false) Integer id) {
        model.addAttribute("product", productRepository.find(id));
        return "editProduct";
    }

    @PostMapping("product/{id}")
    public String updateProduct(@PathVariable Integer id, ProductDto productDto) {
        productRepository.update(id, DtoMapper.getProduct(productDto));
        return "redirect:/product";
    }

    @PostMapping("/product")
    public String createProduct(ProductDto productDto) {
        productRepository.create(DtoMapper.getProduct(productDto));
        return "redirect:/product";
    }

    @GetMapping("/product/new")
    public ModelAndView toCustomer(){
        return new ModelAndView("newProduct");
    }

    @GetMapping("/product/{id}/delete")
    public String deleteProduct(@PathVariable Integer id) {
        productRepository.delete(id);
        return "redirect:/product";
    }

    private <T extends BaseEntity> Page getPage(List<T> list, PageRequest pageRequest) {
        return PaginationUtils.getPage(list, pageRequest);
    }
}
