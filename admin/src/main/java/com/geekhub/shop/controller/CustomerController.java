package com.geekhub.shop.controller;

import com.geekhub.core.model.customer.Customer;
import com.geekhub.core.model.generic.BaseEntity;
import com.geekhub.core.model.order.payment.CreditCard;
import com.geekhub.core.web.pagination.Page;
import com.geekhub.core.web.pagination.PageRequest;
import com.geekhub.core.web.pagination.PaginationUtils;
import com.geekhub.shop.repository.CustomerRepositoryImpl;
import com.geekhub.shop.repository.OrderRepositoryImpl;
import com.geekhub.shop.search.SearchService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

import java.util.List;

@Controller
public class CustomerController {

    private CustomerRepositoryImpl customerRepository;
    private OrderRepositoryImpl orderRepositoryImpl;
    private SearchService searchService;

    @Autowired
    public CustomerController(CustomerRepositoryImpl customerRepository, OrderRepositoryImpl orderRepositoryImpl,
                              SearchService searchService) {
        this.customerRepository = customerRepository;
        this.orderRepositoryImpl = orderRepositoryImpl;
        this.searchService = searchService;
    }

    @GetMapping("/customers")
    public String getAllCustomers(Model model, PageRequest pageRequest) {
        List<Customer> customers = customerRepository.findAll();
        Page page = getPage(customers, pageRequest);
        model.addAttribute("page", page);
        return "customers";
    }

    @GetMapping("/customers/{id}/edit")
    public String editCustomer(Model model, @PathVariable(required = false) Integer id) {
        model.addAttribute("customer", customerRepository.find(id));
        model.addAttribute("orders", orderRepositoryImpl.findByCustomerId(id));
        model.addAttribute("metrics", customerRepository.getCustomerMetrics(id));
        return "editCustomer";
    }

    @PostMapping("customers/{id}")
    public String updateCustomer(@PathVariable Integer id, Customer customer, CreditCard creditCard) {
        customer.setCreditCard(creditCard);
        customerRepository.update(id, customer);
        return "redirect:/customers";
    }

    @PostMapping("/customers")
    public String createCustomer(Customer customer) {
        Integer id = customerRepository.create(customer);
        return "redirect:/customers/" + id + "/edit";
    }

    @GetMapping("customers/new")
    public ModelAndView toCustomer(){
        return new ModelAndView("newCustomer");
    }

    @GetMapping("/customers/{id}/delete")
    public String deleteCustomer(@PathVariable Integer id) {
        customerRepository.delete(id);
        return "redirect:/customers";
    }

    @GetMapping("/search")
    @ResponseBody
    public List<Customer> fulltextSearch(@RequestParam(name = "q") String find) {
        return searchService.searchCustomer(find);
    }

    private <T extends BaseEntity> Page getPage(List<T> list, PageRequest pageRequest) {
        return PaginationUtils.getPage(list, pageRequest);
    }
}
