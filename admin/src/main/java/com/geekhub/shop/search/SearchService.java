package com.geekhub.shop.search;

import com.geekhub.core.model.customer.Customer;
import com.geekhub.core.search.SearchRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class SearchService {

    private SearchRepository searchRepository;

    @Autowired
    public SearchService(SearchRepository searchRepository) {
        this.searchRepository = searchRepository;
    }

    public List<Customer> searchCustomer(String snip) {
        return searchRepository.searchCustomer(snip);
    }
}
