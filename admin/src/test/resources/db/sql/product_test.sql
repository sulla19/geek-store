CREATE TABLE products (
  id          INT           NOT NULL AUTO_INCREMENT,
  name        VARCHAR(100)  NOT NULL,
  category_id INT           NOT NULL,
  length      DOUBLE        NOT NULL,
  weight      DOUBLE        NOT NULL,
  height      DOUBLE        NOT NULL,
  width       DOUBLE        NOT NULL,
  price       DECIMAL(8, 2) NOT NULL,
  PRIMARY KEY (id),
  FOREIGN KEY (category_id) REFERENCES category (id)
  ON UPDATE CASCADE
  ON DELETE CASCADE
);

INSERT INTO products (name, category_id, length, weight, height, width, price)
VALUES ('product 1', 2, 120, 120, 120, 120, 800);
INSERT INTO products (name, category_id, length, weight, height, width, price)
VALUES ('product 2', 1, 250, 250, 300, 1202, 450);

CREATE TABLE reviews (
  id            INT         NOT NULL AUTO_INCREMENT,
  date          DATE        NOT NULL,
  rating        INT         NOT NULL,
  advantages    VARCHAR(45) NOT NULL,
  disadvantages VARCHAR(45) NOT NULL,
  customer_id   INT         NOT NULL,
  product_id    INT         NOT NULL,
  FOREIGN KEY (customer_id) REFERENCES customers (id)
  ON UPDATE CASCADE
  ON DELETE CASCADE,
  FOREIGN KEY (product_id) REFERENCES products (id)
  ON UPDATE CASCADE
  ON DELETE CASCADE,
  PRIMARY KEY (id)
);

INSERT INTO reviews (date, rating, advantages, disadvantages, customer_id, product_id)
VALUES ('1975-05-05', 5, 'super', 'none', 1, 1);
INSERT INTO reviews (date, rating, advantages, disadvantages, customer_id, product_id)
VALUES ('2000-05-05', 2, 'none', 'bad', 1, 2);

CREATE TABLE product_image (
  product_id INT NOT NULL,
  src_image  VARCHAR(1000),
  FOREIGN KEY (product_id) REFERENCES products (id)
  ON UPDATE CASCADE
  ON DELETE CASCADE
);

INSERT INTO product_image (product_id, src_image) VALUES (1, '/src/img.1');
INSERT INTO product_image (product_id, src_image) VALUES (2, '/src/img.15');

CREATE TABLE product_descriptions (
  product_id INT           NOT NULL,
  title       VARCHAR(2000) NOT NULL,
  description VARCHAR(2000) NOT NULL,
  FOREIGN KEY (product_id) REFERENCES products(id) ON UPDATE CASCADE ON DELETE CASCADE
);

INSERT INTO product_descriptions (product_id, title, description)
VALUES (1, 'testTitle1', 'Test description one');

INSERT INTO product_descriptions (product_id, title, description)
VALUES (2, 'testTitle2', 'Test description two');