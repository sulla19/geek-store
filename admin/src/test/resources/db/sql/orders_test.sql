CREATE TABLE orders (
  id                INT           NOT NULL AUTO_INCREMENT,
  last_modified     DATE          NOT NULL,
  customer_id       INT           NOT NULL,
  product_list      VARCHAR(1000) NOT NULL,
  total_price       DECIMAL(8, 2) NOT NULL,
  payment_type      VARCHAR(45)   NOT NULL,
  shipping_type     VARCHAR(45)   NOT NULL,
  delivery_id       INT,
  contact_telephone VARCHAR(50)   NOT NULL,
  PRIMARY KEY (id)
);

CREATE TABLE delivery (
  id          INT         NOT NULL AUTO_INCREMENT,
  order_id    INT         NOT NULL,
  company     VARCHAR(45) NOT NULL,
  country     VARCHAR(45) NOT NULL,
  city        VARCHAR(45) NOT NULL,
  address     VARCHAR(45) NOT NULL,
  postal_code VARCHAR(45) NOT NULL,
  telephone   VARCHAR(45) NOT NULL,
  FOREIGN KEY (order_id) REFERENCES orders (id) ON UPDATE CASCADE ON DELETE CASCADE,
  PRIMARY KEY (id)
);

INSERT INTO orders (last_modified, customer_id, product_list, total_price, payment_type, shipping_type, delivery_id, contact_telephone)
VALUES ('1999-03-03', 1, '[1,4]', 455, 'CREDITCARD', 'NATIONAL', 1, '(066)679-48-27');
INSERT INTO orders (last_modified, customer_id, product_list, total_price, payment_type, shipping_type, delivery_id, contact_telephone)
VALUES ('1945-03-03', 1, '[12,4,19]', 345345, 'CREDITCARD', 'INTERNATIONAL', 1, '(066)679-48-27');

INSERT INTO delivery (order_id, company, country, city, address, postal_code, telephone)
VALUES (1, 'Delivery', 'USA', 'Belfast', 'Washington str, 76', '46795', '(9789)555-55-55');
INSERT INTO delivery (order_id, company, country, city, address, postal_code, telephone)
VALUES (2, 'MistExpr', 'Brasilia', 'San Paolo', 'Washington str, 76', '46795', '(9789)555-55-55');

