CREATE TABLE customers (
  id         INT          NOT NULL AUTO_INCREMENT,
  first_name VARCHAR(45)  NOT NULL,
  last_name  VARCHAR(45)  NOT NULL,
  gender     VARCHAR(2)   NOT NULL,
  birth_date DATE         NOT NULL,
  telephone  VARCHAR(45)  NOT NULL,
  email      VARCHAR(45)  NOT NULL,
  password   VARCHAR(100) NOT NULL,
  PRIMARY KEY (id)
);

INSERT INTO customers (first_name, last_name, gender, birth_date, telephone, email, password)
VALUES ('John', 'Five', 'M', '1978-05-17', '(099)978-45-74', 'jpMail@jmail.com', '123456');
INSERT INTO customers (first_name, last_name, gender, birth_date, telephone, email, password)
VALUES ('Jimmy', 'First', 'M', '1958-08-15', '(099)345-45-74', 'jpageMail@jmail.com', '123456');
INSERT INTO customers (first_name, last_name, gender, birth_date, telephone, email, password)
VALUES ('Ritchie', 'Third', 'M', '1960-01-25', '(099)345-15-44', 'rblack@jmail.com', '123456');
