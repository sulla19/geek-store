CREATE TABLE category (
  id             INT NOT NULL AUTO_INCREMENT,
  img_url        VARCHAR(100),
  parent_id INT NOT NULL,
  PRIMARY KEY (id)
);

CREATE TABLE category_descriptions (
  category_id      INT         NOT NULL ,
  title       VARCHAR(200) NOT NULL,
  description VARCHAR(2000) NOT NULL,
  FOREIGN KEY (category_id) REFERENCES category (id) ON UPDATE CASCADE ON DELETE CASCADE
);

INSERT INTO category(img_url, parent_id) VALUES ( 'Art & Photography', 0);
INSERT INTO category( img_url, parent_id) VALUES ( 'Education & Teaching', 0);
INSERT INTO category( img_url, parent_id) VALUES (  'Computers & Technology', 0);
INSERT INTO category( img_url, parent_id) VALUES (  'Biographies & Memories', 0);
INSERT INTO category( img_url, parent_id) VALUES (  'Children''s Books', 0);
INSERT INTO category( img_url, parent_id) VALUES (  'Cookbooks & Food', 0);
INSERT INTO category( img_url, parent_id) VALUES (  'History', 0);
INSERT INTO category( img_url, parent_id) VALUES (  'Literature & Fiction', 0);
INSERT INTO category( img_url, parent_id) VALUES (  'Romance', 0);
INSERT INTO category( img_url, parent_id) VALUES (  'Sci-Fi & Fantasy', 0);


INSERT INTO category(img_url, parent_id) VALUES ( 'Architecture', 1);
INSERT INTO category(img_url, parent_id) VALUES ( 'Business of Art', 1);
INSERT INTO category( img_url, parent_id) VALUES ( 'Collections, Catalogs & Exhibitions', 1);
INSERT INTO category( img_url, parent_id) VALUES ('Decorative Arts & Design', 1);
INSERT INTO category( img_url, parent_id) VALUES ( 'Drawing', 1);
INSERT INTO category( img_url, parent_id) VALUES ( 'Fashion', 1);
INSERT INTO category( img_url, parent_id) VALUES ( 'Graphic Design', 1);
INSERT INTO category( img_url, parent_id) VALUES ( 'History & Criticism', 1);
INSERT INTO category( img_url, parent_id) VALUES ( 'Individual Artists', 1);
INSERT INTO category( img_url, parent_id) VALUES ( 'Music', 1);
INSERT INTO category( img_url, parent_id) VALUES ( 'Other Media', 1);
INSERT INTO category( img_url, parent_id) VALUES ( 'Painting', 1);
INSERT INTO category( img_url, parent_id) VALUES ( 'Performing Arts', 1);
INSERT INTO category( img_url, parent_id) VALUES ( 'Photography & Video', 1);
INSERT INTO category( img_url, parent_id) VALUES ( 'Religious', 1);
INSERT INTO category( img_url, parent_id) VALUES ( 'Sculpture', 1);
INSERT INTO category( img_url, parent_id) VALUES ( 'Study & Teaching', 1);



INSERT INTO category( img_url, parent_id) VALUES ( 'Higher & Continuing Education', 2);
INSERT INTO category( img_url, parent_id) VALUES ( 'Schools & Teaching ', 2);
INSERT INTO category( img_url, parent_id) VALUES (  'Studying & Workbooks', 2);
INSERT INTO category( img_url, parent_id) VALUES (  'Test Preparation', 2);



INSERT INTO category( img_url, parent_id) VALUES ( 'Business Technology', 3);
INSERT INTO category( img_url, parent_id) VALUES ( 'Certification', 3);
INSERT INTO category( img_url, parent_id) VALUES ( 'Computer Science', 3);
INSERT INTO category( img_url, parent_id) VALUES ( 'Databases & Big Data', 3);
INSERT INTO category( img_url, parent_id) VALUES ( 'Digital Audio, Video & Photography', 3);
INSERT INTO category(img_url, parent_id) VALUES ( 'Games & Strategy Guides', 3);
INSERT INTO category( img_url, parent_id) VALUES ( 'Graphics & Design', 3);
INSERT INTO category( img_url, parent_id) VALUES ( 'Hardware & DIY ', 3);
INSERT INTO category( img_url, parent_id) VALUES ( 'History & Culture', 3);
INSERT INTO category( img_url, parent_id) VALUES ( 'Internet & Social Media', 3);
INSERT INTO category( img_url, parent_id) VALUES ( 'Mobile Phones, Tablets & E-Readers', 3);
INSERT INTO category( img_url, parent_id) VALUES ( 'Networking & Cloud Computing', 3);
INSERT INTO category( img_url, parent_id) VALUES ( 'Operating Systems', 3);
INSERT INTO category( img_url, parent_id) VALUES ( 'Programming', 3);
INSERT INTO category( img_url, parent_id) VALUES ( 'Programming Languages', 3);
INSERT INTO category( img_url, parent_id) VALUES ( 'Security & Encryption', 3);
INSERT INTO category( img_url, parent_id) VALUES ( 'Software', 3);
INSERT INTO category( img_url, parent_id) VALUES ( 'Web Development & Design', 3);



INSERT INTO category( img_url, parent_id) VALUES (  'Ada', 46);
INSERT INTO category( img_url, parent_id) VALUES (  'Ajax', 46);
INSERT INTO category( img_url, parent_id) VALUES (  'Assembly Language Programming ', 46);
INSERT INTO category( img_url, parent_id) VALUES (  'Borland Delphi', 46);
INSERT INTO category( img_url, parent_id) VALUES (  'C & C++', 46);
INSERT INTO category( img_url, parent_id) VALUES (  'C_', 46);
INSERT INTO category( img_url, parent_id) VALUES (  'CSS', 46);
INSERT INTO category( img_url, parent_id) VALUES (  'Compilers', 46);
INSERT INTO category( img_url, parent_id) VALUES (  'Debugging', 46);
INSERT INTO category( img_url, parent_id) VALUES (  'Delphi', 46);
INSERT INTO category( img_url, parent_id) VALUES (  'Fortran', 46);
INSERT INTO category( img_url, parent_id) VALUES (  'Java', 46);
INSERT INTO category( img_url, parent_id) VALUES (  'Lisp', 46);
INSERT INTO category( img_url, parent_id) VALUES (  'Perl', 46);
INSERT INTO category( img_url, parent_id) VALUES (  'Python', 46);
INSERT INTO category( img_url, parent_id) VALUES (  'Ruby', 46);
INSERT INTO category( img_url, parent_id) VALUES (  'Swift', 46);
INSERT INTO category( img_url, parent_id) VALUES (  'Visual Basic', 46);
INSERT INTO category( img_url, parent_id) VALUES (  'XHTML', 46);
INSERT INTO category( img_url, parent_id) VALUES (  'XML', 46);


INSERT INTO category_descriptions(category_id, title, description) VALUES ( 1, 'Art & Photography', 'Description');
INSERT INTO category_descriptions(category_id, title, description) VALUEs ( 11, 'Architecture', 'Description');
INSERT INTO category_descriptions(category_id, title, description) VALUES ( 12, 'Business of Art', 'Description');
INSERT INTO category_descriptions(category_id, title, description) VALUES ( 13, 'Collections, Catalogs & Exhibitions', 'Description');
INSERT INTO category_descriptions(category_id, title, description) VALUES (14,'Decorative Arts & Design', 'Description');
INSERT INTO category_descriptions(category_id, title, description) VALUES (15,  'Drawing', 'Description');
INSERT INTO category_descriptions(category_id, title, description) VALUES (16,  'Fashion',  'Description');
INSERT INTO category_descriptions(category_id, title, description) VALUES (17,  'Graphic Design', 'Description');
INSERT INTO category_descriptions(category_id, title, description) VALUES (18,  'History & Criticism', 'Description');
INSERT INTO category_descriptions(category_id, title, description) VALUES (19,  'Individual Artists', 'Description');
INSERT INTO category_descriptions(category_id, title, description) VALUES (20,  'Music', 'Description');
INSERT INTO category_descriptions(category_id, title, description) VALUES (21,  'Other Media', 'Description');
INSERT INTO category_descriptions(category_id, title, description) VALUES (22,  'Painting', 'Description');
INSERT INTO category_descriptions(category_id, title, description) VALUES (23,  'Performing Arts', 'Description');
INSERT INTO category_descriptions(category_id, title, description) VALUES (24,  'Photography & Video', 'Description');
INSERT INTO category_descriptions(category_id, title, description) VALUES (25,  'Religious', 'Description');
INSERT INTO category_descriptions(category_id, title, description) VALUES (26,  'Sculpture', 'Description');
INSERT INTO category_descriptions(category_id, title, description) VALUES (27,  'Study & Teaching', 'Description');
INSERT INTO category_descriptions(category_id, title, description) VALUES (2,  'Education & Teaching',  'Description');
INSERT INTO category_descriptions(category_id, title, description) VALUES (28,  'Higher & Continuing Education', 'Description');
INSERT INTO category_descriptions(category_id, title, description) VALUES (29,  'Schools & Teaching ',  'Description');
INSERT INTO category_descriptions(category_id, title, description) VALUES (30,  'Studying & Workbooks',  'Description');
INSERT INTO category_descriptions(category_id, title, description) VALUES (31,  'Test Preparation',  'Description');
INSERT INTO category_descriptions(category_id, title, description) VALUES (3,  'Computers & Technology',  'Description');
INSERT INTO category_descriptions(category_id, title, description) VALUES (32,  'Business Technology',  'Description');
INSERT INTO category_descriptions(category_id, title, description) VALUES (33,  'Certification',   'Description');
INSERT INTO category_descriptions(category_id, title, description) VALUES (34,  'Computer Science',  'Description');
INSERT INTO category_descriptions(category_id, title, description) VALUES (35,  'Databases & Big Data',  'Description');
INSERT INTO category_descriptions(category_id, title, description) VALUES (36,  'Digital Audio, Video & Photography',  'Description');
INSERT INTO category_descriptions(category_id, title, description) VALUES (37,  'Games & Strategy Guides',  'Description');
INSERT INTO category_descriptions(category_id, title, description) VALUES (38,  'Graphics & Design',  'Description');
INSERT INTO category_descriptions(category_id, title, description) VALUES (39,  'Hardware & DIY ',  'Description');
INSERT INTO category_descriptions(category_id, title, description) VALUES (40,  'History & Culture',  'Description');
INSERT INTO category_descriptions(category_id, title, description) VALUES (41,  'Internet & Social Media',  'Description');
INSERT INTO category_descriptions(category_id, title, description) VALUES ( 42, 'Mobile Phones, Tablets & E-Readers',  'Description');
INSERT INTO category_descriptions(category_id, title, description) VALUES ( 43, 'Networking & Cloud Computing', 'Description');
INSERT INTO category_descriptions(category_id, title, description) VALUES (44,  'Operating Systems', 'Description');
INSERT INTO category_descriptions(category_id, title, description) VALUES ( 45 ,'Programming', 'Description');
INSERT INTO category_descriptions(category_id, title, description) VALUES ( 46, 'Programming Languages', 'Description');
INSERT INTO category_descriptions(category_id, title, description) VALUES ( 50, 'Ada', 'Description');
INSERT INTO category_descriptions(category_id, title, description) VALUES (51,  'Ajax', 'Description');
INSERT INTO category_descriptions(category_id, title, description) VALUES ( 52, 'Assembly Language Programming ',  'Description');
INSERT INTO category_descriptions(category_id, title, description) VALUES ( 53, 'Borland Delphi',   'Description');
INSERT INTO category_descriptions(category_id, title, description) VALUES ( 54, 'C & C++',  'Description');
INSERT INTO category_descriptions(category_id, title, description) VALUES ( 55, 'C_',  'Description');
INSERT INTO category_descriptions(category_id, title, description) VALUES (56,  'CSS',  'Description');
INSERT INTO category_descriptions(category_id, title, description) VALUES ( 57, 'Compilers',  'Description');
INSERT INTO category_descriptions(category_id, title, description) VALUES ( 58, 'Debugging',  'Description');
INSERT INTO category_descriptions(category_id, title, description) VALUES ( 59, 'Delphi',  'Description');
INSERT INTO category_descriptions(category_id, title, description) VALUES ( 60, 'Fortran',  'Description');
INSERT INTO category_descriptions(category_id, title, description) VALUES ( 61, 'Java',   'Description');
INSERT INTO category_descriptions(category_id, title, description) VALUES ( 62, 'Lisp',  'Description');
INSERT INTO category_descriptions(category_id, title, description) VALUES ( 63, 'Perl',  'Description');
INSERT INTO category_descriptions(category_id, title, description) VALUES ( 64, 'Python',  'Description');
INSERT INTO category_descriptions(category_id, title, description) VALUES ( 65, 'Ruby',  'Description');
INSERT INTO category_descriptions(category_id, title, description) VALUES ( 66, 'Swift',  'Description');
INSERT INTO category_descriptions(category_id, title, description) VALUES ( 67, 'Visual Basic', 'Description');
INSERT INTO category_descriptions(category_id, title, description) VALUES (  68,'XHTML',  'Description');
INSERT INTO category_descriptions(category_id, title, description) VALUES ( 69, 'XML',  'Description');
INSERT INTO category_descriptions(category_id, title, description) VALUES ( 47, 'Security & Encryption',  'Description');
INSERT INTO category_descriptions(category_id, title, description) VALUES ( 48, 'Software',  'Description');
INSERT INTO category_descriptions(category_id, title, description) VALUES ( 49, 'Web Development & Design',  'Description');
INSERT INTO category_descriptions(category_id, title, description) VALUES ( 4,'Biographies & Memories',  'Description');
INSERT INTO category_descriptions(category_id, title, description) VALUES ( 5, 'Children''s Books',   'Description');
INSERT INTO category_descriptions(category_id, title, description) VALUES ( 6, 'Cookbooks & Food',  'Description');
INSERT INTO category_descriptions(category_id, title, description) VALUES ( 7, 'History', 'Description');
INSERT INTO category_descriptions(category_id, title, description) VALUES ( 8, 'Literature & Fiction',  'Description');
INSERT INTO category_descriptions(category_id, title, description) VALUES (9,  'Romance',  'Description');
INSERT INTO category_descriptions(category_id, title, description) VALUES ( 10, 'Sci-Fi & Fantasy',  'Description');


